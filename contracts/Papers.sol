pragma solidity ^ 0.4 .18;

/**
TODO: when sold: proof of sent key
optional TODO: request/review declined message
optional TODO: after deadline: half the money?
*/



/**
 *  @author Christian Weitzel
 *  @title This is RevChain, a Program for uploading and buying papers and
 *    requesting and making reviews.
 *  @notice The Review Process of a paper goes through the following steps:
 *  1.  upload paper and ask for reviews.
 *      Possible paper updates:
 *      - add review slots
 *      - add new paper version
 *      - change price
 *      - change main and sub field
 *      - extend review deadline
 *  2.  a reviewer sends a request for making a review for the paper.
 *  3.  the paper uploader acceptes or declines the review request.
 *  4.  when the paper uploader accepts the review request, the reviewer can
 *        now upload his review.
 *  5.  the paper uploader accepts or declines the uploaded review.
 *      - If declined, the occupied review slot is now free again.
 *      - If accepted, the reviewer receives his payment.
 *      - If the paper uploader does not answer the uploaded review, it can be
 *        declared as accepted and the reviewer can get his payment.
 *  6.  after the deadline is exceeded, the paper uploader can set his paper
 *        to final. No further review requests can be sent and the paper cannot
 *        be updated anymore. When the paper is set to final, the paper uploader
 *        gets back the money for review slots, that were not occupied.
 *
 *  Reason for the data format for papers and reviews with arrays and mappings:
 *    paperList is iteratable and can be used to return a list of IPFS hashes to
 *    all uploaded papers. It is not used to store Paper structs, because that
 *    would mean that every Paper struct added to it, has to be completely defined.
 *    Therefore, paperStructs maps the IPFS hashes of the papers to the corresponding
 *    Paper structs. With this data format, it is possible to have dynamic arrays
 *    inside the Paper struct and it is not necessary to define all values of
 *    a Paper struct element.
 *    Another advantage is that it is not necessary to iterate over a complete
 *    array to find a specific Paper struct. Instead, the mapping can return
 *    the struct very efficient by only needing the bytes32-hash of the paper.
 */
contract Papers {
  // Holds a list of all IPFS hashes of uploaded papers.
  bytes32[] private paperList;
  // Mapping of the IPFS hashes of uploaded papers to the corresponding Paper struct.
  mapping(bytes32 => Paper) paperStructs;
  // Mapping of ethereum addresses to the corresponding Owner structs.
  mapping(address => Owner) ownerStructs;

  /**
   *  Stores the review request states for specific reviews.
   *  0: requested - a review request was written and uploaded to IPFS. It is then
   *       availabel to the paper uploader which can accept or decline the request.
   *  1: review - the uploader accepted a review request. the reviewer has now
   *       the possibility to upload a review. A review slot is now occupied.
   *  2: uploaded - a finished review was uploaded. The paper uploader can now
   *       accept or decline the uploaded review.
   *  3: reviewAccepted - states that the paper uploader accepted the review or
   *       that he/she did not answer for four weeks, because then the review will
   *       automatically be accepted. It is now possible to transfer the payment
   *       to the reviewer.
   *  4: requestDeclined - the paper uploader declined the request. The reviewer
   *       can not make a review.
   *  5: reviewDeclined - the paper uploader declined the uploaded review. The
   *       reviewer will not get any payment.
   *  6: paymentSucceeded - the reviewer received his payment for the finished
   *       and accepted review he uploaded. This will automatically happen when
   *       the paper uploader accepts the review.
  */
  enum reviewState {
    requested,
    review,
    uploaded,
    reviewAccepted,
    requestDeclined,
    reviewDeclined,
    paymentSucceeded
  }

  // --- Structs --------------------------------------------------------------

  /**
   *  @notice Used to store a paper and corresponding information.
   *  @param versions IPFS hashes of the uploaded paper versions and the
   *    following details:
   *    - paper title
   *    - paper abstract
   *    - paper price
   *    - review deadline
   *    - review price
   *    - review slots
   *    - main field
   *    - sub field
   *    When the paper or corresponding details are updated, a new IPFS hash is
   *    pushed onto this array. The first element of this array is always the
   *    hash from the first paper. This hash is also the identifier that is
   *    stored in paperList and paperStructs.
   *  @param owner the uploader of the paper.
   *  @param price the price for which the paper can be bought.
   *  @param reviewPrice the money that a reviewer gets for a finished review.
   *  @param balance the amount of money stored in the struct. When uploading a
   *    paper, it is set to reviewPrice * reviewSlots. If a payment to a reviewer
   *    is made, the amount of one reviewPrice is subtracted from the balance.
   *  @param reviewSlots the number of available review slots.
   *  @param reviewSlotsUsed when a review request is accepted, a review slot is
   *    occupied. A review slot becomes free again when a review is declined. If
   *    reviewSlotsUsed == reviewSlots, all slots are occupied and no further
   *    requests can be accepted.
   *  @param reviewList a list of all IPFS hashes from review requests made for
   *    this paper.
   *  @param reviewStructs a mapping from the IPFS hahes, which are also used in
   *    the reviewList, to the corresponding Review Structs. The reason for this
   *    data structure can be found in the documentation for the global variables
   *    paperList and paperStructs.
   *  @param finalState when a paper is uploaded, this is set to false. If it is
   *    set to TRUE via the function setFinalState, the paper cannot be changed
   *    and no reviews can be requested or made anymore.
   *  @param mainField a compressed bytes32 hash that dissolves into the main
   *    field of this paper (e.g. physics, mathematics, philosophy).
   *  @param subField a compressed bytes32 hash that dissolves into the sub
   *    field of this paper (e.g. quantum physics, numeric, philosophy of education).
   *  @param timestamp unix time of when the paper was uploaded.
   *  @param deadline unix time limit. After this point of time all review requests
   *    where no review was uploaded are declined when . No more
  */
  struct Paper {
    bytes32[] versions;
    address owner;
    uint price;
    uint reviewPrice;
    uint balance;
    uint8 reviewSlots;
    uint8 reviewSlotsUsed;
    bytes32[] reviewList;
    mapping(bytes32 => Review) reviewStructs;
    bool finalState;
    bytes32 mainField;
    bytes32 subField;
    uint timestamp;
    uint deadline;
  }

  /**
   *  @notice Used to store review requests and reviews.
   *  @param owner the address of the review requester.
   *  @param requestIpfsHash IPFS hash of the review request.
   *  @param reviewIpfsHash IPFS hash of the uploaded review.
   *  @param paper the paper to which this review request was made.
   *  @param state Review Request State of the review.
   *  @param timestamp unix time of when the confirmation for the review upload
   *    was posted on the blockchain. This is used because after four weeks of
   *    no answer of the paper uploader, the review is accepted and the reviewer
   *    can withdraw the payment for his review.
  */
  struct Review {
    address owner;
    bytes32 requestIpfsHash;
    bytes32 reviewIpfsHash;
    bytes32 paper;
    reviewState state;
    uint timestamp;
  }

  /**
   *  @notice Used to store the user profile and data for a user.
   *  @param userProfile IPFS hash to the user profile data of a specific person
   *  @param reviewList all review requests made by this user. They can be used
   *    to find the corresponding Review struct.
   *  @param paperUploadList all uploaded papers of this user
   *  @param paperBoughtList all papers bought by this user
   */
  struct Owner {
    bytes32 userProfile;
    bytes32[] reviewList;
    bytes32[] paperUploadList;
    bytes32[] paperBoughtList;
  }


  // --- Modifiers -------------------------------------------------------------

  /**
   *  @author Christian Weitzel
   *  Checks if a paper exists.
   *  @param paperHash IPFS hash of the paper
   *  @param equals should the paper exist?
   */
  modifier paperExists(bytes32 paperHash, bool equals) {
    require(equals == (paperStructs[paperHash].versions.length != 0));
    _;
  }

  /**
   *  @author Christian Weitzel
   *  Check if a paper is in finalState.
   *  @param paperHash IPFS hash of the paper
   *  @param finalState the finalState of the paper should equal this
   */
  modifier paperFinalState(bytes32 paperHash, bool finalState) {
    // requires the paper to exist
    require(paperStructs[paperHash].versions.length != 0);
    require(paperStructs[paperHash].finalState == finalState);
    _;
  }

  /**
   *  @author Christian Weitzel
   *  Checks if there are free review slots.
   *  @param paperHash IPFS hash of the paper
   */
  modifier slotsFree(bytes32 paperHash) {
    // requires the paper to exist
    require(paperStructs[paperHash].versions.length != 0);
    require(paperStructs[paperHash].reviewSlotsUsed < paperStructs[paperHash].reviewSlots);
    _;
  }

  /**
   *  @author Christian Weitzel
   *  Checks if the deadline is not yet exceeded.
   *  @param paperHash IPFS hash of the paper
   */
  modifier beforeDeadline(bytes32 paperHash) {
    // requires the paper to exist
    require(paperStructs[paperHash].versions.length != 0);
    // requires the paper to be not in finalState = true
    require(paperStructs[paperHash].finalState == false);
    // check against the timestamp while mining
    require(paperStructs[paperHash].deadline > block.timestamp);
    _;
  }

  /**
   *  @author Christian Weitzel
   *  Checks if a specific review request exists for a paper.
   *  @param paperHash IPFS hash of the paper
   *  @param requestHash IPFS hash of the review request
   */
  modifier requestExists(bytes32 paperHash, bytes32 requestHash) {
    // requires the paper to exist
    require(paperStructs[paperHash].versions.length != 0);
    require(paperStructs[paperHash].reviewStructs[requestHash].requestIpfsHash == requestHash);
    _;
  }

  /**
   *  @author Christian Weitzel
   *  Checks if a review exists for a specific review request.
   *  @param paperHash IPFS hash of the paper
   *  @param requestHash IPFS hash of the review request
   */
  modifier reviewExists(bytes32 paperHash, bytes32 requestHash) {
    // requires the paper to exist
    require(paperStructs[paperHash].versions.length != 0);
    // the requestHash has to exist. this means, that the Review Struct exists.
    require(paperStructs[paperHash].reviewStructs[requestHash].requestIpfsHash == requestHash);
    require(paperStructs[paperHash].reviewStructs[requestHash].reviewIpfsHash != 0);
    _;
  }

  /**
   *  @author Christian Weitzel
   *  Checks if the msg.sender is the owner of a specific review request.
   *  @param paperHash IPFS hash of the paper
   *  @param requestHash IPFS hash of the review request
   */
  modifier reviewOwner(bytes32 paperHash, bytes32 requestHash) {
    // requires the paper to exist
    require(paperStructs[paperHash].versions.length != 0);
    // the requestHash has to exist. this means, that the Review Struct exists.
    require(paperStructs[paperHash].reviewStructs[requestHash].requestIpfsHash == requestHash);
    require(paperStructs[paperHash].reviewStructs[requestHash].owner == msg.sender);
    _;
  }

  /**
   *  @author Christian Weitzel
   *  Checks if a review request is in a specific reviewState.
   *  @param paperHash IPFS hash of the paper
   *  @param requestHash IPFS hash of the review request
   *  @param enumState the reviewState the review should be in
   */
  modifier isReviewState(bytes32 paperHash, bytes32 requestHash, reviewState enumState) {
    // requires the paper to exist
    require(paperStructs[paperHash].versions.length != 0);
    // the requestHash has to exist. this means, that the Review Struct exists.
    require(paperStructs[paperHash].reviewStructs[requestHash].requestIpfsHash == requestHash);
    require(paperStructs[paperHash].reviewStructs[requestHash].state == enumState);
    _;
  }

  /**
   *  @author Christian Weitzel
   *  Check if the paper was bought by the message sender.
   *  @param paperHash IPFS hash of the paper
   */
  modifier paperBought(bytes32 paperHash) {
    // requires the paper to exist
    require(paperStructs[paperHash].versions.length > 0);
    // boolean: if found, it's true
    bool ready = false;
    // runtime variable for the while loop
    uint i = 0;
    // repeat until the end of the list or until the element is found
    while(i < ownerStructs[msg.sender].paperBoughtList.length){
      // if the element was found, set ready to true and break out of the while
      // loop
      if(ownerStructs[msg.sender].paperBoughtList[i] == paperHash){
        ready = true;
        break;
      }
      // if the element was not found, iterate i and repeat
      i++;
    }
    // when the element was found, start the code, else revert
    require(ready == true);
    _;
  }

  /**
   *  @author Christian Weitzel
   *  Check if the message sender is the owner of the paper.
   *  @param paperHash IPFS hash of the paper
   */
  modifier paperOwner(bytes32 paperHash) {
    require(paperStructs[paperHash].owner == msg.sender);
    _;
  }

  /**
   *  @author Christian Weitzel
   *  Check if the requested user is registered.
   *  @param user ethereum address.
   */
  modifier ownerExists(address user) {
    require(ownerStructs[user].userProfile != 0);
    _;
  }


  // --- Getters: Papers  ------------------------------------------------------

  /**
   *  @author Christian Weitzel
   *  @dev The actual number of papers existing in this program.
   *  @return The length of the global paperList array
   */
  function getPaperArrayLength()
  public view
  returns(uint length) {
    return paperList.length;
  }

  /**
   *  @author Christian Weitzel
   *  @dev Returns a list of all uploaded papers.
   *  @return an array with IPFS hashes of all papers
   */
  function getAllPapers()
  public view
  returns(bytes32[] allPapers) {
    return paperList;
  }


  // --- Getters: Specific Paper -----------------------------------------------

  /**
   *  @author Christian Weitzel
   *  @dev Returns all versions of a specific paper. The first element of the
   *    array is the oldest version and the last element is the newest version.
   *  @param paperHash IPFS hash of the paper
   *  @return an array with IPFS hashes of all paper versions of a specific paper.
   *    Reverts when no paper exists.
   */
  function getPaperVersions(bytes32 paperHash)
  public view
  paperExists(paperHash, true) // paper should exist
  returns(bytes32[] versions) {
    return paperStructs[paperHash].versions;
  }

  /**
   *  @author Christian Weitzel
   *  @dev Returns the number of uploaded versions of a paper.
   *  @param paperHash IPFS hash of the paper
   *  @return the length of the array with IPFS hashes of all paper versions.
   *    Reverts when no paper exists.
   */
  function getPaperVersionsLength(bytes32 paperHash)
  public view
  paperExists(paperHash, true) // paper should exist
  returns(uint versionsLength) {
    return paperStructs[paperHash].versions.length;
  }

  /**
   *  @author Christian Weitzel
   *  @dev Returns the IPFS hash of the newest paper version.
   *  @param paperHash IPFS hash of the paper
   *  @return the last element of the versions array of a Paper struct, which
   *    is the newest version of the paper.
   *    Reverts when no paper exists.
   */
  function getNewestPaperIPFS(bytes32 paperHash)
  public view
  paperExists(paperHash, true) // paper should exist
  returns(bytes32 newestPaperHash) {
    return paperStructs[paperHash].versions[paperStructs[paperHash].versions.length - 1];
  }

  /**
   *  @author Christian Weitzel
   *  @dev Returns the ethereum address of the uploader of a paper.
   *  @param paperHash IPFS hash of the paper
   *  @return the uploader of a paper.
   *    Reverts when no paper exists.
   */
  function getPaperOwner(bytes32 paperHash)
  public view
  paperExists(paperHash, true) // paper should exist
  returns(address owner) {
    return paperStructs[paperHash].owner;
  }

  /**
   *  @author Christian Weitzel
   *  @dev Returns the price for which a paper can be bought.
   *  @param paperHash IPFS hash of the paper
   *  @return the price of a paper.
   *    Reverts when no paper exists.
   */
  function getPaperPrice(bytes32 paperHash)
  public view
  paperExists(paperHash, true) // paper should exist
  returns(uint price) {
    return paperStructs[paperHash].price;
  }

  /**
   *  @author Christian Weitzel
   *  @dev Returns the price for which a review of a paper can be made.
   *  @param paperHash IPFS hash of the paper
   *  @return the price for a review.
   *    Reverts when no paper exists.
   */
  function getReviewPrice(bytes32 paperHash)
  public view
  paperExists(paperHash, true) // paper should exist
  returns(uint reviewPrice) {
    return paperStructs[paperHash].reviewPrice;
  }

  /**
   *  @author Christian Weitzel
   *  @dev Returns the amount of money that is stored for a paper. This money
   *    is used to pay finished and accepted reviews.
   *  @param paperHash IPFS hash of the paper
   *  @return the balance of a paper.
   *    Reverts when no paper exists.
   */
  function getPaperBalance(bytes32 paperHash)
  public view
  paperExists(paperHash, true) // paper should exist
  returns(uint balance) {
    return paperStructs[paperHash].balance;
  }

  /**
   *  @author Christian Weitzel
   *  @dev Returns the total amount of existing review slots for a paper.
   *  @param paperHash IPFS hash of the paper
   *  @return the number of review slots for a paper.
   *    Reverts when no paper exists.
   */
  function getReviewSlots(bytes32 paperHash)
  public view
  paperExists(paperHash, true) // paper should exist
  returns(uint reviewSlots) {
    return paperStructs[paperHash].reviewSlots;
  }

  /**
   *  @author Christian Weitzel
   *  @dev Returns the amount of occupied review slots of a paper.
   *  @param paperHash IPFS hash of the paper
   *  @return the amount of occupied review slots.
   *    Reverts when no paper exists.
   */
  function getReviewSlotsUsed(bytes32 paperHash)
  public view
  paperExists(paperHash, true) // paper should exist
  returns(uint reviewSlots) {
    return paperStructs[paperHash].reviewSlotsUsed;
  }

  /**
   *  @author Christian Weitzel
   *  @dev Returns the amount of occupied review slots of a paper.
   *  @param paperHash IPFS hash of the paper
   *  @return the amount of occupied review slots.
   *    Reverts when no paper exists.
   */
  function getReviewSlotsFree(bytes32 paperHash)
  public view
  paperExists(paperHash, true) // paper should exist
  returns(uint reviewSlots) {
    return paperStructs[paperHash].reviewSlots - paperStructs[paperHash].reviewSlotsUsed;
  }

  /**
   *  @author Christian Weitzel
   *  @dev Returns an array with IPFS hashes of all review requests made for a
   *    paper. Theses IPFS hashes are also the adresses of the review structs.
   *  @param paperHash IPFS hash of the paper
   *  @return an array with hashes of all review structs for a specific paper.
   *    Reverts when no paper exists.
   */
  function getPaperReviewList(bytes32 paperHash)
  public view
  paperExists(paperHash, true) // paper should exist
  returns(bytes32[] allReviews) {
    return paperStructs[paperHash].reviewList;
  }

  /**
   *  @author Christian Weitzel
   *  @dev Returns TRUE if a request exists and reverts if not
   *  @param paperHash IPFS hash of the paper
   *  @param requestHash IPFS hash of the review request
   *  @return true, because otherwise the modifier requestExists will revert.
   */
  function getReviewRequestExists(bytes32 paperHash, bytes32 requestHash)
  public view
  requestExists(paperHash, requestHash) // review request should exist
  returns(bool exists) {
    return true;
  }

  /**
   *  @author Christian Weitzel/Carsten Fricke
   *  @dev Returns the address of the reviewer.
   *  @param paperHash IPFS hash of the paper
   *  @return the ethereum address of the creator of the review struct.
   */
  function getReviewOwner(bytes32 paperHash, bytes32 requestHash)
  public view
  requestExists(paperHash, requestHash) // review request should exist
  returns(address owner) {
    return paperStructs[paperHash].reviewStructs[requestHash].owner;
  }

  /**
   *  @author Christian Weitzel
   *  @dev Returns TRUE if a review exists and reverts if not
   *  @param paperHash IPFS hash of the paper
   *  @param requestHash IPFS hash of the review request
   *  @return true, because otherwise the modifier reviewExists will revert.
   */
  function getReviewExists(bytes32 paperHash, bytes32 requestHash)
  public view
  reviewExists(paperHash, requestHash) // review should exist
  returns(bool exists) {
    return true;
  }

  /**
   *  @author Christian Weitzel
   *  @dev Returns the state of the paper. A paper in finalState can not be
   *    changed anymore and can not get any reviews.
   *  @param paperHash IPFS hash of the paper
   *  @return the finalState of the paper.
   *    Reverts when no paper exists.
   */
  function getPaperFinalState(bytes32 paperHash)
  public view
  paperExists(paperHash, true) // paper should exist
  returns(bool thisFinalState) {
    return paperStructs[paperHash].finalState;
  }

  /**
   *  @author Christian Weitzel
   *  @dev Returns the main field of a paper. (e.g. physics)
   *  @param paperHash IPFS hash of the paper
   *  @return the mainField of a paper.
   *    Reverts when no paper exists.
   */
  function getMainField(bytes32 paperHash)
  public view
  paperExists(paperHash, true) // paper should exist
  returns(bytes32 mainField) {
    return paperStructs[paperHash].mainField;
  }

  /**
   *  @author Christian Weitzel
   *  @dev Returns the sub field of a paper. (e.g. quantum physics)
   *  @param paperHash IPFS hash of the paper
   *  @return the subField of a paper.
   *    Reverts when no paper exists.
   */
  function getSubField(bytes32 paperHash)
  public view
  paperExists(paperHash, true) // paper should exist
  returns(bytes32 subField) {
    return paperStructs[paperHash].subField;
  }

  /**
   *  @author Christian Weitzel
   *  @dev Returns the unix time-stamp of when a paper was uploaded.
   *  @param paperHash IPFS hash of the paper
   *  @return timestamp of the paper.
   *    Reverts when no paper exists.
   */
  function getTimestamp(bytes32 paperHash)
  public view
  paperExists(paperHash, true) // paper should exist
  returns(uint timestamp) {
    return paperStructs[paperHash].timestamp;
  }

  /**
   *  @author Christian Weitzel
   *  @dev Returns the unix time "deadline" until which reviews have to be
   *    finished. Reviews not finished before this deadline can not be uploaded
   *    anymore and will be automatically declined when the reviewState of the
   *    paper is set to final.
   *  @param paperHash IPFS hash of the paper
   *  @return the deadline for reviews of a paper.
   *    Reverts when no paper exists.
   */
  function getDeadline(bytes32 paperHash)
  public view
  paperExists(paperHash, true) // paper should exist
  returns(uint deadline) {
    return paperStructs[paperHash].deadline;
  }


  // --- Getters: Reviews  -----------------------------------------------------

  /**
   *  @author Christian Weitzel
   *  @dev Returns the IPFS hash of the review.
   *  @param paperHash IPFS hash of the paper
   *  @param requestHash IPFS hash of the review request
   *  @return the IPFS hash of a review.
   *    Reverts when no paper exists.
   */
  function getReviewIpfsHash(bytes32 paperHash, bytes32 requestHash)
  public view
  requestExists(paperHash, requestHash) // request should exist
  returns(bytes32 reviewRequest) {
    return paperStructs[paperHash].reviewStructs[requestHash].reviewIpfsHash;
  }

  /**
   *  @author Christian Weitzel
   *  @dev Returns the reviewState of a specific review.
   *  @param paperHash IPFS hash of the paper
   *  @param requestHash IPFS hash of the review request
   *  @return reviewState.
   *    Reverts when no paper exists.
   */
  function getReviewState(bytes32 paperHash, bytes32 requestHash)
  public view
  requestExists(paperHash, requestHash) // request should exist
  returns(uint thisreviewState) {
    return uint(paperStructs[paperHash].reviewStructs[requestHash].state);
  }


  // --- Getters: Owner  -------------------------------------------------------



  /**
   *  @author Christian Weitzel
   *  @dev Returns the IPFS hash of the user profile of a specific account.
   *  @param account the adress of a specific ethereum account
   *  @return IPFS hash of the user profile.
   *    Reverts when the user profile is not existent.
   */
  function getUserDetailsIPFS(address account)
  public view
  returns(bytes32 userProfile) {
    require(ownerStructs[account].userProfile != 0);
    return ownerStructs[account].userProfile;
  }

  /**
   *  @author Christian Weitzel
   *  @dev Returns an array with all review requests that were made by a user
   *    identified by his/her ethereum account address.
   *  @param account the adress of a specific ethereum account
   *  @return an array of all made review requests of a user.
   */
  function getOwnerReviewList(address account)
  public view
  returns(bytes32[] reviewList) {
    return ownerStructs[account].reviewList;
  }

  /**
   *  @author Christian Weitzel
   *  @dev returns an array of all papers that were uploaded by a user
   *    identified by his/her ethereum account address.
   *  @param account the adress of a specific ethereum account
   *  @return an array of all uploaded papers of a user
   */
  function getOwnerPaperUploadList(address account)
  public view
  returns(bytes32[] paperUploadList) {
    return ownerStructs[account].paperUploadList;
  }

  /**
   *  @author Christian Weitzel
   *  @dev Retunrs an array with all papers that were bought by a user identified
   *    by his/her ethereum account address.
   *  @param account the adress of a specific ethereum account
   *  @return an array of all bought papers of a user
   */
  function getOwnerPaperBoughtList(address account)
  public view
  returns(bytes32[] paperBoughtList) {
    return ownerStructs[account].paperBoughtList;
  }


  // @author Henrik Csoere, Christian Weitzel
  // --- User Details ----------------------------------------------------------
  // save the IPFS hash to a user profile


  /**
   *  @author Christian Weitzel
   *  @dev Saves the user profile data of the caller in the mapping ownerStructs.
   *  @param profileHash IPFS hash of the user profile
   */
  function setUserDetailsIPFS(bytes32 profileHash)
  public {
    ownerStructs[msg.sender].userProfile = profileHash;
  }


  // --- Review Process --------------------------------------------------------


  /**
   *  @author Christian Weitzel
   *  @dev 1. Upload a paper, request reviews and reserve money for the review
   *    payments.
   *  @param paperHash IPFS hash of the uploaded paper
   *  @param price the price for which the paper can be bought
   *  @param reviewPrice the money a reviewer gets for a review. Has to be higher
   *    than price
   *  @param reviewSlots the amount of reviews that the uploader wants to get
   *  @param mainField the main field of the paper (e.g. physics)
   *  @param subField the sub field of the paper (e.g. quantum physics)
   *  @param deadline the deadline for reviews to be finished
   */
  function uploadPaper(bytes32 paperHash, uint price, uint reviewPrice,
  uint8 reviewSlots, bytes32 mainField, bytes32 subField, uint deadline)
  public payable
  paperExists(paperHash, false) // paper should not exist
  ownerExists(msg.sender) // user should be registered
  {
    // the money a reviewer gets has to be higher than the price of the paper
    require(price >= 0 && reviewPrice >= 0 && reviewPrice > price);
    // the total amount of money reserved has to equal reviewPrice * reviewSlots
    require(msg.value == reviewPrice * reviewSlots);
    // the deadline has to be in the future
    require(deadline > block.timestamp);

    // write information into the corresponding mapping location of the paper
    // paper IPFS hash
    paperStructs[paperHash].versions.push(paperHash);
    // owner of the paper / paper uploader
    paperStructs[paperHash].owner = msg.sender;
    paperStructs[paperHash].price = price;
    paperStructs[paperHash].reviewPrice = reviewPrice;
    // store money
    paperStructs[paperHash].balance = msg.value;
    paperStructs[paperHash].reviewSlots = reviewSlots;
    // no review slots are used yet
    paperStructs[paperHash].reviewSlotsUsed = 0;
    // paper is not in final state
    paperStructs[paperHash].finalState = false;
    paperStructs[paperHash].mainField = mainField;
    paperStructs[paperHash].subField = subField;
    // timestamp of when the paper was uploaded and saved in the blockchain
    paperStructs[paperHash].timestamp = block.timestamp;
    paperStructs[paperHash].deadline = deadline;

    // save the paper hash for the owner
    ownerStructs[msg.sender].paperUploadList.push(paperHash);
    // add the IPFS hash of the paper to the paperList
    paperList.push(paperHash);
  }

  /**
   *  @author Christian Weitzel
   *  @dev 1. Upload a paper without requesting for reviews. Its finalState is
   *    directly set to TRUE and all review-related parameters are set to 0.
   *  @param paperHash IPFS hash of the uploaded paper
   *  @param price the price for which the paper can be bought
   *  @param mainField the main field of the paper (e.g. physics)
   *  @param subField the sub field of the paper (e.g. quantum physics)
   */
  function uploadFinalPaper(bytes32 paperHash, uint price, bytes32 mainField, bytes32 subField)
  public
  paperExists(paperHash, false) // paper should not exist
  ownerExists(msg.sender) // user should be registered
  {
   // write information into the corresponding mapping location of the paper
   // paper IPFS hash
   paperStructs[paperHash].versions.push(paperHash);
   // owner of the paper / paper uploader
   paperStructs[paperHash].owner = msg.sender;
   paperStructs[paperHash].price = price;
   paperStructs[paperHash].reviewPrice = 0;
   // store money
   paperStructs[paperHash].balance = 0;
   paperStructs[paperHash].reviewSlots = 0;
   // no review slots are used yet
   paperStructs[paperHash].reviewSlotsUsed = 0;
   // paper is not in final state
   paperStructs[paperHash].finalState = true;
   paperStructs[paperHash].mainField = mainField;
   paperStructs[paperHash].subField = subField;
   // timestamp of when the paper was uploaded and saved in the blockchain
   paperStructs[paperHash].timestamp = block.timestamp;
   paperStructs[paperHash].deadline = block.timestamp;

   // save the paper hash for the owner
   ownerStructs[msg.sender].paperUploadList.push(paperHash);
   // add the IPFS hash of the paper to the paperList
   paperList.push(paperHash);
  }


  /**
   *  @author Christian Weitzel
   *  @dev 1. Add more review slots and reserve the necessary amount of money.
   *  @param paperHash IPFS hash of the paper
   *  @param newSlots amount of new slots to be added
   */
  function addReviewSlots(bytes32 paperHash, uint8 newSlots)
  public payable
  beforeDeadline(paperHash) // deadline should not be exceeded
  paperOwner(paperHash) // caller should be owner of the paper
  {
    // temporarily save the review price for further calculations
    uint reviewPrice = paperStructs[paperHash].reviewPrice;
    // check if the message value equals reviewPrice * newSlots
    require(msg.value == reviewPrice * newSlots);

    // increase the money for reviews
    paperStructs[paperHash].balance += reviewPrice * newSlots;
    // add new review slots
    paperStructs[paperHash].reviewSlots += newSlots;
  }


  /**
   *  @author Christian Weitzel
   *  @dev 1. The paper uploader can upload a new paper version and store its
   *    IPFS hash in the versions array of the paper.
   *  @param paperHash IPFS hash of the first paper version, so the identifier
   *  @param newPaperHash IPFS hash of the new updated version of the paper
   */
  function updatePaper(bytes32 paperHash, bytes32 newPaperHash)
  public
  paperOwner(paperHash) // caller should be owner of the paper
  {
    // add the new paper version to the versions array
    paperStructs[paperHash].versions.push(newPaperHash);
  }


  /**
   *  @author Christian Weitzel
   *  @dev 1. The paper uploader can change the price of the paper.
   *  @param paperHash IPFS hash of the paper
   *  @param newPrice new price for the paper. If the deadline is not yet
   *    exceeded, the paper price cannot be set higher than the reviewPrice
   */
  function updatePrice(bytes32 paperHash, uint newPrice)
  public
  paperExists(paperHash, true) // paper should exist
  paperOwner(paperHash) // caller should be owner of the paper
  {
    // if deadline is not yet exceeded
    if(paperStructs[paperHash].deadline > block.timestamp){
      // check if newPrice is lower than reviewPrice
      require(paperStructs[paperHash].reviewPrice > newPrice);
    }
    // update paper price
    paperStructs[paperHash].price = newPrice;
  }


  /**
   *  @author Christian Weitzel
   *  @dev 1. The paper uploader can change the main and sub field of a paper.
   *  @param paperHash IPFS hash of the paper
   *  @param mainField the new main field of the paper
   *  @param subField the new sub field of the paper
   */
  function updateField(bytes32 paperHash, bytes32 mainField, bytes32 subField)
  public
  paperExists(paperHash, true) // paper should exist
  paperOwner(paperHash) // caller should be owner of the paper
  {
    // update main field
    paperStructs[paperHash].mainField = mainField;
    // update sub field
    paperStructs[paperHash].subField = subField;
  }


  /**
   *  @author Christian Weitzel
   *  @dev 1. The paper uploader can extend the deadline for reviews of a paper.
   *  @param paperHash IPFS hash of the paper
   *  @param deadline the new deadline. Has to be higher than the old deadline
   */
  function updateDeadline(bytes32 paperHash, uint deadline)
  public
  paperFinalState(paperHash, false) // paper should not be final
  paperOwner(paperHash) // caller should be owner of the paper
  {
    // new deadline has to be in the future and later than the old deadline
    require(block.timestamp < deadline &&
      paperStructs[paperHash].deadline < deadline);
    paperStructs[paperHash].deadline = deadline;
  }


  /**
   *  @author Christian Weitzel
   *  @dev 2. A reviewer can send a request for making a review for a paper
   *  @param paperHash IPFS hash of the paper
   *  @param requestHash IPFS hash of the uploaded review request
   */
  function sendReviewRequest(bytes32 paperHash, bytes32 requestHash)
  public
  paperBought(paperHash) // paper should be bought
  slotsFree(paperHash) // paper should have free review slots
  beforeDeadline(paperHash) // deadline should not be exceeded
  ownerExists(msg.sender) // user should be registered
  {

    // set the data for the review request
    paperStructs[paperHash].reviewStructs[requestHash].owner = msg.sender;
    paperStructs[paperHash].reviewStructs[requestHash].requestIpfsHash = requestHash;
    paperStructs[paperHash].reviewStructs[requestHash].paper = paperHash;
    // set state to requested
    paperStructs[paperHash].reviewStructs[requestHash].state = reviewState.requested;

    // add the requestHash to the reviewList of the corresponding paper
    paperStructs[paperHash].reviewList.push(requestHash);

    // save the review request hash for the owner
    ownerStructs[msg.sender].reviewList.push(requestHash);
  }


  /**
   *  @author Christian Weitzel
   *  @dev 3. The paper uploader can accept or decline a review request. If it
   *    is accepted, a review slot will be occupied.
   *  @param paperHash IPFS hash of the paper
   *  @param requestHash IPFS hash of the review request
   *  @param accepted accept or decline the review request. TRUE = accepted
   */
  function answerReviewRequest(bytes32 paperHash, bytes32 requestHash, bool accepted)
  public
  beforeDeadline(paperHash) // deadline should not be exceeded
  paperOwner(paperHash) // caller should be owner of the paper
  // review should be in requested state
  isReviewState(paperHash, requestHash, reviewState.requested)
  {
    // when the request is answered as accepted, check if there are still free
    // review slots available
    if (accepted == true) {
      require(getReviewSlots(paperHash) > getReviewSlotsUsed(paperHash));
      // set reviewState to request accepted
      // occupy a review slot
      paperStructs[paperHash].reviewStructs[requestHash].state = reviewState.review;
      paperStructs[paperHash].reviewSlotsUsed++;
    }
    // set reviewState to request declined
    else {
      paperStructs[paperHash].reviewStructs[requestHash].state = reviewState.requestDeclined;
    }
  }


  /**
   *  @author Christian Weitzel
   *  @dev 4. The reviewer can upload his review.
   *  @param paperHash IPFS hash of the paper
   *  @param requestHash IPFS hash of the review request
   *  @param reviewHash IPFS hash of the uploaded review
   */
  function uploadReview(bytes32 paperHash, bytes32 requestHash, bytes32 reviewHash)
  public
  // caller should be the uploader of the review
  reviewOwner(paperHash, requestHash)
  // review should be in review state
  isReviewState(paperHash, requestHash, reviewState.review)
  beforeDeadline(paperHash) // deadline should not be exceeded
  {
    // save the review
    paperStructs[paperHash].reviewStructs[requestHash].reviewIpfsHash = reviewHash;
    //update the reviewState to review uploaded
    paperStructs[paperHash].reviewStructs[requestHash].state = reviewState.uploaded;
    // save the timestamp
    paperStructs[paperHash].reviewStructs[requestHash].timestamp = block.timestamp;
  }


  /**
   *  @author Christian Weitzel
   *  @dev 5. The paper uploader can accept or decline an uploaded review.
   *    If the review is accepted, transfer the payment to the reviewer.
   *    If it is declined, the occupied review slot is now free again.
   *  @param paperHash IPFS hash of the paper
   *  @param requestHash IPFS hash of the review request
   *  @param accepted accept or decline the review. TRUE = accepted
   */
  function answerReview(bytes32 paperHash, bytes32 requestHash, bool accepted)
  public
  // review should be in uploaded state
  isReviewState(paperHash, requestHash, reviewState.uploaded)
  paperOwner(paperHash) // caller should be owner of the paper
  reviewExists(paperHash, requestHash) // review should exist
  {
    // if the review was not answered for more than 4 weeks or if the answer was
    // set to accepted
    if (paperStructs[paperHash].reviewStructs[requestHash].timestamp < block.timestamp - 4 weeks
    || accepted == true) {
      // set review state to accepted
      paperStructs[paperHash].reviewStructs[requestHash].state = reviewState.reviewAccepted;
      // the review money is transferred to the reviewer
      sendReviewPayment(paperHash, requestHash);
      // if sendReviewPayment was successful, set review state to payment succeeded
      paperStructs[paperHash].reviewStructs[requestHash].state = reviewState.paymentSucceeded;
    }
    // answer the review to be declined
    else if (accepted == false) {
      // the used review slot is now free again
      paperStructs[paperHash].reviewSlotsUsed--;
      // set review state to review declined
      paperStructs[paperHash].reviewStructs[requestHash].state = reviewState.reviewDeclined;
    }
  }


  /**
   *  @author Christian Weitzel
   *  @dev 5. Send the payment for a finished review. if the uploader did not
   *    accept or decline the uploaded review for more than 4 weeks, the reviewer
   *    will always get the review paid.
   *  @param paperHash IPFS hash of the paper
   *  @param requestHash IPFS hash of the review request
   */
  function sendReviewPayment(bytes32 paperHash, bytes32 requestHash)
  public
  requestExists(paperHash, requestHash) // request should exist
  {
    // the review has to be accepted or 4 weeks need to have passed
    require(paperStructs[paperHash].reviewStructs[requestHash].state == reviewState.reviewAccepted
    || (paperStructs[paperHash].reviewStructs[requestHash].state == reviewState.uploaded &&
    paperStructs[paperHash].reviewStructs[requestHash].timestamp > block.timestamp - 4 weeks));

    // there still has to be money left in the contract. this is just for security
    // purposes, not really needed.
    require(paperStructs[paperHash].balance >= paperStructs[paperHash].reviewPrice);
    //update the reviewState to review uploaded
    paperStructs[paperHash].reviewStructs[requestHash].state = reviewState.paymentSucceeded;
    // take the money for one review from the paper balance
    paperStructs[paperHash].balance -= paperStructs[paperHash].reviewPrice;
    // transfer the money
    paperStructs[paperHash].reviewStructs[requestHash].owner.transfer(paperStructs[paperHash].reviewPrice);
  }


  /**
   *  @author Christian Weitzel
   *  @dev 6. The paper uploader can set finalState of a paper to TRUE, if the
   *    deadline is exceeded and all uploaded reviews are answered. The remaining
   *    money stored in "balance" of the paper, will be returned to the paper
   *    uploader.
   *  @param paperHash IPFS hash of the paper
   */
  // set the uploaded paper to finalState - cannot be changed anymore
  function setFinalState(bytes32 paperHash)
  public
  paperFinalState(paperHash, false) // paper should not be final
  paperOwner(paperHash) // caller should be owner of the paper
  {
    // if the deposit is not spent, check the review states
    if(paperStructs[paperHash].balance > 0) {
      // if the balance does not equal (reviewSlots - reviewSlotsUsed) * reviewPrice
      if(paperStructs[paperHash].balance !=
      (getReviewSlotsFree(paperHash)
      * paperStructs[paperHash].reviewPrice)){
        // iterate through all reviews and handle illegal reviewStates
        for(uint i = 0; i < paperStructs[paperHash].reviewList.length; i++) {
          // if a review was uploaded and not yet answered, revert.
          require(paperStructs[paperHash].reviewStructs[paperStructs[paperHash].reviewList[i]].state
          != reviewState.uploaded);
          // if the deadline is exceeded, set every unfinished review to reviewDeclined
          if(paperStructs[paperHash].reviewStructs[paperStructs[paperHash].reviewList[i]].state
          == reviewState.review){
            // if the deadline is not exceeded, revert because there is at least
            // one ongoing review that can not be declined this way.
            require(paperStructs[paperHash].deadline < block.timestamp);
            paperStructs[paperHash].reviewStructs[paperStructs[paperHash].reviewList[i]].state
            = reviewState.reviewDeclined;
          }
        }
      }
      // if the balance equals (reviewSlots - reviewSlotsUsed) * reviewPrice,
      // that means that there are no reviews in illegal reviewStates.

      // transfer the money
      msg.sender.transfer(paperStructs[paperHash].balance);
      // set the balance to 0
      paperStructs[paperHash].balance = 0;
    }

    // set finalState to TRUE
    paperStructs[paperHash].finalState = true;
  }


  // --- Buy Paper -------------------------------------------------------------


  /**
   *  @author Christian Weitzel
   *  @dev A user can buy a paper and pay the defined price or more for it.
   *  @param paperHash IPFS hash of the paper
   */
  function buyPaper(bytes32 paperHash)
  public payable
  paperExists(paperHash, true) // paper should exist
  ownerExists(msg.sender) // user should be registered
  {
    // if paper exists, check that transferred ether is equal or bigger than the
    // price
    require(msg.value >= paperStructs[paperHash].price);
    // transfer ether to paper Owner
    paperStructs[paperHash].owner.transfer(msg.value);
    // save the bought paper as "bought" for the buyer
    ownerStructs[msg.sender].paperBoughtList.push(paperHash);
  }
}
