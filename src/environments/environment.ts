import { ReviewState } from '../app/model/review-state.enum';
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  appShortName: 'RC',
  appName: 'ReviewChain',
  userImgPath: 'assets/reviewchain_logo.png',
  logoImgPath: 'assets/reviewchain_logo.png',
  errors: {
    PAPER_PRICE_TOO_LOW: () => 'Prices are faulty. The money a reviewer receives has'
    + 'to be higher that the price of the paper.'
    + 'Both have to be positive.',
    REVIEW_DEADLINE_IS_IN_PAST: () => 'Deadline is in the past!',
    NEW_REVIEW_DEADLINE_BEFORE_OLD_DEADLINE: () => 'New deadline has to be set after the old one!',
    PAPER_ALREADY_EXISTS: (paperId: string) => `Paper with ID ${paperId} already exists!`,
    PAPER_DOES_NOT_EXIST: (paperId: string) => `Paper with ID ${paperId} does not exist!`,
    PAPER_IS_IN_FINAL_STATE: (paperId: string) => `Paper with id ${paperId} is in final-state and cannot be edited!`,
    CURRENT_USER_IS_NOT_AUTHOR: (paperId: string) => `You are not the owner of the Paper with ID ${paperId}!`,
    CURRENT_USER_IS_NOT_REVIEWER: (reviewId: string) => `You are not the owner of the Review with ID ${reviewId}!`,
    REVIEW_STATE_DOES_NOT_EQUAL: (reviewId: string, wantedState: ReviewState) =>
      `Review ${reviewId} should be in ${wantedState}-State, but it isn't!`,
    REVIEW_STATE_EQUALS: (reviewId: string, illegalState: ReviewState) =>
      `Review ${reviewId} should not be in ${illegalState}-State, but it is!`,
    DEADLINE_IS_OVER: () => `The deadline of the review is already over!`,
    NO_FREE_REVIEW_SLOTS: () => `There are no unoccupied slots left for reviewing the paper!`,
    REVIEW_DOES_NOT_EXIST: (reviewId: string) => `Review with ID ${reviewId} does not exist!`,
    REVIEW_ALREADY_EXISTS: (reviewId: string) => `Review with ID ${reviewId} already exists!`,
    REQUEST_DOES_NOT_EXIST: (reviewId: string) => `Request with ID ${reviewId} does not exist!`,
    PAPER_HAS_NOT_BEEN_BOUGHT: () => `The Paper has not been bought yet!`,
  }
};
