import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IpfsService } from './services/ipfs.service';
import { Web3Service } from './services/web3.service';
import { HeaderComponent } from './components/header/header.component';
import { UserDetailsFormComponent } from './components/user-details-form/user-details-form.component';
import { UserDetailsService } from './services/user-details.service';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { AppRoutingModule } from './app.routing';
import { ProfileComponent } from './components/profile/profile.component';
import { InitContractService } from './services/init-contract.service';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { UserDetailsComponent } from './components/user-details/user-details.component';
import { UploadPaperFormComponent } from './components/upload-paper-form/upload-paper-form.component';
import { PaperService } from './services/paper.service';
import { ReviewService } from './services/review.service';
import { UploadReviewFormComponent } from './components/upload-review-form/upload-review-form.component';
import { PaperListComponent } from './components/paper-list/paper-list.component';
import { PaperListItemComponent } from './components/paper-list/paper-list-item/paper-list-item.component';
import { PaperComponent } from './components/paper/paper.component';
import { ReviewComponent } from './components/review/review.component';
import { ReviewListComponent } from './components/review-list/review-list.component';
import { ReviewListItemComponent } from './components/review-list/review-list-item/review-list-item.component';
import { ReviewRequestFormComponent } from './components/review-request-form/review-request-form.component';
import { LoggedInGuard } from './guards/logged-in.guard';
import { ReviewStatesListComponent } from './components/paper/review-states-list/review-states-list.component';
import { ReviewStatesListItemComponent } from './components/paper/review-states-list/review-states-list-item/review-states-list-item.component';
import { PaperTagService } from './services/paper-tag.service';
import { EditProfileFormComponent } from './components/edit-profile-form/edit-profile-form.component';
import { UploadedPaperListComponent } from './components/uploaded-paper-list/uploaded-paper-list.component';
import { LoadingComponent } from './components/loading/loading.component';
import { LoadingService } from './services/loading.service';
import { ToastrModule } from 'ngx-toastr';
import { EditUploadedPaperComponent } from './components/edit-uploaded-paper/edit-uploaded-paper.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    UserDetailsFormComponent,
    RegisterComponent,
    HomeComponent,
    ProfileComponent,
    SidebarComponent,
    UserDetailsComponent,
    UploadPaperFormComponent,
    UploadReviewFormComponent,
    PaperListComponent,
    PaperListItemComponent,
    PaperComponent,
    ReviewComponent,
    ReviewListComponent,
    ReviewListItemComponent,
    ReviewRequestFormComponent,
    ReviewStatesListComponent,
    ReviewStatesListItemComponent,
    EditProfileFormComponent,
    UploadedPaperListComponent,
    LoadingComponent,
    EditUploadedPaperComponent
  ],
  imports: [
    BrowserAnimationsModule,
    CommonModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    ToastrModule.forRoot()
  ],
  providers: [
    IpfsService,
    Web3Service,
    InitContractService,
    UserDetailsService,
    PaperService,
    ReviewService,
    PaperTagService,
    LoggedInGuard,
    LoadingService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
