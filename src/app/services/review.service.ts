import { Injectable } from '@angular/core';
import { InitContractService } from './init-contract.service';
import { IpfsService } from './ipfs.service';
import { Paper } from '../model/paper';
import { PaperService } from './paper.service';
import { environment } from '../../environments/environment';
import { UserDetailsService } from './user-details.service';
import { Buffer } from 'buffer';
import { UploadedData } from '../model/uploaded-data';
import { Review } from '../model/review';
import { ReviewState } from '../model/review-state.enum';
import { BigNumber } from 'bignumber.js';
import { Web3Service } from './web3.service';

@Injectable({
  providedIn: 'root'
})
export class ReviewService {

  /**
   * Instance of Paper-Contract
   */
  reviewContractPromise: Promise<any>;

  constructor(
    private initContractService: InitContractService,
    private userDetailsService: UserDetailsService,
    private paperService: PaperService,
    private ipfsService: IpfsService,
    private web3Service: Web3Service
  ) {
    this.reviewContractPromise = this.initContractService.initContract('Papers');
  }

  /**
   * saves a request message to the chain and adds this message to the paper, which a user wants to review.
   *
   * @param paperId paper to which the review belongs (not as byte32)
   * @param request request message, that should be uploaded to IPFS and saved to the chain
   */
  public async sendReviewRequest(paperId: string, request: string) {

    const currentAccount = await this.userDetailsService.getCurrentEthereumAccount();

    if (await this.paperService.isPaperFinal(paperId)) {
      throw Error(environment.errors.PAPER_IS_IN_FINAL_STATE(paperId));
    }

    // check paper bought
    if (! (await this.hasPaperBeenBoughtByUser(paperId, currentAccount))) {
      throw Error(environment.errors.PAPER_HAS_NOT_BEEN_BOUGHT());
    }

    // check slots free
    if (await this.paperService.getFreeReviewSlotsOfPaper(paperId) <= 0) {
      throw Error(environment.errors.NO_FREE_REVIEW_SLOTS());
    }

    // check before deadline
    if (await this.paperService.isDeadlineOver(paperId)) {
      throw Error(environment.errors.DEADLINE_IS_OVER());
    }

    // upload the review and extract the hash and use it as an identifier
    const uploadedData: UploadedData[] = await this.ipfsService.upload(Buffer.from(request));
    const reviewId = uploadedData[0].hash;

    await this.reviewContractPromise.then(
      async (contract) => {
        const byte32PaperId = this.ipfsService.getBytes32FromIpfsHash(paperId);
        const byte32ReviewId = this.ipfsService.getBytes32FromIpfsHash(reviewId);

        await contract.sendReviewRequest(byte32PaperId, byte32ReviewId,
          {
            from: currentAccount
          });
      }
    );
  }

  /**
   * accept or decline a review request
   *
   * @param paperId not as byte32
   * @param reviewId ipfs hash of the review request (this is also the key of a request)
   * @param accept accept or decline the review request
   */
  public async answerReviewRequest(paperId: string, reviewId: string, accept: boolean) {
    // check paper exists
    if (!this.paperService.paperExists(paperId)) {
      throw Error(environment.errors.PAPER_DOES_NOT_EXIST(paperId));
    }

    const currentAccount = await this.userDetailsService.getCurrentEthereumAccount();

    // check paper owner is same as current user
    if (await this.paperService.getOwnerOfPaper(paperId) !== currentAccount) {
      throw Error(environment.errors.CURRENT_USER_IS_NOT_AUTHOR(paperId));
    }

    // check request exists
    if (!(await this.reviewRequestExists(paperId, reviewId))) {
      throw Error(environment.errors.REQUEST_DOES_NOT_EXIST(reviewId));
    }

    // check deadline
    if (await this.paperService.isDeadlineOver(paperId)) {
      throw Error(environment.errors.DEADLINE_IS_OVER());
    }

    // check reviewstate of review is set to "requested"
    if (!(await this.doesReviewStateOfReviewEqual(paperId, reviewId, ReviewState.requested))) {
      throw Error(environment.errors.REVIEW_STATE_DOES_NOT_EQUAL(reviewId, ReviewState.requested));
    }

    // check slots free
    if (await this.paperService.getFreeReviewSlotsOfPaper(paperId) <= 0) {
      throw Error(environment.errors.NO_FREE_REVIEW_SLOTS());
    }

    await this.reviewContractPromise.then(
      async (contract) => {
        const byte32PaperId = this.ipfsService.getBytes32FromIpfsHash(paperId);
        const byte32ReviewId = this.ipfsService.getBytes32FromIpfsHash(reviewId);

        await contract.answerReviewRequest(byte32PaperId, byte32ReviewId, accept,
        {
          from: currentAccount
        });
      }
    );
  }

  /**
   * Uploads a Review to a paper.
   *
   * @param paperId paper to which the review belongs (not as byte32)
   * @param reviewId review identifier (not in byte32)
   * @param review Buffer, which contains the review-file
   */
  public async uploadReviewOfPaper(paperId: string, reviewId: string, review: Buffer) {
    // check paper exists
    if (!(await this.paperService.paperExists(paperId))) {
      throw Error(environment.errors.PAPER_DOES_NOT_EXIST(paperId));
    }

    // check request exists
    if (!(await this.reviewRequestExists(paperId, reviewId))) {
      throw Error(environment.errors.REQUEST_DOES_NOT_EXIST(reviewId));
    }

    // check review exists
    if (await this.reviewExists(paperId, reviewId)) {
      throw Error(environment.errors.REVIEW_ALREADY_EXISTS(reviewId));
    }

    const currentAccount = await this.userDetailsService.getCurrentEthereumAccount();

    // check review owner is same as current user
    if (!(await this.isOwnerOfReview(currentAccount, reviewId))) {
      throw Error(environment.errors.CURRENT_USER_IS_NOT_REVIEWER(reviewId));
    }

    // check review state is "review"
    if (!(await this.doesReviewStateOfReviewEqual(paperId, reviewId, ReviewState.review))) {
      throw Error(environment.errors.REVIEW_STATE_DOES_NOT_EQUAL(reviewId, ReviewState.review));
    }

    // check deadline
    if (await this.paperService.isDeadlineOver(paperId)) {
      throw Error(environment.errors.DEADLINE_IS_OVER());
    }

    // upload review and save hash to the chain.
    const uploadedData: UploadedData[] = await this.ipfsService.upload(review);

    await this.reviewContractPromise.then(
      async (contract) => {
        const reviewIpfsHashByte32 = this.ipfsService.getBytes32FromIpfsHash(uploadedData[0].hash);
        const reviewIdByte32 = this.ipfsService.getBytes32FromIpfsHash(reviewId);
        const paperIdByte32 = this.ipfsService.getBytes32FromIpfsHash(paperId);

        await contract.uploadReview(paperIdByte32, reviewIdByte32, reviewIpfsHashByte32, {
          from: currentAccount
        });
      }
    );
  }


  /**
   * accept or decline a review
   *
   * @param paperId paper to which the review belongs (not as byte32)
   * @param reviewId review identifier (not in byte32)
   * @param accept
   */
  public async answerReview(paperId: string, reviewId: string, accept: boolean) {

    const currentAccount = await this.userDetailsService.getCurrentEthereumAccount();

    // check paper owner is same as current user
    if (await this.paperService.getOwnerOfPaper(paperId) !== currentAccount) {
      throw Error(environment.errors.CURRENT_USER_IS_NOT_AUTHOR(paperId));
    }

    // check review exists
    if (!(await this.reviewExists(paperId, reviewId))) {
      throw Error(environment.errors.REVIEW_DOES_NOT_EXIST(reviewId));
    }

    // check review state is "uploaded"
    if (!(await this.doesReviewStateOfReviewEqual(paperId, reviewId, ReviewState.uploaded))) {
      throw Error(environment.errors.REVIEW_STATE_DOES_NOT_EQUAL(reviewId, ReviewState.uploaded));
    }


    await this.reviewContractPromise.then(
      async (contract) => {
        const reviewIdByte32 = this.ipfsService.getBytes32FromIpfsHash(reviewId);
        const paperIdByte32 = this.ipfsService.getBytes32FromIpfsHash(paperId);

        await contract.answerReview(paperIdByte32, reviewIdByte32, accept,
        {
          from: currentAccount
        });
      }
    );

  }

  /**
   * returns the Review to corresponding reviewId
   * @param paperId paper to which the review belongs
   * @param reviewId review identifier (not in byte32)
   */
  public async getReviewbyId(paperId: string, reviewId: string): Promise<Review> {
    let review: Review = null;

    // check review exists
    if (!(await this.reviewRequestExists(paperId, reviewId))) {
      throw Error(environment.errors.REQUEST_DOES_NOT_EXIST(reviewId));
    }

    await this.reviewContractPromise.then(
      async (contract) => {
        review = new Review();

        const byte32PaperId = this.ipfsService.getBytes32FromIpfsHash(paperId);
        const byte32ReviewId = this.ipfsService.getBytes32FromIpfsHash(reviewId);

        // get state of Review
        const reviewState: BigNumber = await contract.getReviewState(byte32PaperId, byte32ReviewId);

        // get review of Review
        let reviewIpfsHash = await contract.getReviewIpfsHash(byte32PaperId, byte32ReviewId);
        // contract returns 0, if there is no review found. set it to null, so it is easier to check in the future
        if (reviewIpfsHash === 0) {
          reviewIpfsHash = null;
        }

        // get review timestamp
        const timeInSeconds = await contract.getTimestamp(this.ipfsService.getBytes32FromIpfsHash(paperId));
        const timestamp = new Date(timeInSeconds * 1000);

        // get review owner
        const owner = await this.getOwnerOfReview(paperId, reviewId);

        review.owner = owner;
        review.reviewState = reviewState.toNumber();
        review.requestIpfsHash = reviewId;
        review.reviewIpfsHash = reviewIpfsHash;
        review.timestamp = timestamp;
        review.paperIpfsHash = paperId;
      }
    );

    return review;
  }

  /**
   * gets a list of review-ids of reviews issued by the user
   * @param ethereumAccount
   */
  public async getReviewsOfAccount(ethereumAccount: string): Promise<string[]> {
    let reviews: string[] = [];

    await this.reviewContractPromise.then(
      async (contract) => {
        reviews = await contract.getOwnerReviewList(ethereumAccount);
      }
    );

    return reviews;
  }

  /**
   * determines, whether the specidfied ethereum account has written specified review.
   * @param ethereumAccount
   * @param reviewId not as byte32
   */
  public async isOwnerOfReview(ethereumAccount: string, reviewId: string) {
    const reviews: string[] = await this.getReviewsOfAccount(ethereumAccount);
    // we get byte32 strings from ethereum. so we need to change reviewId.
    return reviews.includes(this.ipfsService.getBytes32FromIpfsHash(reviewId));
  }

  /**
   * returns the review-state of the specified review
   * @param paperId paper to which the review belongs
   * @param reviewId review identifier (not in byte32)
   */
  public async getReviewState(paperId: string, reviewId: string): Promise<ReviewState> {
    let reviewState: ReviewState = null;

    await this.reviewContractPromise.then(
      async (contract) => {
        const byte32PaperId = this.ipfsService.getBytes32FromIpfsHash(paperId);
        const byte32ReviewId = this.ipfsService.getBytes32FromIpfsHash(reviewId);
        const reviewStateBignumber: BigNumber = await contract.getReviewState(byte32PaperId, byte32ReviewId);
        reviewState = reviewStateBignumber.toNumber();
      }
    );

    return reviewState;
  }

  /**
   * returns the owner (the reviewer) of the specified review
   *
   * @param paperId paper to which the review belongs
   * @param reviewId review identifier (not in byte32)
   */
  public async getOwnerOfReview(paperId: string, reviewId: string): Promise<string> {
    let owner: string = null;

    await this.reviewContractPromise.then(
      async (contract) => {
        const byte32PaperId = this.ipfsService.getBytes32FromIpfsHash(paperId);
        const byte32ReviewId = this.ipfsService.getBytes32FromIpfsHash(reviewId);
        const ownerAsLowercase = await contract.getReviewOwner(byte32PaperId, byte32ReviewId);
        // convert lowercase address returned by contract to checksum-address:
        owner = this.web3Service.getAddressChecksum(ownerAsLowercase);
      }
    );

    return owner;
  }

  /**
   * checks, whether a paper has been bought by specified account.
   * returns true, if the secified account has bought a the paper, false otherwise
   *
   * @param paperId paper, which should have been bought
   * @param ethAccount account, which should have bought the paper
   */
  public async hasPaperBeenBoughtByUser(paperId: string, ethAccount: string): Promise<boolean> {
    const boughtPapers: Paper[] = await this.paperService.getBoughtPapersOfAccount(ethAccount);

    for (const paper of  boughtPapers) {
      // if we've found the paper with the same ID:
      if (paper.paperIpfsHashIdentification === paperId) {
        return true;
      }
    }

    // if we havn't found anything, return false
    return false;
  }

  /**
   * checks the review state of specified paper.
   * if the review state of the paper is the same as
   * @param paperId paper to which the review belongs
   * @param reviewId review identifier (not in byte32)
   * @param reviewState reviewState the review should have
   */
  public async doesReviewStateOfReviewEqual(paperId: string, reviewId: string, reviewState: ReviewState): Promise<boolean> {
    let isStateEqual = false;
    const currentReviewState: ReviewState = await this.getReviewState(paperId, reviewId);
    if (currentReviewState === reviewState) {
      isStateEqual = true;
    }

    return isStateEqual;
  }

  /**
   * checks, if a review exists by calling getReviewExists and catching an error
   *
   * @param paperId paper to which the review belongs
   * @param reviewId review identifier (not in byte32)
   */
  public async reviewExists(paperId: string, reviewId: string) {
    let reviewExists = true;

    await this.reviewContractPromise.then(
      async (contract) => {
        const byte32PaperId = this.ipfsService.getBytes32FromIpfsHash(paperId);
        const byte32ReviewId = this.ipfsService.getBytes32FromIpfsHash(reviewId);
        try {
          await contract.getReviewExists(byte32PaperId, byte32ReviewId);
        } catch (error) {
          // the review does not exist...
          reviewExists = false;
        }
      }
    );

    return reviewExists;
  }

  /**
   * checks, if a request exists by calling getReviewRequestExists and catching an error
   *
   * @param paperId paper to which the review belongs
   * @param reviewId review identifier (not in byte32)
   */
  public async reviewRequestExists(paperId: string, reviewId: string) {
    let reviewExists = true;

    await this.reviewContractPromise.then(
      async (contract) => {
        const byte32PaperId = this.ipfsService.getBytes32FromIpfsHash(paperId);
        const byte32ReviewId = this.ipfsService.getBytes32FromIpfsHash(reviewId);
        try {
          await contract.getReviewRequestExists(byte32PaperId, byte32ReviewId);
        } catch (error) {
          // the review does not exist...
          reviewExists = false;
        }
      }
    );

    return reviewExists;
  }

  /**
   * set the paper to final,
   * which means reviews are not possible anymore.
   *
   * if we successfully set the paper to final,
   * then update the paper with updated paper-details (paper.finalState set to true)
   *
   * we need to have this method inside the review-service,
   * so we don't end up having a circular dependency, because we need to call getReviewState().
   *
   *
   * @param paperId not in byte32
   */
  public async setFinal(paperId: string) {
    // check paper exists
    if (!(await this.paperService.paperExists(paperId))) {
      throw Error(environment.errors.PAPER_DOES_NOT_EXIST(paperId));
    }

    // check final state
    if (await this.paperService.isPaperFinal(paperId)) {
      throw Error(environment.errors.PAPER_IS_IN_FINAL_STATE(paperId));
    }

    const currentUser = await this.userDetailsService.getCurrentEthereumAccount();

    // check is current user the owner of the paper
    if (!(await this.paperService.getOwnerOfPaper(paperId) === currentUser)) {
      throw Error(environment.errors.CURRENT_USER_IS_NOT_AUTHOR(paperId));
    }

    // check, if all of the current reviews of the paper are not in uploaded state.
    const reviewIds: string[] = await this.paperService.getReviewIdsOfPaper(paperId);

    for (const reviewId of reviewIds) {
      const reviewState = await this.getReviewState(paperId, reviewId);
      if ((reviewState === ReviewState.uploaded)) {
        throw Error(environment.errors.REVIEW_STATE_EQUALS(reviewId, ReviewState.uploaded));
      }
    }

    // we have to to this beforehand, because the paper is not editable, once it is set to final
    await this.reviewContractPromise.then(
      async (contract) => {
        await contract.setFinalState(this.ipfsService.getBytes32FromIpfsHash(paperId),
        {
          from: currentUser
        });
      }
    );

    // also, update the paper with finalState to true!
    const paper: Paper = await this.paperService.loadPaper(paperId);
    paper.finalsState = true;
    await this.paperService.updatePaper(paperId, paper);
  }

}
