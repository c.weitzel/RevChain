import { Injectable } from '@angular/core';
import { ToastrService } from '../../../node_modules/ngx-toastr';

/**
 * With this service you can show a toast-message.
 * It either shows an error, or it shows a success-message
 */
@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(private toastrService: ToastrService) { }


  /**
   * Displays the message as a Toast with red background to indicate failure.
   * @param message message to be shown
   */
  public showError(message: string) {
    this.toastrService.error(message);
  }


  /**
   * Displays the message as a Toast with green background to indicate success.
   * @param message message to be shown
   */
  public showSuccess(message: string) {
    this.toastrService.success(message);
  }
}
