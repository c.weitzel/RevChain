import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

/**
 * This service triggers the displaying of the loading-screen (in LoadingComponent).
 */
@Injectable()
export class LoadingService {

  /**
   * Subject, which fires true, if loading my start, and fires false, if loading may stop
   */
  loading = new Subject();

  constructor() {  }

  /**
   * Messages the subscribers of loading-subject that loading screen can be shown
   */
  startLoading() {
    this.loading.next(true);
  }

  /**
   * Messages the subscribers of loading-subject that loading screen should be hidden
   */
  stopLoading() {
    this.loading.next(false);
  }

}
