import { Injectable } from '@angular/core';

/**
 * This service provides a list of UDC codes for paper-classification.
 * this list is only a subset of the entire UDC specification.
 *
 * On the chain, the tags are saved with the values in "mainField"/"subField".
 * You can see, that both of these variables start with "0x". This is because,
 * the tags are saved as byte32 on the chain.
 *
 * this service is incomplete and should be developed further!
 * for example, the service should fetch all of the specifcations.
 */
@Injectable({
  providedIn: 'root'
})
export class PaperTagService {

  tags = [
    {
      mainField: '0x00',
      mainFieldLabel: `SCIENCE AND KNOWLEDGE. ORGANIZATION.
       COMPUTER SCIENCE. INFORMATION. DOCUMENTATION. LIBRARIANSHIP. INSTITUTIONS. PUBLICATIONS`,
      subFields: [
        {
          subField: '0x004',
          subFieldLabel: 'Computer science and technology. Computing. Data processing'
        },
        {
          subField: '0x02',
          subFieldLabel: 'Biology'
        }
      ]
    },
    {
      mainField: '0x01',
      mainFieldLabel: 'PHILOSOPHY. PSYCHOLOGY',
      subFields: [
        {
          subField: '0x159',
          subFieldLabel: 'Psychology'
        },
        {
          subField: '0x17',
          subFieldLabel: 'Moral philosophy. Ethics. Practical philosophy'
        }
      ]
    },
    {
      mainField: '0x02',
      mainFieldLabel: 'RELIGION. THEOLOGY',
      subFields: [
        {
          subField: '0x21',
          subFieldLabel: 'Prehistoric religions. Religions of early societies'
        },
        {
          subField: '0x24',
          subFieldLabel: 'Buddhism'
        }
      ]
    },
    {
      mainField: '0x03',
      mainFieldLabel: 'SOCIAL SCIENCES',
      subFields: [
        {
          subField: '0x32',
          subFieldLabel: 'Politics'
        },
        {
          subField: '0x33',
          subFieldLabel: 'Economics. Economic science'
        },
        {
          subField: '0x37',
          subFieldLabel: 'Education'
        }
      ]
    },
    {
      mainField: '0x05',
      mainFieldLabel: 'MATHEMATICS. NATURAL SCIENCES',
      subFields: [
        {
          subField: '0x51',
          subFieldLabel: 'Mathematics'
        },
        {
          subField: '0x53',
          subFieldLabel: 'Physics'
        },
        {
          subField: '0x54',
          subFieldLabel: 'Chemistry. Crystallography. Mineralogy'
        },
        {
          subField: '0x57',
          subFieldLabel: 'Biological sciences in general'
        }
      ]
    },
  ];

  constructor() { }


  /**
   * returns the list of available tags.
   */
  getTags(): {mainField: string, mainFieldLabel: string, subFields: {subField: string, subFieldLabel: string}[]}[]  {
    return this.tags;
  }

  /**
   * Returns the label to a specific subfield-number, or null, if it has not been found.
   * @param mainTagNumber
   */
  getTaglabelFromMaintag(mainTagNumber: string): string {
    for (const mainTag of this.tags) {
      if (mainTag.mainField === mainTagNumber) {
        return mainTag.mainFieldLabel;
      }
    }
    return null;
  }

  /**
   * Returns the label to a specific subfield-number, or null, if it has not been found.
   * @param subTagNumber
   */
  getTaglabelFromSubtag(subTagNumber: string): string {
    for (const mainTag of this.tags) {
      for (const subTag of mainTag.subFields) {
        if (subTag.subField === subTagNumber) {
          return subTag.subFieldLabel;
        }
      }
    }
    return null;
  }
}
