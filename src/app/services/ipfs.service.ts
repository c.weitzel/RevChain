import { Injectable } from '@angular/core';
import * as ipfsapi from 'ipfs-api';
import { Buffer } from 'buffer';
import { UploadedData } from '../model/uploaded-data';
import { DownloadedData } from '../model/downloaded-data';
import * as bs58 from 'bs58';

/**
 * This Service uploads any kind of Data to IPFS.
 * For this, I am using the following Library: https://github.com/ipfs/js-ipfs-api
 */

@Injectable()
export class IpfsService {
  private IPFS_SERVER_LINK = 'ipfs.infura.io';
  private IPFS_SERVER_PORT = '5001';
  private IPFS_SERVER_PROTOCOL = 'https';

  ipfsapi = null;

  constructor() {
    this.ipfsapi = ipfsapi(this.IPFS_SERVER_LINK, this.IPFS_SERVER_PORT, {protocol: this.IPFS_SERVER_PROTOCOL});
  }

  /**
   * Uploads Data to IPFS and returns a Promise which contains UploadadData[]
   * @param data Any kind of Object, which one wants to save to IPFS
   */
  public upload(data: Buffer): Promise<UploadedData[]> {
    return new Promise((resolve, reject) => {
      this.ipfsapi.add(data, (err, result: UploadedData[]) => {
        if (err) {
          console.error('upload to ipfs failed');
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  }

  /**
   * Returns a Promise containing DownloadedData[].
   * @param hash IPFS Hash, which is to be called
   */
  public download(hash: string): Promise<DownloadedData[]> {

    return new Promise((resolve, reject) => {
      // check whether IPFS-Hash is valid in the first place.
      // if not, then reject the promise with an error-message.
      if (!this.isIPFSHashValid(hash)) {
        reject('Invalid IPFS-Hash: ' + hash);
      }

      // get the file from IPFS using the ipfs-api library
      this.ipfsapi.get(hash, (err, files: DownloadedData[]) => {
          if (err) {
            console.error('download from ipfs failed');
            reject(err);
          } else {
            resolve(files);
          }
      });
    });
  }

  /**
   * Checks, if the Ipfs has a valid format.
   * @param ipfsHash hash which is to be validated
   * @returns true, if is not null or undefined and hash has appropriate length
   */
  private isIPFSHashValid(ipfsHash: string) {
    return ipfsHash && ipfsHash.length > 30;
  }

/**
 * **this code comes from https://ethereum.stackexchange.com/a/39961**
 *
 * stripping leading 2 bytes from 34 byte IPFS hash
 * Assume IPFS defaults: function:0x12=sha2, size:0x20=256 bits
 * E.g. "QmNSUYVKDSvPUnRLKmuxk9diJ6yS96r1TrAXzjTiBcCLAL" -->
 * "0x017dfd85d4f6cb4dcd715a88101f7b1f06cd1e009b2327a0809d01eb9c91f231"
 *
 * we need the '0x' in front of the hash, so solidity
 * interprites the string as Byte32
 *
 * @param ipfsHash
 * @returns bytes32 hex string from base58 encoded ipfs hash,
 */
  public getBytes32FromIpfsHash(ipfsHash: string): string {
    return '0x' + bs58.decode(ipfsHash).slice(2).toString('hex');
  }

  /**
   * **this code comes from https://ethereum.stackexchange.com/a/39961 **
   *
   * E.g. "0x017dfd85d4f6cb4dcd715a88101f7b1f06cd1e009b2327a0809d01eb9c91f231"
   * --> "QmNSUYVKDSvPUnRLKmuxk9diJ6yS96r1TrAXzjTiBcCLAL"
   * @param bytes32Hex
   * @returns base58 encoded ipfs hash from bytes32 hex string
   */
  public getIpfsHashFromBytes32(bytes32Hex: string): string {
    // Add our default ipfs values for first 2 bytes:
    // function:0x12=sha2, size:0x20=256 bits
    // and cut off leading "0x"
    const hashHex = '1220' + bytes32Hex.slice(2);
    const hashBytes = Buffer.from(hashHex, 'hex');
    const hashStr = bs58.encode(hashBytes);
    return hashStr;
  }

  /**
   * appends the ipfsHash to the URL of the IPFS-Server
   * @param ipfsHash
   * @returns full URL to IPFS-Hash
   */
  public getIPFSURL(ipfsHash: string) {
    return `https://gateway.ipfs.io/ipfs/${ipfsHash}`;
  }
}

/*
   db                    db
 d8'`8b                d8'`8b
`"    "'              `"    "'



         oooooooooooo
 */
