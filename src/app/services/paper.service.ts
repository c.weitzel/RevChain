import { Injectable, Injector } from '@angular/core';
import { Web3Service } from './web3.service';
import { IpfsService } from './ipfs.service';
import { PaperDetails } from '../model/paper-details';
import { UploadedData } from '../model/uploaded-data';
import { Paper } from '../model/paper';
import { DownloadedData } from '../model/downloaded-data';
import { InitContractService } from './init-contract.service';
import { environment } from '../../environments/environment';
import * as moment from 'moment';
import { UserDetailsService } from './user-details.service';
import { Buffer } from 'buffer';


/**
 * This services handles everything related to a Paper, for instance uploading and downloading a paper.
 *
 */
@Injectable()
export class PaperService {

  /**
   * Instance of Paper-Contract
   */
  paperContractPromise: Promise<any>;

  constructor(
    private initContractService: InitContractService,
    private web3Service: Web3Service,
    private ipfsService: IpfsService,
    private userDetailsService: UserDetailsService) {
    this.paperContractPromise = this.initContractService.initContract('Papers');
  }

  /**
   * Uploads a Paper to IPFS
   *
   * First we upload the File to IPFS,
   * and then we create a new Paper-Object with PaperDetails and the
   * Link to the actual Paper in ipfs.
   *
   * The caller chooses whether he wants the paper to be reviewable or not
   *
   * @param paperDetails
   * @param paper as a buffer
   * @param isPaperRevieable booelan which indicates whether a paper should be reviewable or not
   */
  public async uploadPaper(paperDetails: PaperDetails, paper: Buffer, isPaperReviewable: boolean) {

    // first check the parameters:
    if (isPaperReviewable &&
      (paperDetails.reviewPrice < 0 || paperDetails.price < 0 || paperDetails.reviewPrice <= paperDetails.price)) {
      throw Error(environment.errors.PAPER_PRICE_TOO_LOW());
    }

    if (isPaperReviewable &&
      (moment(paperDetails.reviewDeadline).isBefore(moment.now()))) {
      throw Error(environment.errors.REVIEW_DEADLINE_IS_IN_PAST());
    }

    await this.paperContractPromise.then(
      async (contract) => {
        // first upload file to IPFS

        await this.ipfsService.upload(paper).then(
        async(uploadedPaperFile: UploadedData[]) => {
          const paperId = uploadedPaperFile[0].hash;

          // check if paper exists
          if (await this.paperExists(paperId)) {
            throw Error(environment.errors.PAPER_ALREADY_EXISTS(paperId));
          }

          const paperData = new Paper();
          paperData.paperDetails = paperDetails;
          paperData.paperIpfsHash = paperId;

          const currentAccount = await this.userDetailsService.getCurrentEthereumAccount();
          paperData.owner = currentAccount;

          // then upload paper-details + IPFS hash of paper to IPFS
          await this.ipfsService.upload(Buffer.from(JSON.stringify(paperData))).then(
            async (uploadedPaper: UploadedData[]) => {

              const paperIpfsHash = uploadedPaper[0].hash;
              const paperHashBytes32 = this.ipfsService.getBytes32FromIpfsHash(paperIpfsHash);

              // now write it to the chain!
              if (isPaperReviewable) {
                await contract.uploadPaper(
                  paperHashBytes32,
                  this.web3Service.mircoetherToWei(`${paperDetails.price}`),
                  this.web3Service.mircoetherToWei(`${paperDetails.reviewPrice}`),
                  paperDetails.reviewSlots,
                  paperDetails.mainField,
                  paperDetails.subField,
                  paperDetails.reviewDeadline.getTime() / 1000,  // convert to seconds
                  {
                    from: paperData.owner,
                    value: this.web3Service.mircoetherToWei(`${paperDetails.reviewSlots * paperDetails.reviewPrice}`)
                    // value: paperDetails.reviewSlots * paperDetails.reviewPrice
                  });
              } else {
                await contract.uploadFinalPaper(
                  paperHashBytes32,
                  this.web3Service.mircoetherToWei(`${paperDetails.price}`),
                  paperDetails.mainField,
                  paperDetails.subField,
                  {
                    from: paperData.owner
                  });
              }
            }
          );
        });
      }
    );
  }

  /**
   * Download the PaperDetails from IPFS by the given key.
   * The Paper-File itself is not yet downloaded.
   * Only the Link to it is being downloaded.
   * Reviews of the paper also have to be loaded separately, because most of the time we dont need the reviews for a specific paper.
   *
   * @param paperId IPFS-Hash to Paper (not the paper-file, but the whole Paper-object)
   */
  public async loadPaper(paperId: string): Promise<Paper> {
    let paper: Paper = null;

    await this.paperContractPromise.then(
      async (contract) => {

        const byte32IpfsLinkToPaperIdentifier = this.ipfsService.getBytes32FromIpfsHash(paperId);
        const byte32IpfsLinkToLatestPaper = await contract.getNewestPaperIPFS(byte32IpfsLinkToPaperIdentifier);
        const ipfsLinkToLatestPaper = this.ipfsService.getIpfsHashFromBytes32(byte32IpfsLinkToLatestPaper);
        const paperData: DownloadedData[] = await this.ipfsService.download(ipfsLinkToLatestPaper);
        paper = JSON.parse(paperData[0].content.toString('utf-8'));
        paper.paperIpfsHashIdentification = paperId;
      }
    );

    return paper;
  }

  /**
   * updates a paper (not only the paper-file, but also the paper-details)
   *
   * we are calling different methods of the contract, if different attributes have changed.
   *
   * @param paperId not in byte32
   * @param newPaper new version of the Paper, of which every attribute is being updated.
   * @param newPaperFile new paper-file to be uploaded. can be omitted, if there is no new file to be uploaded
   */
  public async updatePaper(paperId: string, newPaper: Paper, newPaperFile: Buffer = null) {
    // check paper exists
    if (!(await this.paperExists(paperId))) {
      throw Error(environment.errors.PAPER_DOES_NOT_EXIST(paperId));
    }

    // check final state
    if (await this.isPaperFinal(paperId)) {
      throw Error(environment.errors.PAPER_IS_IN_FINAL_STATE(paperId));
    }

    const currentUser = await this.userDetailsService.getCurrentEthereumAccount();

    // check is current user the owner of the paper
    if (!(await this.getOwnerOfPaper(paperId) === currentUser)) {
      throw Error(environment.errors.CURRENT_USER_IS_NOT_AUTHOR(paperId));
    }

    // call appropriate functions and upload paper to IPFS and save the hash
    // get old version of the paper
    const oldPaper = await this.loadPaper(paperId);

    if (oldPaper.paperDetails.mainField !== newPaper.paperDetails.mainField ||
      oldPaper.paperDetails.subField !== newPaper.paperDetails.subField) {
      this.updateField(paperId, newPaper.paperDetails.mainField, newPaper.paperDetails.subField);
    }

    if (oldPaper.paperDetails.price !== newPaper.paperDetails.price) {
      this.updatePrice(paperId, newPaper.paperDetails.price);
    }

    if (oldPaper.paperDetails.reviewDeadline !== newPaper.paperDetails.reviewDeadline) {
      this.updateDeadline(paperId, newPaper.paperDetails.reviewDeadline);
    }

    if (oldPaper.paperDetails.reviewSlots !== newPaper.paperDetails.reviewSlots) {
      this.addReviewSlotsToPaperReview(paperId, newPaper.paperDetails.reviewSlots);
    }

    if (newPaperFile) {
      const uploadedPapers: UploadedData[] = await this.ipfsService.upload(newPaperFile);
      newPaper.paperIpfsHash = uploadedPapers[0].hash;
    }

    // upload new paper to ipfs
    const uploadedPaper = await this.ipfsService.upload(Buffer.from(JSON.stringify(newPaper)));
    // call contract and update paper
    await this.paperContractPromise.then(
      async (contract) => {
        const paperIdByte32 = this.ipfsService.getBytes32FromIpfsHash(paperId);
        const newPaperIdByte32 = this.ipfsService.getBytes32FromIpfsHash(uploadedPaper[0].hash);
        await contract.updatePaper(paperIdByte32, newPaperIdByte32,
        {
          from : currentUser
        });
      });
  }

  /**
   * adds additional slots to a paper during a review-process
   *
   * since we call this function in updatePaper,
   * we dont need to check whether the paper exists or
   * if the current user is the owner of the paper
   *
   * @param paperId not in byte32
   * @param newSlots number of slots, which should be added
   */
  private async addReviewSlotsToPaperReview(paperId: string, newSlots: number) {
    // check deadline
    if (await this.isDeadlineOver) {
      throw Error(environment.errors.DEADLINE_IS_OVER());
    }

    const paper = await this.loadPaper(paperId);

    await this.paperContractPromise.then(
      async (contract) => {
        await contract.addReviewSlots(this.ipfsService.getBytes32FromIpfsHash(paperId), newSlots,
        {
          value: paper.paperDetails.reviewPrice * newSlots
        });
      }
    );
  }

  /**
   * updates the price of the paper
   *
   * @param paperId not in byte32
   * @param newPrice new price of the paper
   */
  public async updatePrice(paperId: string, newPrice: number) {
    // check paper exists
    if (!(await this.paperExists(paperId))) {
      throw Error(environment.errors.PAPER_DOES_NOT_EXIST(paperId));
    }

    // check is current user the owner of the paper
    if (await this.getOwnerOfPaper(paperId) === await this.userDetailsService.getCurrentEthereumAccount()) {
      throw Error(environment.errors.CURRENT_USER_IS_NOT_AUTHOR(paperId));
    }

    // if deadline is not exceeded, then check reviewPrice > price
    if (!(await this.isDeadlineOver(paperId))) {
      const reviewPrice: number = await this.getReviewPrice(paperId);
      if (newPrice <= reviewPrice) {
        throw Error(environment.errors.PAPER_PRICE_TOO_LOW());
      }
    }


    await this.paperContractPromise.then(
      async (contract) => {
        await contract.updatePrice(this.ipfsService.getBytes32FromIpfsHash(paperId), newPrice);
      }
    );
  }

  /**
   * updates the main field and the sub field of the paper
   *
   * since we call this function in updatePaper,
   * we dont need to check whether the paper exists or
   * if the current user is the owner of the paper
   *
   * @param paperId not in byte32
   * @param newMainField new mainfield
   * @param newSubField new subfield
   */
  private async updateField(paperId: string, newMainField: string, newSubField: string) {

    await this.paperContractPromise.then(
      async (contract) => {
        await contract.updateField(this.ipfsService.getBytes32FromIpfsHash(paperId), newMainField, newSubField);
      }
    );

  }

  /**
   * updates the Deadline of the review-process of specified paper
   *
   * since we call this function in updatePaper,
   * we dont need to check whether the paper exists or
   * if the current user is the owner of the paper
   *
   * @param paperId not in byte32
   * @param deadline new Deadline
   */
  private async updateDeadline(paperId: string, deadline: Date) {
    // check deadline is in the future and later than old deadline

    const oldDeadline: Date = await this.getDeadlineOfReviewProcessOfPaper(paperId);
    if (moment(deadline).isBefore(moment()) || moment(deadline).isBefore(moment(oldDeadline))) {
      throw Error(environment.errors.NEW_REVIEW_DEADLINE_BEFORE_OLD_DEADLINE());
    }

    await this.paperContractPromise.then(
      async (contract) => {
        // use seconds, not milliseconds, as block.timestamp uses seconds
        await contract.updateDeadline(this.ipfsService.getBytes32FromIpfsHash(paperId), deadline.getTime() / 1000);
      }
    );
  }

  /**
* buy specified paper
* @param paperId not as byte32
*/
public async buyPaper(paperId: string) {
  // check paper exists
  if (!(await this.paperExists(paperId))) {
  throw Error(environment.errors.PAPER_DOES_NOT_EXIST(paperId));
  }

  // get Paper price to set as default
  const price = await this.getPriceOfPaper(paperId);


  // we have to to this beforehand, because the paper is not editable, once it is set to final
  await this.paperContractPromise.then(
  async (contract) => {
  await contract.buyPaper(this.ipfsService.getBytes32FromIpfsHash(paperId),
  {
  from: await this.userDetailsService.getCurrentEthereumAccount(),
  value: price
  });
  }
  );
  }

  /**
   * returns the full link to a paper
   * @param ipfsHash IPFS-Hash in byte32 to Paper
   * @returns full IPFS URL to Paper-file
   */
  public getURLOfPaper(ipfsHash: string): string {
    return this.ipfsService.getIPFSURL(this.ipfsService.getIpfsHashFromBytes32(ipfsHash));
  }

  /**
   * Reads the ETH-Hash/Key of the currently logged in user and calls
   * this.getPapersOfAuthor(string);
   */
  public async getPapersOfCurrentAccount(): Promise<Paper[]> {
    const accounts = await this.web3Service.getAccounts();

    return this.getPapersOfAuthor(accounts[0]);
  }

  /**
   * Returns a list of Papers of the specified Author with his/her
   * ethereum-account
   *
   * we only return PaperDetails and the Link to the actual paper.
   * This means, that the actual Paper has to be downloaded separately.
   * This is because a user may not want to look as all of the Papers at once,
   * but rather only at one specific paper at a time.
   *
   * @param ethAddress
   * @returns List of Papers of specified Ethereum-Account
   */
  public async getPapersOfAuthor(ethAddress: string): Promise<Paper[]> {
    const listOfPapers: Paper[] = [];

    await this.paperContractPromise.then(
      async (contract) => {
        // we receive byte32 IPFS-Hashes
        const paperByte32IPFSHashes: string[] = await contract.getOwnerPaperUploadList(ethAddress);
        // use IPFSService to change byte32 string to IPFS-Hash and get paper
        for (const byte32Hash of paperByte32IPFSHashes) {
          const paper: Paper = await this.loadPaper(this.ipfsService.getIpfsHashFromBytes32(byte32Hash));
          listOfPapers.push(paper);
        }
      }
    );

    return listOfPapers;
  }

  /**
   * returns the bought papers of the currently logged in user
   */
  public async getBoughtPapersOfCurrentAccount(): Promise<Paper[]> {
    return this.getBoughtPapersOfAccount(await this.userDetailsService.getCurrentEthereumAccount());
  }

  /**
   * returns the bought papers of the specified user with ethereum-address
   * @param ethAddress ethereum address of a user
   */
  public async getBoughtPapersOfAccount(ethAddress: string): Promise<Paper[]> {
    const listOfPapers: Paper[] = [];

    await this.paperContractPromise.then(
      async (contract) => {
        // we receive byte32 IPFS-Hashes
        const paperByte32IPFSHashes: string[] = await contract.getOwnerPaperBoughtList(ethAddress);
        // use IPFSService to change byte32 string to IPFS-Hash and get paper
        for (const byte32Hash of paperByte32IPFSHashes) {
          const ipfsHash = this.ipfsService.getIpfsHashFromBytes32(byte32Hash);
          // now load the actual paper-data fromIPFS with ipfsService:
          const paperData: DownloadedData[] = await this.ipfsService.download(ipfsHash);

          const paper: Paper = JSON.parse(paperData[0].content.toString('utf-8'));
          paper.paperIpfsHashIdentification = ipfsHash;

          listOfPapers.push(paper);
        }
      }
    );


    return listOfPapers;
  }

  /**
   * returns the IDs (the IPFS-Hash of a Review-Request) of the reviews of a paper
   * @param paperId (not as byte32)
   */
  public async getReviewIdsOfPaper(paperId: string): Promise<string[]> {
    const reviewIds: string[] = [];

    if (!(await this.paperExists(paperId))) {
      throw Error(environment.errors.PAPER_DOES_NOT_EXIST(paperId));
    }

    await this.paperContractPromise.then(
      async (contract) => {
        const reviewIdsByte32 = await contract.getPaperReviewList(this.ipfsService.getBytes32FromIpfsHash(paperId));
        for (let i = 0; i < reviewIdsByte32.length; i++) {
          reviewIds.push(this.ipfsService.getIpfsHashFromBytes32(reviewIdsByte32[i]));
        }
      }
    );

    return reviewIds;
  }


  /**
   * returns the total number of papers.
   */
  async getNumberOfPapers(): Promise<number> {
    let numberOfPapers = 0;

    await this.paperContractPromise.then(
      async (contract) => {
        numberOfPapers = await contract.getPaperArrayLength();
      }
    );

    return numberOfPapers;
  }

  /**
   * Returns all paper-keys.
   * We do not return all of the Papers themselves,
   * because most of the time the user does not need EVERY paper ever submitted to the chain!
   *
   * use loadPaper() to get the specific paper-data
   */
  async getAllPaperKeys(): Promise<string[]> {
    let paperKeys: string[] = [];

    await this.paperContractPromise.then(
      async (contract) => {
        paperKeys = await contract.getAllPapers();
      }
    );

    return paperKeys;
  }


  /**
   * returns ipfs-hashes to all versions of paper-files.
   * @param paperId not as byte32
   */
  async getVersionsOfPaper(paperId: string): Promise<string[]> {
    let paperVersions: string[] = [];

    if (!(await this.paperExists(paperId))) {
      throw Error(environment.errors.PAPER_DOES_NOT_EXIST(paperId));
    }

    await this.paperContractPromise.then(
      async (contract) => {
        paperVersions = await contract.getPaperVersions(this.ipfsService.getBytes32FromIpfsHash(paperId));
      }
    );

    return paperVersions;
  }


  /**
   * returns the number of versions of a paper
   * @param paperId not as byte32
   */
  async getNumberOfVersionsOfPaper(paperId: string): Promise<number> {
    let numberOfPaperVersions = 0;

    if (!(await this.paperExists(paperId))) {
      throw Error(environment.errors.PAPER_DOES_NOT_EXIST(paperId));
    }

    await this.paperContractPromise.then(
      async (contract) => {
        numberOfPaperVersions = await contract.getPaperVersionsLength(this.ipfsService.getBytes32FromIpfsHash(paperId));
      }
    );

    return numberOfPaperVersions;
  }

  /**
   * returns the ethereum-hash of the author of a paper
   * @param paperId not as byte32
   */
  async getOwnerOfPaper(paperId: string): Promise<string> {
    let author: string = null;

    if (!(await this.paperExists(paperId))) {
      throw Error(environment.errors.PAPER_DOES_NOT_EXIST(paperId));
    }

    await this.paperContractPromise.then(
      async (contract) => {
        author = await contract.getPaperOwner(this.ipfsService.getBytes32FromIpfsHash(paperId));
        // change author-string (lowercase) to checksum-string (checksum-format)
        author = this.web3Service.getAddressChecksum(author);
      }
    );

    return author;
  }

  /**
   * returns the price of the paper
   * @param paperId not as byte32
   */
  async getPriceOfPaper(paperId: string): Promise<number> {
    let price = 0;

    if (!(await this.paperExists(paperId))) {
      throw Error(environment.errors.PAPER_DOES_NOT_EXIST(paperId));
    }

    await this.paperContractPromise.then(
      async (contract) => {
        price = await contract.getPaperPrice(this.ipfsService.getBytes32FromIpfsHash(paperId));
      }
    );

    return price;
  }

  /**
   * returns the balance of the paper-review-process
   * @param paperId not as byte32
   */
  async getBalanceOfPaperReview(paperId: string): Promise<number> {
    let price = 0;

    if (!(await this.paperExists(paperId))) {
      throw Error(environment.errors.PAPER_DOES_NOT_EXIST(paperId));
    }

    await this.paperContractPromise.then(
      async (contract) => {
        price = await contract.getPaperPrice(this.ipfsService.getBytes32FromIpfsHash(paperId));
      }
    );

    return price;
  }

  /**
   * returns the total review-slots of the paper-review-process.
   * @param paperId not as byte32
   */
  async getReviewSlotsOfPaper(paperId: string): Promise<number> {
    let price = 0;

    if (!(await this.paperExists(paperId))) {
      throw Error(environment.errors.PAPER_DOES_NOT_EXIST(paperId));
    }

    await this.paperContractPromise.then(
      async (contract) => {
        price = await contract.getReviewSlots(this.ipfsService.getBytes32FromIpfsHash(paperId));
      }
    );

    return price;
  }

  /**
   * returns the total review-slots of the paper-review-process.
   * @param paperId not as byte32
   */
  async getUsedReviewSlotsOfPaper(paperId: string): Promise<number> {
    let price = 0;

    if (!(await this.paperExists(paperId))) {
      throw Error(environment.errors.PAPER_DOES_NOT_EXIST(paperId));
    }

    await this.paperContractPromise.then(
      async (contract) => {
        price = await contract.getReviewSlotsUsed(this.ipfsService.getBytes32FromIpfsHash(paperId));
      }
    );

    return price;
  }

  /**
   * returns the amount of free review-slots of the paper
   * @param paperId (not as byte32)
   */
  public async getFreeReviewSlotsOfPaper(paperId: string): Promise<number> {
    let reviewSlots = 0;

    if (!(await this.paperExists(paperId))) {
      throw Error(environment.errors.PAPER_DOES_NOT_EXIST(paperId));
    }

    await this.paperContractPromise.then(
      async (contract) => {
        reviewSlots = await contract.getReviewSlotsFree(this.ipfsService.getBytes32FromIpfsHash(paperId));
      }
    );

    return reviewSlots;
  }

  /**
   * checks, if the paper is final
   * @param paperId (not as byte32)
   */
  public async isPaperFinal(paperId: string): Promise<boolean> {
    let paperFinal = true;

    if (!(await this.paperExists(paperId))) {
      throw Error(environment.errors.PAPER_DOES_NOT_EXIST(paperId));
    }

    await this.paperContractPromise.then(
      async (contract) => {
        const paperidByte32 = this.ipfsService.getBytes32FromIpfsHash(paperId);
        paperFinal = await contract.getPaperFinalState(this.ipfsService.getBytes32FromIpfsHash(paperId));
      }
    );

    return paperFinal;
  }


  /**
   * returns the main-field of the paper
   * @param paperId (not as byte32)
   */
  public async getMainFieldOfPaper(paperId: string): Promise<string> {
    let mainField: string = null;

    if (!(await this.paperExists(paperId))) {
      throw Error(environment.errors.PAPER_DOES_NOT_EXIST(paperId));
    }

    await this.paperContractPromise.then(
      async (contract) => {
        mainField = await contract.getMainField(this.ipfsService.getBytes32FromIpfsHash(paperId));
      }
    );

    return mainField;
  }


  /**
   * returns the sub-field of the paper
   * @param paperId (not as byte32)
   */
  public async getSubFieldOfPaper(paperId: string): Promise<string> {
    let subField: string = null;

    if (!(await this.paperExists(paperId))) {
      throw Error(environment.errors.PAPER_DOES_NOT_EXIST(paperId));
    }

    await this.paperContractPromise.then(
      async (contract) => {
        subField = await contract.getSubField(this.ipfsService.getBytes32FromIpfsHash(paperId));
      }
    );

    return subField;
  }

  /**
   * returns timestamp of the paper, when it was uploaded
   * @param paperId (not as byte32)
   */
  public async getTimestampUploadedOfPaper(paperId: string): Promise<Date> {
    let timestamp: Date = null;

    if (!(await this.paperExists(paperId))) {
      throw Error(environment.errors.PAPER_DOES_NOT_EXIST(paperId));
    }

    await this.paperContractPromise.then(
      async (contract) => {
        const timeInSeconds = await contract.getTimestamp(this.ipfsService.getBytes32FromIpfsHash(paperId));
        timestamp = new Date(timeInSeconds * 1000);
      }
    );

    return timestamp;
  }

  /**
   * returns the deadline of the review-paper
   * @param paperId (not as byte32)
   */
  public async getDeadlineOfReviewProcessOfPaper(paperId: string): Promise<Date> {
    let timestamp: Date = null;

    if (!(await this.paperExists(paperId))) {
      throw Error(environment.errors.PAPER_DOES_NOT_EXIST(paperId));
    }

    await this.paperContractPromise.then(
      async (contract) => {
        const timeInSeconds = await contract.getDeadline(this.ipfsService.getBytes32FromIpfsHash(paperId));
        timestamp = new Date(timeInSeconds * 1000);
      }
    );

    return timestamp;
  }

  /**
   * returns whether or not the deadline of the review-process of the paper is over.
   * @param paperId not as byte32
   */
  public async isDeadlineOver(paperId: string): Promise<boolean> {
    const deadline: Date = await this.getDeadlineOfReviewProcessOfPaper(paperId);
    if (moment(deadline).isBefore(moment())) {
      return true;
    }

    return false;
  }

  /**
   * returns the reviewPrice of the specified paper
   * @param paperId not as byte32
   */
  public async getReviewPrice(paperId: string): Promise<number> {
    let reviewPrice = 0;
    if (!(await this.paperExists(paperId))) {
      throw Error(environment.errors.PAPER_DOES_NOT_EXIST(paperId));
    }

    await this.paperContractPromise.then(
      async (contract) => {
        reviewPrice = await contract.getReviewPrice(this.ipfsService.getBytes32FromIpfsHash(paperId));
        reviewPrice = this.web3Service.mircoetherToWei(`${reviewPrice}`);
      }
    );

    return reviewPrice;
  }

  /**
   * checks, whether a paper exists or not by trying to load the
   * timestamp of the presumably uploaded paper.
   *
   * @param paperId not as byte32
   */
  async paperExists(paperId: string): Promise<boolean> {

    let paperExists = true;

    await this.paperContractPromise.then(
      async (contract) => {
        try {
          await contract.getTimestamp(this.ipfsService.getBytes32FromIpfsHash(paperId));
        } catch (error) {
          // if getTimestamp throws an error, the paper does not exist.
          paperExists = false;
        }
      }
    );

    return paperExists;
  }

}
