import { Injectable } from '@angular/core';
import { Web3Service } from './web3.service';
import { UserDetails } from '../model/user-details';
import { IpfsService } from './ipfs.service';
import { DownloadedData } from '../model/downloaded-data';
import { UploadedData } from '../model/uploaded-data';
import { Subject } from 'rxjs';
import { InitContractService } from './init-contract.service';
import { Buffer } from 'buffer';

@Injectable()
export class UserDetailsService {

  /**
   * Subject, which fires, as soon as the userDEtails have changed
   */
  myUserDetailsChanged = new Subject<UserDetails>();
  /**
   * Contains the UserDetails-Contract
   */
  userDetailsContractPromise: Promise<any>;

  /**
   * as soon as userDetails are loaded, this variable is being filled with them.
   */
  userDetails: UserDetails;

  constructor(private initContractService: InitContractService, private ipfsService: IpfsService, private web3Service: Web3Service) {
    // init the Contract in the InitContractService.
    this.userDetailsContractPromise = this.initContractService.initContract('Papers');
  }

  /**
   * returns the loaded UserDetails
   */
  public getUserDetails(): UserDetails {
    return this.userDetails;
  }

  /**
   * Saves the UserDetails to IPFS and saves the Link to it to the chain.
   * @returns IPFS-Hash
   */
  public async setUserDetails(userDetails: UserDetails) {
    let ipfsHash = null;

    await this.userDetailsContractPromise.then(
      async (contract) => {

        await this.ipfsService.upload(Buffer.from(JSON.stringify(userDetails))).then(
          // as soon as upload is done, save the IPFS link to the Chain
          async (_ipfsHash: UploadedData[]) => {
            ipfsHash = _ipfsHash[0].hash;
            const currentAccount = await this.getCurrentEthereumAccount();
            await contract.setUserDetailsIPFS(this.ipfsService.getBytes32FromIpfsHash(ipfsHash), {from: currentAccount});
            this.myUserDetailsChanged.next(userDetails);
            this.userDetails = userDetails;
          });
      }
    );

    return ipfsHash;
  }

  /**
   *
   * Reads the IPFS-Hash from the Blockchain according to the currently signed-in user (via MetaMask)
   * With the hash, we are downloading the file from IPFS.
   * This file is parsed as a UserDetails object and is returned to the caller.
   */
  public async loadUserDetailsOfCurrentAccount(): Promise<UserDetails> {
    let userDetails: UserDetails;

    const account = await this.getCurrentEthereumAccount();
    userDetails = await this.loadUserDetailsOf(account);
    return userDetails;
  }


  /**
   * Loads UserDetails of a specific user with according ETH-Account.
   * @param account ETH-Account Hash (of MetaMask)
   */
  public async loadUserDetailsOf(account: string): Promise<UserDetails> {
    // call getIpfsUserDetails of the Contract with the current MetaMask (actually Ethereum-) Account.
    // getIpfsUserDetails returns the most recent IPFS-Hash to the saved UserDetails.
    let details = null;
    await this.userDetailsContractPromise.then(
      async (contract) => {
        // get the IPFS link from the blockchain
        const ipfsLink = await contract.getUserDetailsIPFS(account);
        // load the UserDetails from IPFS with IPFS link from the blockchain
        await this.ipfsService.download(this.ipfsService.getIpfsHashFromBytes32(ipfsLink)).then(
          async (data: DownloadedData[]) => {
            // parse the content of the file of IPFS to JSON
            // (assume, that it has the right format)
            details = JSON.parse(data[0].content.toString('utf-8'));
            this.userDetails = details;

            // here we are letting all subscribers know, that the current logged in user has changed his details.
            // only trigger this event, if the curent account is the account in the argument
            const currentAccount = await  this.getCurrentEthereumAccount();
            if (account === currentAccount) {
              this.myUserDetailsChanged.next(details);
            }
          }
        );
      });

    return details;
  }


  /**
   * Returns the Etherreum address of the currently logged-in account.
   */
  public async getCurrentEthereumAccount(): Promise<string> {
    const accounts: string[] = await this.web3Service.getAccounts();

    return accounts[0];
  }
}
