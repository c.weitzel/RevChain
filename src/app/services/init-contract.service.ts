import { Injectable } from '@angular/core';
import { Web3Service } from './web3.service';


declare let require: any; // so you can use "require".

/**
 * This service is responsible for initiating a contract,
 * so we can use it it here.
 * Call this service, as soon as you initialize another service, which uses a contract
 */
@Injectable()
export class InitContractService {

  constructor(private web3Service: Web3Service) { }

  /**
   * Initializes a contract.
   * We need this, so we can actually use the methods of the contract
   */
  async initContract(contractName: string): Promise<any> {
    // load the artifact out of the contract-directory:
    const artifact = require(`../../../build/contracts/${contractName}.json`);

    let deployedContract = null;
    // Converts the Artifact to a contract:
    await this.web3Service.artifactsToContract(artifact).then(
      async (contract) => {
        // Get an instance of the Contract:
        deployedContract = await contract.deployed();
      }
    );

    return deployedContract;
  }

}
