import { Component, OnInit, Input } from '@angular/core';
import { Paper } from '../../model/paper';
import { ActivatedRoute, Params } from '../../../../node_modules/@angular/router';
import { Subscription } from '../../../../node_modules/rxjs';
import { PaperService } from '../../services/paper.service';
import { LoadingService } from '../../services/loading.service';
import { ReviewService } from '../../services/review.service';
import { MessageService } from '../../services/message.service';

/**
 * Displays a Form for inputting a Review Request.
 * A review Request needs to be made for a specific paper.
 */
@Component({
  selector: 'app-review-request-form',
  templateUrl: './review-request-form.component.html',
  styleUrls: ['./review-request-form.component.css']
})
export class ReviewRequestFormComponent implements OnInit {

  /**
   * Subscription which holds the route.
   * We subscribe in ngOnInit() and unsubscribe in ngOnDestroy()
   */
  routeSubscription: Subscription;
  /**
   * The paper the review is requestet for
   */
  paper: Paper;
  /**
   * Message the possible reviewer adds to the request
   */
  requestMessage: string;
  constructor(
    private paperService: PaperService,
    private reviewService: ReviewService,
    private loadingService: LoadingService,
    private activatedRoute: ActivatedRoute,
    private messageService: MessageService) { }

  ngOnInit() {
    this.routeSubscription = this.activatedRoute.params.subscribe(
      (params: Params) => {
        this.loadPaperFromIPFS(params['id']);
      }
    );

    // get the current id of the current route:
    const currentPaperId = this.activatedRoute.snapshot.params['id'];
    this.loadPaperFromIPFS(currentPaperId);
  }

  /**
   * loads a paper from IPFS with given IPFS-Hash
   * @param ipfsHash in the normal IPFS-Hash format (not in the byte32 format)
   */
  loadPaperFromIPFS(ipfsHash: string) {
    // read the ID from the Route and load the paper by this ID
    this.loadingService.startLoading();
    this.paperService.loadPaper(ipfsHash).then(
      (paper: Paper) => {
        this.paper = paper;
        // check the priviliges of the current user
        this.loadingService.stopLoading();
      }
    ).catch( // on Error stop loading!
      (error) => {
        console.error(error);
        this.loadingService.stopLoading();
      }
    );
  }
 /**
  * Send a review request with a message to the author, which the author can accept or decline.
  * To send a review the Reviewer has to buy the paper first.
  */
 async sendRequest() {
    this.loadingService.startLoading();
    try {
      await this.reviewService.sendReviewRequest(this.paper.paperIpfsHashIdentification, this.requestMessage);
      this.messageService.showSuccess('Sending successful!');
    } catch (error) {
      this.messageService.showError('Sending failed!');
      console.error(error);
    }
    this.loadingService.stopLoading();
  }
}
