import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewRequestFormComponent } from './review-request-form.component';

describe('ReviewRequestFormComponent', () => {
  let component: ReviewRequestFormComponent;
  let fixture: ComponentFixture<ReviewRequestFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewRequestFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewRequestFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
