import { Component, OnInit } from '@angular/core';
import { Review } from '../../model/review';
import { ActivatedRoute } from '../../../../node_modules/@angular/router';
import { ReviewService } from '../../services/review.service';
import { IpfsService } from '../../services/ipfs.service';
import { DownloadedData } from '../../model/downloaded-data';
import { UserDetails } from '../../model/user-details';
import { UserDetailsService } from '../../services/user-details.service';
import { MessageService } from '../../services/message.service';
import { PaperService } from '../../services/paper.service';
import { Buffer } from 'buffer';
import { LoadingService } from '../../services/loading.service';
import { ReviewState } from '../../model/review-state.enum';

/**
 * Component which displays a review with all it's attributes
 */
@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent implements OnInit {

  /**
   * current review
   */
  review: Review = null;

  /**
   * request message as text
   */
  request: string = null;

  /**
   * userDetails of the reviewer
   */
  reviewUserDetails: UserDetails = null;

  /**
   * certain functionalities are only available, if the current user is also the owner of the review.
   *
   * for example uploading a paper.
   */
  isCurrentUserOwnerOfReview = false;

  /**
   * certain functionalities are only available, if the current user is also the owner of the paper.
   *
   * for example accepting/declining a request or review
   */
  isCurrentUserOwnerOfPaper = false;

  /**
   * make enumeration available in frontend
   */
  reviewState = ReviewState;

  /**
   * review-file as a buffer, for uploading to IPFS.
   */
  reviewBuffer: Buffer = null;

  /**
   * contains the link to the uploaded review, if it exists.
   */
  reviewIpfsLink: string = null;

  /**
   * current path-param containing the paperId
   */
  currentPaperId = '';

  /**
   * current path-param containing the current reviewId
   */
  currentReviewId = '';

  constructor(
    private activatedRoute: ActivatedRoute,
    private reviewService: ReviewService,
    private ipfsService: IpfsService,
    private userDetailsService: UserDetailsService,
    private messageService: MessageService,
    private paperService: PaperService,
    private loadingServce: LoadingService) { }

  ngOnInit() {
    const snapshotParams = this.activatedRoute.snapshot.params;
    // get the paperHash out of the URL
    this.currentPaperId = snapshotParams['paper-id'];
    // get the reviewHash out of the URL
    this.currentReviewId = snapshotParams['review-id'];
    try {
      // load the review with the two Hashes
      this.reviewService.getReviewbyId(this.currentPaperId, this.currentReviewId).then(
        (review) => {
          this.review = review;
          this.loadReviewData();
        }
      );
    } catch (error) {
      this.messageService.showError(error);
    } finally {
      this.loadingServce.stopLoading();
    }
  }

  /**
   * Prepares all data needed for displaying the review.
   */
  async loadReviewData() {
    this.loadingServce.startLoading();
    try {
      // get the request message:
      this.ipfsService.download(this.currentReviewId).then(
        (downloadedData: DownloadedData[]) => {
          this.request = downloadedData[0].content.toString('utf-8');
        }
      );

      // get user details of review owner
      this.reviewUserDetails = await this.userDetailsService.loadUserDetailsOf(this.review.owner);

      // set boolean variables, which determine the permissions (which button is clickable)
      const paperOwner: string = await this.paperService.getOwnerOfPaper(this.currentPaperId);
      const currentUser =  await this.userDetailsService.getCurrentEthereumAccount();
      this.isCurrentUserOwnerOfPaper = (paperOwner === currentUser);
      this.isCurrentUserOwnerOfReview = (this.review.owner === currentUser);

      // get link of review
      if (this.review.reviewIpfsHash) {
        this.reviewIpfsLink = this.ipfsService.getIPFSURL(this.ipfsService.getIpfsHashFromBytes32(this.review.reviewIpfsHash));
      }
    } catch (error) {
      console.error(error);
      this.messageService.showError(error);
      this.loadingServce.stopLoading();
    } finally {
      this.loadingServce.stopLoading();
    }
  }

  /**
   * depending on the current review State, accepts the reviewRequest or the Review
   * only available, if reviewState == requested or uploaded and current user is owner of the paper
   */
  async accept() {
    this.loadingServce.startLoading();
    try {
      if (this.review.reviewState === ReviewState.requested) {
        await this.reviewService.answerReviewRequest(this.currentPaperId, this.currentReviewId, true);
        this.messageService.showSuccess('Request has been accepted!');
      } else {
        await this.reviewService.answerReview(this.currentPaperId, this.currentReviewId, true);
        this.messageService.showSuccess('Review has been accepted!');
      }
    } catch (error) {
      console.error(error);
      this.messageService.showError(error);
    } finally {
      this.loadingServce.stopLoading();
    }
  }

  /**
   * depending on the current review State, accepts the reviewRequest or the Review
   * only available, if reviewState == requested or uploaded and current user is owner of the paper
   */
  async decline() {
    this.loadingServce.startLoading();
    try {
      if (this.review.reviewState === ReviewState.requested) {
        await this.reviewService.answerReviewRequest(this.currentPaperId, this.currentReviewId, false);
        this.messageService.showSuccess('Request has been declined!');
      } else {
        await this.reviewService.answerReview(this.currentPaperId, this.currentReviewId, false);
        this.messageService.showSuccess('Review has been declined!');
      }
    } catch (error) {
      console.error(error);
      this.messageService.showError(error);
    } finally {
      this.loadingServce.stopLoading();
    }
  }

  /**
   * uploads a review.
   * only available, if reviewState == review and if current user is the owner of the review
   */
  async sendReview() {
    await this.loadingServce.startLoading();
    try {
      await this.reviewService.uploadReviewOfPaper(this.currentPaperId, this.currentReviewId, this.reviewBuffer);
      this.messageService.showSuccess('Successfully uploaded review!');
    } catch (error) {
      console.error(error);
      this.messageService.showError(error);
    } finally {
      this.loadingServce.stopLoading();
    }
  }


  /**
   * On uploading a File, set reviewBuffer to the uploaded file as a buffer
   */
  setPaperFile(files: FileList) {
    const file: File = files.item(0);
    const fileReader: FileReader = new FileReader();

    fileReader.onload = (event) => {
      this.reviewBuffer = Buffer.from(event.target.result);
    };

    fileReader.readAsText(file);
  }

}
