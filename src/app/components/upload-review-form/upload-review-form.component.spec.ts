import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadReviewFormComponent } from './upload-review-form.component';

describe('UploadReviewFormComponent', () => {
  let component: UploadReviewFormComponent;
  let fixture: ComponentFixture<UploadReviewFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadReviewFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadReviewFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
