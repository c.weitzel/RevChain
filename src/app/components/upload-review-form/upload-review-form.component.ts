import { Component, OnInit, Input } from '@angular/core';
import { Review } from '../../model/review';
import { Paper } from '../../model/paper';
import { ReviewService } from '../../services/review.service';
import { PaperService } from '../../services/paper.service';
import { Subscription } from '../../../../node_modules/rxjs';
import { ActivatedRoute, Params } from '../../../../node_modules/@angular/router';
import { IpfsService } from '../../services/ipfs.service';
import { LoadingService } from '../../services/loading.service';
import { Buffer } from 'buffer';

/**
 * This component contains a form for handling the upload of a review.
 * A review is specifically made for a paper. therefore we need to have a reference to a paper
 */
@Component({
  selector: 'app-upload-review-form',
  templateUrl: './upload-review-form.component.html',
  styleUrls: ['./upload-review-form.component.css']
})
export class UploadReviewFormComponent implements OnInit {

  /**
   * reference to the paper, for which the review should be used.
   */
  paper: Paper;

  /**
   * actual review as a file
   */
  reviewFile: Buffer = null;

  /**
   * Subscription which holds the route.
   * We subscribe in ngOnInit() and unsubscribe in ngOnDestroy()
   */
  routeSubscription: Subscription;

  /**
   * Review-instance containing more information regarding the review
   */
  review: Review = new Review();

  constructor(
    private reviewService: ReviewService,
    private paperService: PaperService,
    private activatedRoute: ActivatedRoute,
    private loadingService: LoadingService,
    private ipfsService: IpfsService
  ) { }

  ngOnInit() {
    this.routeSubscription = this.activatedRoute.params.subscribe(
      (params: Params) => {
        this.loadPaperFromIPFS(params['id']);
      }
    );

    // get the current id of the current route:
    const currentPaperId = this.activatedRoute.snapshot.params['id'];
    this.loadPaperFromIPFS(currentPaperId);
  }

  /**
   * loads a paper from IPFS with given IPFS-Hash
   * @param ipfsHash in the normal IPFS-Hash format (not in the byte32 format)
   */
  loadPaperFromIPFS(ipfsHash: string) {
    // read the ID from the Route and load the paper by this ID
    // this.loadingService.startLoading();
    this.paperService.loadPaper(ipfsHash).then(
      (paper: Paper) => {
        this.paper = paper;
        this.loadingService.stopLoading();
      }
    ).catch( // on Error stop loading!
      (error) => {
        console.error(error);
        this.loadingService.stopLoading();
      }
    );
  }
/**
   * On uploading a File, set this.reviewFile to the uploaded file
   */
  setReviewFile(files: FileList) {
    const file: File = files.item(0);
    const fileReader: FileReader = new FileReader();

    fileReader.onload = (event) => {
      this.reviewFile = Buffer.from(event.target.result);
    };

    fileReader.readAsText(file);
  }
/**
 * Uploads a review to IPFS and adds it to the reviews of the specific paperHash
 * so that the review is listed in paper reviews of that paper.
 */
uploadReview() {
    this.loadingService.startLoading();
    this.review.reviewIpfsHash = this.ipfsService.getIpfsHashFromBytes32(this.review.reviewIpfsHash);
    this.paper.paperIpfsHash = this.ipfsService.getIpfsHashFromBytes32(this.paper.paperIpfsHash);
    this.reviewService.uploadReviewOfPaper(this.paper.paperIpfsHash, this.review.reviewIpfsHash, this.reviewFile);
    this.loadingService.stopLoading();
  }
}
