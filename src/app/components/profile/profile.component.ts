import { Component, OnInit, OnDestroy } from '@angular/core';
import { Paper } from '../../model/paper';
import { Review } from '../../model/review';
import { User } from '../../model/user';
import { Subscription } from 'rxjs';
import { UserDetailsService } from '../../services/user-details.service';
import { UserDetails } from '../../model/user-details';
import { ActivatedRoute, Params } from '@angular/router';
import { PaperService } from '../../services/paper.service';
import { ReviewService } from '../../services/review.service';
import { LoadingService } from '../../services/loading.service';


/**
 * Component which contains all the neccessary components, which resemble a user-profile.
 * Specifically:
 * * UserDetailsComponent
 * * PaperListComponent
 * * ReviewListComponent
 *
 */
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit, OnDestroy {

  /**
   * String containing the ETH-Account.
   * This string is set in ngOnInit() and the value is taken from the URL
   */
  userAccount: string;

  /**
   * User instance corresponding to the userAccount
   */
  user: User = new User();

  /**
   * Subscription which holds the route.
   * We subscribe in ngOnInit() and unsubscribe in ngOnDestroy()
   */
  routeSubscription: Subscription;
  /**
   *
   * @param userDetailsService For loading the UserDetails from the Chain.
   */
  constructor(
    private userDetailsService: UserDetailsService,
    private route: ActivatedRoute,
    private paperService: PaperService,
    private reviewService: ReviewService,
    private loadingService: LoadingService) {  }

  ngOnInit(): void {
    this.routeSubscription = this.route.params.subscribe(
      (params: Params) => {
        this.userAccount = params['eth-account'];
        this.loadUser();
      }
    );

    // set id, whether it exists or not.
    this.userAccount = this.route.snapshot.params['eth-account'];

    this.loadUser();
  }

  /**
   * Load the attributes of the user.
   */
  loadUser() {
    this.loadingService.startLoading();
    try {
      this.loadDetails();
      this.loadBoughtPapers();
      this.loadUploadedPapers();
      this.loadReviews();
    } finally {
      this.loadingService.stopLoading();
    }
  }

  /**
   * Load user-details of the user with the current userAccount from the Chain.
   */
  private loadDetails() {
    this.userDetailsService.loadUserDetailsOf(this.userAccount).then(this.setUserDetails.bind(this));
  }

  /**
   * Load all papers uploaded by the current user
   */
  private loadUploadedPapers() {
    this.paperService.getPapersOfAuthor(this.userAccount).then(this.setUploadedPapers.bind(this));
  }

  /**
   * @param papers papers to be set
   */
  private setUploadedPapers(papers: Paper[]) {
    // copy elements of papers
    this.user.paperUploadList = [...papers];
  }

  /**
   *
   * @param userDetails userdetails to be set
   */
  private setUserDetails(userDetails: UserDetails) {
    // copy attributes of userDetails
    this.user.userProfile = Object.assign({}, userDetails);
  }

  /**
   * Loads the bought papers of the user and sets them.
   */
  private loadBoughtPapers() {
    this.paperService.getBoughtPapersOfCurrentAccount().then(this.setBoughtPapers.bind(this));
  }

  /**
   * Loads reviews from the Chain and sets them to the user.
   */
  private loadReviews() {
    this.reviewService.getReviewsOfAccount(this.userAccount).then(this.setReviews.bind(this));
  }

  /**
   *
   * @param reviews Sets the reviews of the currentAccount
   */
  private setReviews(reviews: Review[]) {
    this.user.reviewList = [...reviews];
  }
  /**
   *
   * @private
   * @param {Paper[]} boughtPapers Sets the bought papers of the currentAccount
   */
  private setBoughtPapers(boughtPapers: Paper[]) {
    this.user.paperBoughtList = [...boughtPapers];
  }

  ngOnDestroy(): void {
    this.routeSubscription.unsubscribe();
  }

}
