import { Component, OnInit } from '@angular/core';

/**
 * This component contains a simple welcome-message.  
 * In further releases it should contain the following things:
 * * Feed - new Papers by category
 * * Status - status of Papers which are currently being reviewed
 * 
 * right now it contains dummy-stuff which should be deleted
 */
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  constructor() {
  }

  ngOnInit() { }

}
