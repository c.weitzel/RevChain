import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserDetails } from '../../model/user-details';
import { environment } from '../../../environments/environment';
import { Subscription } from 'rxjs';
import { UserDetailsService } from '../../services/user-details.service';

/**
 * This Component shows the app-title and the currently logged in user.
 * 
 */
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {

  /**
   * contains the current Ethereum Address of the currently logged in user.
   * we need this for linking the profile
   */
  currentEthAccount: string = null;

  /**
   * Contains information about the currently logged in user.
   */
  userDetails: UserDetails = null;

  /**
   * Short-name for smaller screens/collapsed sidebar
   */
  appShortName = environment.appShortName;

  /**
   * app name for fanciness
   */
  appName = environment.appName;

  /**
   * Fancy avatar for a user
   */
  userImgPath = environment.userImgPath;

  /**
   * Subcription, which holds userDetailsService.myUserDetailsChanged.
   * We subscribe in ngOnInit() and unsubscribe in ngOnDestroy()
   */
  userDetailsSubscription: Subscription;
  
  /**
   * 
   * @param userDetailsService For loading the user-details of the current logged-in user
   */
  constructor(private userDetailsService: UserDetailsService) { }

  ngOnInit() {
    // subscribe to the userDetails and listen to changes
    this.userDetailsSubscription = this.userDetailsService.myUserDetailsChanged.subscribe(
      (userDetails: UserDetails) => {
        // copy the attributes
        this.userDetails = Object.assign({}, userDetails);

        this.userDetailsService.getCurrentEthereumAccount().then(
          (account: string) => {
            this.currentEthAccount = account;
          }
        );
      }
    );

    // load the details
    this.loadDetails();
  }

  /**
   * load the userdetails with the userDetailsService
   */
  private loadDetails() {
    this.userDetailsService.loadUserDetailsOfCurrentAccount().then(
      (userDetails: UserDetails) => {
        this.userDetails = Object.assign({}, userDetails);
      }
    );
  }

  ngOnDestroy(): void {
    this.userDetailsSubscription.unsubscribe();
  }
}
