import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadedPaperListComponent } from './uploaded-paper-list.component';

describe('UploadedPaperListComponent', () => {
  let component: UploadedPaperListComponent;
  let fixture: ComponentFixture<UploadedPaperListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadedPaperListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadedPaperListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
