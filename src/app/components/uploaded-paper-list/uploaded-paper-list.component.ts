import { Component, OnInit } from '@angular/core';
import { PaperService } from '../../services/paper.service';
import { LoadingService } from '../../services/loading.service';
import { Paper } from '../../model/paper';
import { IpfsService } from '../../services/ipfs.service';

@Component({
  selector: 'app-uploaded-paper-list',
  templateUrl: './uploaded-paper-list.component.html',
  styleUrls: ['./uploaded-paper-list.component.css']
})
export class UploadedPaperListComponent implements OnInit {
  /**
   * Stores the Hashes of all uploaded papers
   */
  paperHashes: string[];
  /**
   *Stores the actual papers which are uploaded
   */
  paperList: Paper[] = [];

  constructor(
    private paperService: PaperService,
    private loadingService: LoadingService,
    private ipfsService: IpfsService
  ) { }

  ngOnInit() {
    this.loadPapers();
  }
  /**
   * Load all papers that are uploaded with the method loadPaperHashes
   */
  loadPapers() {
    this.loadingService.startLoading();
    try {
      this.loadPaperHashes();
    } finally {
      this.loadingService.stopLoading();
    }
  }
/**
 * Paperhashes are loaded and then stored in the paperHashes array
 */
loadPaperHashes() {
    this.paperService.getAllPaperKeys().then(
      (paperHashes: string[]) => {
        this.setPaperHashes(paperHashes);
        this.loadPapersWithHashes();
      }
    );
  }
/**
 * For each hash of the paperHashes array the paper will be loaded and stored in the paper array
 */
loadPapersWithHashes() {
    this.paperHashes.forEach(element => {
      element = this.ipfsService.getIpfsHashFromBytes32(element);
      this.paperService.loadPaper(element).then(this.addPaperToList.bind(this));
    });
  }
  /**
   * Adds a paper to the paper array
   */
  addPaperToList(paper: Paper) {
   this.paperList.push(paper);
  }
  /**
   * Setter for the paperHashes array
   */
  setPaperHashes(hashes: string[]) {
    this.paperHashes = [...hashes];
  }
}
