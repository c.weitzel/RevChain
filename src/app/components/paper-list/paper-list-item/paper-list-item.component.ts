import { Component, OnInit, Input } from '@angular/core';
import { Paper } from '../../../model/paper';
import { PaperTagService } from '../../../services/paper-tag.service';

/**
 * displays a Paper as a list-item, which means it only displays the most important information.
 */
@Component({
  selector: 'app-paper-list-item',
  templateUrl: './paper-list-item.component.html',
  styleUrls: ['./paper-list-item.component.css']
})
export class PaperListItemComponent implements OnInit {

  /**
   * The paper,which is displayed
   */
  @Input() paper: Paper;
  /**
   * Label that is loaded from the mainfieldnumber
   */
  mainFieldLabel: string;
  /**
   * Label that is loaded from the subfieldnumber
   */
  subFieldLabel: string;

  constructor(
    private paperTagService: PaperTagService
  ) { }

  ngOnInit() {
    // Load the mainfield and the subfield form the tag number
    this.mainFieldLabel = this.paperTagService.getTaglabelFromMaintag(this.paper.paperDetails.mainField);
    this.subFieldLabel = this.paperTagService.getTaglabelFromSubtag(this.paper.paperDetails.subField);
  }

}
