import { Component, OnInit, Input } from '@angular/core';
import { Paper } from '../../model/paper';

/**
 * Contains a list of papers and displays them as paper-list-items.
 */
@Component({
  selector: 'app-paper-list',
  templateUrl: './paper-list.component.html',
  styleUrls: ['./paper-list.component.css']
})
export class PaperListComponent implements OnInit {

  /**
   * The list of papers, which are displayed
   */
  @Input() papers: Paper[] = [];

  constructor() { }

  ngOnInit() {
  }

}
