import { Component, OnInit, ViewChild, OnDestroy, Input } from '@angular/core';
import { UserDetailsService } from '../../services/user-details.service';
import { UserDetails } from '../../model/user-details';
import { Subscription } from 'rxjs';


/**
 * This components shows User-Details in a non-editable manner.
 */
@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {
  /**
   * Variable containing the userDetails.
   */
  @Input() user: UserDetails;

  constructor() {  }

  ngOnInit(): void {}

}
