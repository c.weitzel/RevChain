import { Component, OnInit, Input } from '@angular/core';
import { PaperDetails } from '../../model/paper-details';
import { PaperService } from '../../services/paper.service';
import { LoadingService } from '../../services/loading.service';
import * as moment from 'moment';
import { Buffer } from 'buffer';
import { PaperTagService } from '../../services/paper-tag.service';
import { MessageService } from '../../services/message.service';
import { Paper } from '../../model/paper';

/**
 * This Component offers a form for uploading both a reviewable and a non-revieable Paper .
 */
@Component({
  selector: 'app-upload-paper-form',
  templateUrl: './upload-paper-form.component.html',
  styleUrls: ['./upload-paper-form.component.css']
})
export class UploadPaperFormComponent implements OnInit {
  /**
   * corresponds to checkbox in template.
   * Whether or not the paper should be reviewable or not.
   */
  isPaperReviewable = true;
  /**
   * corresponds to checkbox in template
   * Whether a new version of the paper is uploaded or not.
   */
  isReupload = false;
  /**
   * Paper instance, which has the same attributes as the Paper
   */
  @Input() paper: Paper = new Paper();

  /**
   * the actual paper as a Buffer
   */
  paperFile: Buffer = null;
  /**
   * List of the possibleMainAndSubfields
   */
  paperTags: any[];
  /**
   * Array of the subtags of the selectedMaintag
   * Array fills the subtag drowdown-menu after a maintag is selected.
   */
  selectedSubTags: any[];
  /**
   * Stores the selected tag in the maintag dropdown-menu
   */
  selectedMainTag: any;
  /**
   * Stores the selected tag in the subtag dropdown-menu
   */
  selectedSubTag: any;


  /**
   * moment instance, for using it in the tempalte
   */
  moment = moment;

  /**
   *
   * @param paperService For uploading paper and paper-details
   * @param loadingService For showing a loading-screen during upload
   */
  constructor(
    private paperService: PaperService,
    private loadingService: LoadingService,
    private paperTagService: PaperTagService,
    private messageService: MessageService) {
    this.paper.paperDetails.price = 40000;
    this.paper.paperDetails.reviewPrice = 50000;
    this.paper.paperDetails.reviewSlots = 4;
    this.paper.paperDetails.mainField = this.paperTagService.getTags()[0].mainField;
    this.paper.paperDetails.subField = this.paperTagService.getTags()[0].subFields[0].subField;
    this.paper.paperDetails.reviewDeadline = moment().add(2, 'month').toDate();
  }

  ngOnInit() {
    this.loadPaperTags();
  }

  /**
   * On uploading a File, set this.paperFile to the uploaded file
   */
  setPaperFile(files: FileList) {
    const file: File = files.item(0);
    const fileReader: FileReader = new FileReader();

    fileReader.onload = (event) => {
      this.paperFile = Buffer.from(event.target.result);
    };

    fileReader.readAsText(file);
  }

  /**
   * Sets the tags in paperdetail to the selected tags
   */
  setPaperTags() {
    if (this.selectedMainTag.mainField) {
      this.paper.paperDetails.mainField = this.selectedMainTag.mainField;
    }
    if (this.selectedSubTag.subField) {
      this.paper.paperDetails.subField = this.selectedSubTag.subField;
    }
  }

  /**
   * Calls the paperService to let it handle the upload-process of a paper.
   * Also shows a loading-screen during the upload-process.
   */
  async uploadPaper() {
    this.loadingService.startLoading();
    try {
      this.setPaperTags();
      await this.paperService.uploadPaper(this.paper.paperDetails, this.paperFile, this.isPaperReviewable);
      this.messageService.showSuccess('Upload successful!');
    } catch (error) {
      this.messageService.showError(error);
      console.error(error);
    } finally {
      this.loadingService.stopLoading();
    }
  }
  /**
   * Calls the paperService to let it handle the update-process of a paper.
   * Also shows a loading-screen during the update-process.
   */
  async updatePaper() {
    this.loadingService.startLoading();
    try {
      await this.paperService.updatePaper(this.paper.paperIpfsHashIdentification, this.paper, this.paperFile);
      this.messageService.showSuccess('Update successful');
    } catch (error) {
      this.messageService.showError(error);
      console.error(error);
    } finally {
      this.loadingService.stopLoading();
    }
  }
  /**
   * Load all papertags, which are needed for filtering them
   *
   * @private
   * @memberof UploadPaperFormComponent
   */
  private loadPaperTags() {
    this.paperTags = this.paperTagService.getTags();
  }
  /**
   * Load the subfields of the selected mainfield
   *
   * @param {*} mainfield
   * @memberof UploadPaperFormComponent
   */
  updateSubField(mainfield: any) {
    this.selectedSubTags = mainfield.subFields;
  }
}
