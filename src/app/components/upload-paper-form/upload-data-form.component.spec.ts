import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadPaperFormComponent } from './upload-paper-form.component';

describe('UploadPaperFormComponent', () => {
  let component: UploadPaperFormComponent;
  let fixture: ComponentFixture<UploadPaperFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadPaperFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadPaperFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
