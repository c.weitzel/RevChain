import { Component, OnInit, OnDestroy } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Web3Service } from '../../services/web3.service';
import { Subscription } from 'rxjs';
import { UserDetailsService } from '../../services/user-details.service';
import { UserDetails } from '../../model/user-details';


/**
 * This component contains the navigation to other parts of the application.
 * Specific links are only shown, when a user is logged in.
 */
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit, OnDestroy {

  /**
   * contains the current Ethereum Address of the currently logged in user.
   * we need this for linking the profile
   */
  currentEthAccount: string = null;

  /**
   * contains a link to a small-logo for fancyness
   */
  logoImgPath = environment.logoImgPath;

  /**
   * Variable used in the template to show or hide specific links
   */
  isLoggedIn = false;

  /**
   * Subscription, which holds UserDetailsService.myUserDetailsChanged - Subject.
   * We Subscribe in ngOnInit() and unsubscribe in ngOnDestory()
   */
  userDetailsSubscription: Subscription;

  /**
   *
   * @param userDetailsService For checking, whether a user is logged in or not.
   */
  constructor(private userDetailsService: UserDetailsService) { }

  ngOnInit() {
    // we check if the user has an account
    this.userDetailsSubscription = this.userDetailsService.myUserDetailsChanged.subscribe(
      (userDetails: UserDetails) => {
        // if there are any userDetails, then the User is registrated and we show him/her the profile-button
        if (userDetails) {
          this.isLoggedIn = true;
        } else {
          this.isLoggedIn = false;
        }

        this.userDetailsService.getCurrentEthereumAccount().then(
          (account: string) => {
            this.currentEthAccount = account;
          }
        );
      }
    );
  }

  ngOnDestroy(): void {
    this.userDetailsSubscription.unsubscribe();
  }
}
