import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewStatesListComponent } from './review-states-list.component';

describe('ReviewStatesListComponent', () => {
  let component: ReviewStatesListComponent;
  let fixture: ComponentFixture<ReviewStatesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewStatesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewStatesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
