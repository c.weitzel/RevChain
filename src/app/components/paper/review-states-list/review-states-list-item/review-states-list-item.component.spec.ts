import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewStatesListItemComponent } from './review-states-list-item.component';

describe('ReviewStatesListItemComponent', () => {
  let component: ReviewStatesListItemComponent;
  let fixture: ComponentFixture<ReviewStatesListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewStatesListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewStatesListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
