import { Component, OnInit, Input } from '@angular/core';
import { Review } from '../../../../model/review';
import { UserDetailsService } from '../../../../services/user-details.service';
import { UserDetails } from '../../../../model/user-details';
import { IpfsService } from '../../../../services/ipfs.service';
import { ReviewState } from '../../../../model/review-state.enum';

/**
 * This component shows the state of the given review and defines
 * options for accepting/declining reviews/review-requests for the author of the paper
 */
@Component({
  selector: 'app-review-states-list-item',
  templateUrl: './review-states-list-item.component.html',
  styleUrls: ['./review-states-list-item.component.css']
})
export class ReviewStatesListItemComponent implements OnInit {

  /**
   * review with appropriate state
   */
  @Input() review: Review;
  /**
   * UserDetails of the owner of the review
   */
  userDetails: UserDetails;
  reviewState = ReviewState;
  constructor(
    private userDetailsService: UserDetailsService,
    private ipfsService: IpfsService
  ) { }

  ngOnInit() {
    this.setUserDetails();
  }
  /**
   * Gets the UserDetails of the review owner to display them in  the template
   *
   */
  async setUserDetails() {
     this.userDetails = await this.userDetailsService.loadUserDetailsOf(this.review.owner);
  }
}
