import { Component, OnInit, Input } from '@angular/core';
import { Paper } from '../../../model/paper';


/**
 * Contains information about the Review-States of the paper, if the paper is reviewable.
 * this component should only be shown, if the current user is the author of the viewed paper.
 */
@Component({
  selector: 'app-review-states-list',
  templateUrl: './review-states-list.component.html',
  styleUrls: ['./review-states-list.component.css']
})
export class ReviewStatesListComponent implements OnInit {

  /**
   * Paper containing the reviews
   */
  @Input() paper: Paper;

  constructor() { }

  ngOnInit() { }

}
