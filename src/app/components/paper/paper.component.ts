import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Paper } from '../../model/paper';
import { UserDetailsService } from '../../services/user-details.service';
import { PaperService } from '../../services/paper.service';
import { LoadingService } from '../../services/loading.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs';
import { PaperTagService } from '../../services/paper-tag.service';
import { IpfsService } from '../../services/ipfs.service';
import { ReviewService } from '../../services/review.service';
import { MessageService } from '../../services/message.service';
import { Review } from '../../model/review';

/**
 * This components displays detailed information about a paper.
 * It also contains a button to buy the paper and - if the paper is revieable -
 * a button to send a review-request by opening the review-request-form.
 *
 * If the currently logged-in user is the owner, then the status of  the reviews is shown.
 */
@Component({
  selector: 'app-paper',
  templateUrl: './paper.component.html',
  styleUrls: ['./paper.component.css']
})
export class PaperComponent implements OnInit, OnDestroy {

  /**
   * current Paper, of which its details are displayed
   */
  paper: Paper;

  /**
   * Subscription which holds the route.
   * We subscribe in ngOnInit() and unsubscribe in ngOnDestroy()
   */
  routeSubscription: Subscription;

  /**
   * Variable determining, if the current User is also the author of a paper.
   * We need this, so the template knows, when to show certain buttons.
   */
  currentUserAuthor = false;

   /**
   * Variable determining, if the current User is also the author of a paper.
   * We need this, so the template knows, when to show certain buttons.
   */
  currentUserBoughtPaper = false;

  /**
   * Variable with the download link to the paper.
   */
  paperURL: string;

  /**
   * Variable determining, if the paper is reviewable.
   * We need this, so the template knows, when to show certain buttons.
   */
  paperReviewable = false;
  /**
   * Variable determining, if the paper is final.
   * We need this, so the template knows, when to show certain buttons.
   */
  paperFinal = false;
  /**
   * Variable with the mainfield label which represents the mainfield number of the shown paper.
   */
  mainFieldLabel: string;
  /**
   * Variable with the subfield label which represents the subfield number of the shown paper.
   */
  subFieldLabel: string;
  /**
   *
   *
   * @type {*}
   * @memberof PaperComponent
   */
  reviewStatus: any;
  reviewersReview: Review;

  constructor(
    private userDetailsService: UserDetailsService,
    private paperService: PaperService,
    private loadingService: LoadingService,
    private activatedRoute: ActivatedRoute,
    private paperTagService: PaperTagService,
    private ipfsService: IpfsService,
    private reviewService: ReviewService,
    private messageService: MessageService
  ) { }

  ngOnInit() {
    const currentPaperId = this.activatedRoute.snapshot.params['id'];
    this.loadPaperFromIPFS(currentPaperId);
  }

  /**
   * loads a paper from IPFS with given IPFS-Hash
   * @param ipfsHash in the normal IPFS-Hash format (not in the byte32 format)
   */
  loadPaperFromIPFS(ipfsHash: string) {
    // read the ID from the Route and load the paper by this ID
    this.loadingService.startLoading();
    this.paperService.loadPaper(ipfsHash).then(
      (paper: Paper) => {
        this.paper = paper;
        // check the priviliges of the current user
        this.isCurrentUserAuthor();
        this.didCurrentUserBuy();
        // check whether there are reviews for the paper
        this.isPaperReviewable();
        // check the paper status final
        this.isPaperFinal();
        // set the labels for the fields
        this.setTagLabels();
        // get all reviews of the paper
        this.loadReviewsOfPaper().then(
          () => {
            this.loadReviewStatus();
          }
        );
        this.paperURL = this.ipfsService.getIPFSURL(this.paper.paperIpfsHash);
        this.loadingService.stopLoading();
      }
    ).catch( // on Error stop loading!
      (error) => {
        console.error(error);
        this.loadingService.stopLoading();
      }
    );
  }
  /**
   * gets the reviews of the paper by the paperhash and the reviewhash
   * is needed to display the reviews of a paper properly
   */
  async loadReviewsOfPaper() {
    this.paper.reviews = [];
    const reviewHashList = await this.paperService.getReviewIdsOfPaper(this.paper.paperIpfsHashIdentification);
    for (let i = 0; i < reviewHashList.length; i++) {
      this.paper.reviews.push(await this.reviewService.getReviewbyId(this.paper.paperIpfsHashIdentification, reviewHashList[i]));
    }
  }
  /**
   * Gets the review status of the reviews so the reviewer can see
   * whether the request for example is accepted or declined
   */
  async loadReviewStatus() {
    const owner = await this.userDetailsService.getCurrentEthereumAccount();
    for (const review of this.paper.reviews) {
      if (review.owner === owner) {
        this.reviewStatus = review.reviewState;
        this.reviewersReview = review;
      }
    }
  }
  /**
   * Gets the labels of the main and subfield numbers to display them in the template
   */
  setTagLabels() {
    this.mainFieldLabel = this.paperTagService.getTaglabelFromMaintag(this.paper.paperDetails.mainField);
    this.subFieldLabel = this.paperTagService.getTaglabelFromSubtag(this.paper.paperDetails.subField);
  }


  /**
   * checks, whether the currently logged in user is the author of the currently viewed paper.
   */
  async isCurrentUserAuthor() {
    const currentEthereumAccount = await this.userDetailsService.getCurrentEthereumAccount();
    this.currentUserAuthor = (this.paper && (currentEthereumAccount === this.paper.owner));
  }
  /**
   * checks whether the currently logged in user has already bought the viewed paper.
   */
  async didCurrentUserBuy() {
    const currentEthereumAccount = await this.userDetailsService.getCurrentEthereumAccount();
    this.currentUserBoughtPaper = await this.reviewService.hasPaperBeenBoughtByUser(
      this.paper.paperIpfsHashIdentification, currentEthereumAccount
    );
  }

  /**
   * a paper is reviewable, if the following conditions are met:
   * * current user is not the author
   * * current user is signed in
   * * there is at least one unoccupied review-slot
   * * the current user has not issued a request for the same paper before
   * *
   */
  async isPaperReviewable() {
    this.paperReviewable = (this.paper &&
      (!this.currentUserAuthor &&
      this.userDetailsService.getUserDetails() &&
      await this.paperService.getFreeReviewSlotsOfPaper(this.paper.paperIpfsHashIdentification) > 0 &&
      !(await this.paperService.isPaperFinal(this.paper.paperIpfsHashIdentification))));
  }
  /**
   * To check whether the paper is final.
   * If the paper is final only the buy button is visible for other users than the author.
   */
  async isPaperFinal() {
    this.paperFinal = await this.paperService.isPaperFinal(this.paper.paperIpfsHashIdentification);
  }
  /**
   * User buys the paper he is currently visiting.
   */
  async buyPaper() {
    this.loadingService.startLoading();
    try {
      await this.paperService.buyPaper(this.paper.paperIpfsHashIdentification);
      this.loadingService.stopLoading();
      this.messageService.showSuccess('Paper bought!');
    } catch (error) {
      console.error(error);
      this.loadingService.stopLoading();
      this.messageService.showError('Buying failed!');
    }
  }
  /**
   * Author sets the paper he is currently visiting to final.
   * The reviews have to be finished and answered.
   * Afterwards no more new reviews and request can be added.
   */
  async setPaperToFinal() {
    this.loadingService.startLoading();
    try {
      await this.reviewService.setFinal(this.paper.paperIpfsHashIdentification);
      this.loadingService.stopLoading();
      this.messageService.showSuccess('Paper set to final!');
    } catch (error) {
      console.error(error);
      this.loadingService.stopLoading();
      this.messageService.showError('Error: Setting to final failed!');
    }
  }

  ngOnDestroy(): void {
  }

}
