import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditUploadedPaperComponent } from './edit-uploaded-paper.component';

describe('EditUploadedPaperComponent', () => {
  let component: EditUploadedPaperComponent;
  let fixture: ComponentFixture<EditUploadedPaperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditUploadedPaperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditUploadedPaperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
