import { Component, OnInit } from '@angular/core';
import { Paper } from '../../model/paper';
import { PaperService } from '../../services/paper.service';
import { ActivatedRoute } from '../../../../node_modules/@angular/router';
import { LoadingService } from '../../services/loading.service';

@Component({
  selector: 'app-edit-uploaded-paper',
  templateUrl: './edit-uploaded-paper.component.html',
  styleUrls: ['./edit-uploaded-paper.component.css']
})
export class EditUploadedPaperComponent implements OnInit {
  /**
   * Instance of paper that is loaded by routerLink
   * and passed on to the upload-paper-form-.component to edit it
   *
   * @type {Paper}
   * @memberof EditUploadedPaperComponent
   */
  paper: Paper = new Paper();
  constructor(
    private paperService: PaperService,
    private activatedRoute: ActivatedRoute,
    private loadingService: LoadingService
  ) { }

  ngOnInit() {
    const currentPaperId = this.activatedRoute.snapshot.params['id'];
    this.loadPaperFromIPFS(currentPaperId);
  }
/**
 * Method loads the paper of the IPFS-Hash out of IPFS
 *
 * @private
 * @param {string} ipfsHash
 * @memberof EditUploadedPaperComponent
 */
private async loadPaperFromIPFS(ipfsHash: string) {
    this.loadingService.startLoading();
    await this.paperService.loadPaper(ipfsHash).then(
      (paper: Paper) => {
        this.paper = paper;
        this.loadingService.stopLoading();
      }
    ).catch( // on Error stop loading!
      (error) => {
        console.error(error);
        this.loadingService.stopLoading();
      }
    );
  }
}
