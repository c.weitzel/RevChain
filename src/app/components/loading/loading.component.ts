import { Component, OnInit } from '@angular/core';

/**
 * This component is embedded in AppComponent and is shown, as soon as LoadingService emitts "true".
 * It shows a loading animation via CSS.
 */
@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.css']
})
export class LoadingComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
