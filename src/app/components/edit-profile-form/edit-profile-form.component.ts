import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserDetails } from '../../model/user-details';
import { UserDetailsService } from '../../services/user-details.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs';
import { User } from '../../model/user';
import { Review } from '../../model/review';
import { Paper } from '../../model/paper';
import { PaperService } from '../../services/paper.service';
import { ReviewService } from '../../services/review.service';
import { LoadingService } from '../../services/loading.service';

@Component({
  selector: 'app-edit-profile-form',
  templateUrl: './edit-profile-form.component.html',
  styleUrls: ['./edit-profile-form.component.css']
})
export class EditProfileFormComponent implements OnInit, OnDestroy {

  /**
   * String containing the ETH-Account.
   * This string is set in ngOnInit() and the value is from metamask
   */
  userAccount: string;

  /**
   * User instance corresponding to the userAccount
   */
  user: UserDetails = new UserDetails();

  /**
   * Subscription which holds the route.
   * We subscribe in ngOnInit() and unsubscribe in ngOnDestroy()
   */
  routeSubscription: Subscription;

  constructor(
    private userDetailsService: UserDetailsService,
    private route: ActivatedRoute,
    private paperService: PaperService,
    private reviewService: ReviewService,
    private loadingService: LoadingService) {  }

  ngOnInit(): void {
    // load the userDetails vor the current ETH-Account
    this.userDetailsService.getCurrentEthereumAccount().then(
      (user: string) => {
        this.userAccount = user;
        this.loadUser();
      }
    );
  }

  /**
   * Load the attributes of the user.
   */
  loadUser() {
    this.loadingService.startLoading();
    try {
      this.loadDetails();
    } finally {
      this.loadingService.stopLoading();
    }
  }

  /**
   * Load user-details of the user with the current userAccount from the Chain.
   */
  private loadDetails() {
    this.userDetailsService.loadUserDetailsOf(this.userAccount).then(this.setUserDetails.bind(this));
  }

  /**
   *
   * @param userDetails userdetails to be set
   */
  private setUserDetails(userDetails: UserDetails) {
    // copy attributes of userDetails
    this.user = Object.assign({}, userDetails);
  }

  ngOnDestroy(): void {
    this.routeSubscription.unsubscribe();
  }

  onSubmit() {}
}
