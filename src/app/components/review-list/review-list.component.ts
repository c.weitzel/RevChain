import { Component, OnInit, Input } from '@angular/core';
import { Review } from '../../model/review';


/**
 * This component contains a list of review-list-items.
 */
@Component({
  selector: 'app-review-list',
  templateUrl: './review-list.component.html',
  styleUrls: ['./review-list.component.css']
})
export class ReviewListComponent implements OnInit {

  /**
   * list of reviews
   */
  @Input() reviews: Review[] = [];

  constructor() { }

  ngOnInit() {
  }

}
