import { Component, OnInit, Input } from '@angular/core';
import { Review } from '../../../model/review';

/**
 * This component shows short information about a review.
 * On Click, the useris redirected to the appropriate review-page and the user is shown the review.
 */
@Component({
  selector: 'app-review-list-item',
  templateUrl: './review-list-item.component.html',
  styleUrls: ['./review-list-item.component.css']
})
export class ReviewListItemComponent implements OnInit {

  @Input() review: Review;
  constructor() { }

  ngOnInit() {
  }

}
