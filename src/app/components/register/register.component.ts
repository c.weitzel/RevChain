import { Component, OnInit } from '@angular/core';

/**
 * Component, which contains the components neccessary for registering a new User.
 * It contains the UserDetailsFormComponent.
 */
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
