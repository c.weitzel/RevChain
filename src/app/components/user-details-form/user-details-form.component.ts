import { Component, OnInit, ViewChild, OnDestroy, Input } from '@angular/core';
import { UserDetailsService } from '../../services/user-details.service';
import { UserDetails } from '../../model/user-details';
import { Subscription } from 'rxjs';
import { LoadingService } from '../../services/loading.service';
import { Router } from '../../../../node_modules/@angular/router';
import { MessageService } from '../../services/message.service';

/**
 * This component holds a form for entering User-Details.
 */
@Component({
  selector: 'app-user-details-form',
  templateUrl: './user-details-form.component.html',
  styleUrls: ['./user-details-form.component.css']
})
export class UserDetailsFormComponent implements OnInit {

  /**
   * instance of userdetails corresponds to the input-field-values in the template
   */
  @Input() user: UserDetails = new UserDetails();

  /**
   *
   * @param userDetailsService For recaiving and uploading UserDetails
   */
  constructor(
    private userDetailsService: UserDetailsService,
    private loadingService: LoadingService,
    private messageService: MessageService
  ) {  }

  ngOnInit(): void {}


  /**
   * The Form is being submitted to IPFS and then the hash is stored to the chain.
   */
  onSubmit() {
    this.loadingService.startLoading();
    // the service is handling everything (upload to IPFS....)
    try {
      this.userDetailsService.setUserDetails(this.user).then(
        () => {
          this.loadingService.stopLoading();
          this.messageService.showSuccess('Saved!');
        }
      );
    } catch (error) {
      console.error(error);
      this.messageService.showError('Saving failed!');
      this.loadingService.stopLoading();
    }
  }

}
