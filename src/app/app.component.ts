import { Component, OnInit, OnDestroy, ChangeDetectorRef, ElementRef } from '@angular/core';
import { LoadingService } from './services/loading.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  loading: boolean;
  loadingSubscription: Subscription;

  constructor(
    private loadingService: LoadingService,
    private cdr: ChangeDetectorRef,
    private eRef: ElementRef) {
      this.loading = false;
  }

  ngOnInit() {
    this.loadingSubscription = this.loadingService.loading.subscribe(
      (loading: boolean) => {
        this.loading = loading;

        // ohne ChangeDetection kommt der Fehler "ExpressionChangedAfterItHasBeenCheckedError: Expression has changed after it was checked"
        // siehe: https://github.com/angular/angular/issues/17572
        this.cdr.detectChanges();
      }
    );

  }

  ngOnDestroy(): void {
    this.loadingSubscription.unsubscribe();
  }


}
