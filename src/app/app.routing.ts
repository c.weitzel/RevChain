import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { ProfileComponent } from './components/profile/profile.component';
import { UploadPaperFormComponent } from './components/upload-paper-form/upload-paper-form.component';
import { PaperComponent } from './components/paper/paper.component';
import { PaperListComponent } from './components/paper-list/paper-list.component';
import { LoggedInGuard } from './guards/logged-in.guard';
import { EditProfileFormComponent } from './components/edit-profile-form/edit-profile-form.component';
import { UploadedPaperListComponent } from './components/uploaded-paper-list/uploaded-paper-list.component';
import { ReviewRequestFormComponent } from './components/review-request-form/review-request-form.component';
import { UploadReviewFormComponent } from './components/upload-review-form/upload-review-form.component';
import { ReviewComponent } from './components/review/review.component';
import { EditUploadedPaperComponent } from './components/edit-uploaded-paper/edit-uploaded-paper.component';

/**
 * Here we define Routes for our Application.
 */
const appRoutes: Routes = [
  { path: 'register', component: RegisterComponent},
  { path: 'profiles/:eth-account', component: ProfileComponent},
  { path: 'papers', component: PaperListComponent, canActivate: [LoggedInGuard]},
  { path: 'papers/:id', component: PaperComponent, canActivate: [LoggedInGuard]},
  { path: 'papers/:paper-id/reviews/:review-id', component: ReviewComponent, canActivate: [LoggedInGuard]},
  { path: 'upload-paper', component: UploadPaperFormComponent, canActivate: [LoggedInGuard]},
  { path: 'upload-review/:id', component: UploadReviewFormComponent, canActivate: [LoggedInGuard]},
  { path: 'edit-profile', component: EditProfileFormComponent, canActivate: [LoggedInGuard]},
  { path: 'paper-list', component: UploadedPaperListComponent},
  { path: 'review-request/:id', component: ReviewRequestFormComponent, canActivate: [LoggedInGuard]},
  { path: 'edit-paper/:id', component: EditUploadedPaperComponent, canActivate: [LoggedInGuard]},
  { path: '**', component: HomeComponent}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes)
  ],
  declarations: [],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
