import { Buffer } from 'buffer';
/**
 * These Attributes are returned by the .get() Method of the ipfs-api library.
 * I've collected these Attributes as an interface, so that it's easier to handle in the rest of the application.
 * See:  https://github.com/ipfs/interface-ipfs-core/blob/master/SPEC/FILES.md#filesget
 */
export interface DownloadedData {
    content: Buffer;
    path: string;
}
