
/**
 * UserDetails are Attributes, which describe the user.
 * They are separated from the User-Class, because some functionality only relies on these specific attributes (e.g. UserDetailsForm)
 */
export class UserDetails {
    name: string;
    cv: string;
    fieldOfStudies: string;
    institute: string;
    interests: string[];
}
