/**
 * @author Christian Weitzel
 *  Stores the review request states for specific reviews.
 *  *  0: requested - a review request was written and uploaded to IPFS. It is then
 *         availabel to the paper uploader which can accept or decline the request.
 *  *  1: review - the uploader accepted a review request. the reviewer has now
 *         the possibility to upload a review. A review slot is now occupied.
 *  *  2: uploaded - a finished review was uploaded. The paper uploader can now
 *         accept or decline the uploaded review.
 *  *  3: reviewAccepted - states that the paper uploader accepted the review or
 *         that he/she did not answer for four weeks, because then the review will
 *         automatically be accepted. It is now possible to transfer the payment
 *         to the reviewer.
 *  *  4: requestDeclined - the paper uploader declined the request. The reviewer
 *         can not make a review.
 *  *  5: reviewDeclined - the paper uploader declined the uploaded review. The
 *         reviewer will not get any payment.
 *  *  6: paymentSucceeded - the reviewer received his payment for the finished
 *         and accepted review he uploaded. This will automatically happen when
 *         the paper uploader accepts the review.
 */
export enum ReviewState {
  requested = 0,
  review = 1,
  uploaded = 2,
  reviewAccepted = 3,
  requestDeclined = 4,
  reviewDeclined = 5,
  paymentSucceeded = 6
}
