
/**
 * Contains the attributes of a paper, which can be inserted into something like a formular.
 * Followoing fields are defined here:
 *
 * * author: string;
 * * title: string;
 * * abstract: string;
 * * price: number;
 * * reviewDeadLine: Date;
 * * reviewPrice: number;
 * * reviewSlots: number;
 * * mainField: string;
 * * subField: string;
 * * tags: string[];
 *
 */
export class PaperDetails {
    author: string;
    title: string;
    abstract: string;
    price: number;
    reviewDeadline: Date;
    reviewPrice: number;
    reviewSlots: number;
    mainField: string;
    subField: string;
    tags: string[];

    constructor() {
      this.tags = [];
    }
}
