import { Paper } from './paper';
import { Review } from './review';
import { UserDetails } from './user-details';

/**
 * This class represents a User which is registered to RevChain.
 */
export class User {

    /**
     * contains all the details about the user
     */
    userProfile: UserDetails;

    /**
     * contains papers, which have been uploaded by this user.
     */
    paperUploadList: Paper[];

    /**
     * contains papers, which have been purchased by this user.
     */
    paperBoughtList: Paper[];

    /**
     * contains reviews issued by this user.
     */
    reviewList: Review[];
}
