
/**
 * These Attributes are returned by the .add() Method of the ipfs-api library.
 * I've collected these Attributes as an interface, so that it's easier to handle in the rest of the application.
 * See: https://github.com/ipfs/interface-ipfs-core/blob/master/SPEC/FILES.md#filesadd
 */
export interface UploadedData {
    path: string;
    size: number;
    hash: string;
}
