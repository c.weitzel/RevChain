import { ReviewState } from './review-state.enum';

/**
 * This class represents a Review.
 * It models the different stages of issueing a review.
 */
export class Review {

    /**
     * metamask address of the owner of the review
     */
    owner: string;

    /**
     * IPFS-Hash of finished Review. (is a file)
     */
    reviewIpfsHash: string;
    /**
     * ipfshash of the Review Request. (And also the idientifier of the Review).
     * With this key, we identify the request during whole request-process.
     */
    requestIpfsHash: string;

    /**
     * Key to the Paper, which is reviewed.
     */
    paperIpfsHash: string;

    /**
     * Enumeration of the different states of a review.
     */
    reviewState: ReviewState;

    /**
     * Timestamp, on which the Review has been uploaded.
     */
    timestamp: Date;
}
