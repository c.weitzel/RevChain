import { Review } from './review';
import { PaperDetails } from './paper-details';

export class Paper {

    /**
     * identifier of the paper.
     * It contains a link to IPFS which contains the link to the paper and all the PaperDetails
     */
    paperIpfsHashIdentification: string;

    /**
     * Contains paper-metadata
     */
    paperDetails: PaperDetails;

    /**
     * saves the MetaMask/ETH-Account-Hash
     */
    owner: string;

    /**
     * Link to Paper-File (e.g. a PDF File)
     */
    paperIpfsHash: string;

    /**
     * reviews of a paper
     */
    reviews: Review[];

    /**
     * indicates whether the paper can be changed
     * and reviews can be requested or made
     */
    finalsState: boolean;

    constructor() {
      this.paperDetails = new PaperDetails();
      this.paperDetails.tags = [];
    }
}
