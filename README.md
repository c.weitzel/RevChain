**Description**

Authors of scientific papers usually have to pay a lot of money to get their papers reviewed and get little to know money for the paper itself.  
This is very unfair, so we have decided to offer a platform for scientists which helps them to publish their paper and make it available for reviewer to review their papers.  
_ReviewChain_ is a Web-Application where registered users can sell, buy and review scientific papers and also let their own papers be reviewed.  

The key goal of this project was to somehow integrate Blockchain-technology into it. A more detailed report can be found [here](https://docs.google.com/document/d/1CP9WIJheCuTuLuq3NolE43hLtaBOjYfOhUiF9eow1-g).  
  
  
**Installation**  
  
Make sure you have installed the following tools:
  * [git](https://git-scm.com/)
  * [NodeJS](https://nodejs.org/en/)
  * [Truffle](https://github.com/trufflesuite/truffle)
  * [Ganache](https://truffleframework.com/ganache)
  * [MetaMask](https://metamask.io/)

Now follow [these steps](https://truffleframework.com/docs/advanced/truffle-with-metamask) to correctly configure MetaMask with Ganache.

Then, to install the application, open the previously installed Git-Bash and run the following commands:
  * `git clone https://gitlab.gwdg.de/c.weitzel/RevChain` for cloning the repository
  * `cd RevChain` to go into the project folder
  * `npm install` to install all the dependcies (*)
  * `truffle compile` to compile the contracts
  * `truffle migrate` to migrate the contracts
  * `npm start` to start the application locally
  
(*) If there happens to be a problem with `npm install` please try deleting the file `package-lock.json`
with `rm package-lock.json` (or `del package-log.json`) and then `npm install` again. 
If there happens to be another error (this may happen on Windows computers) 
try following [these steps](https://github.com/Microsoft/nodejs-guidelines/blob/master/windows-environment.md#compiling-native-addon-modules) to get rid of the problems.  