// @author Carsten Fricke
// test script to deploy the papers contract to ganache-cli test
// network and test contract functions for expected behavior.

const assert = require('assert');
const ganache = require('ganache-cli');
const Web3 = require('web3');
// line required because of bug in beta.26
const provider = ganache.provider();
const web3 = new Web3(provider);
const ethers = require('ethers');

const { interface, bytecode } = require ('./compile.js');

const AssertionError = require('assert').AssertionError;

let Papers;
let accounts;
let ipfsHashes;
let returnCode;

// function to convert string to bytes32
function web3StringToBytes32(text) {
    var result = ethers.utils.hexlify(ethers.utils.toUtf8Bytes(text));
    while (result.length < 66) { result += '0'; }
    if (result.length !== 66) { throw new Error("invalid web3 implicit bytes32"); }
    return result;
}

beforeEach(async ()=> {
  // get A LIST OF ALL ACCOUNTS
  accounts = await web3.eth.getAccounts();

  // sample ipfsHashes
  ipfsHashes = [
    'QmahqCsAUAw7zMv6P6Ae8PjCTck7taQA6FgGQLnWdKG7U8',
    'Qmb4atcgbbN5v4CDJ8nz5QG5L2pgwSTLd3raDrnyhLjnUH',
  ];


  Papers = await new web3.eth.Contract(JSON.parse(interface))
    .deploy({data: bytecode})
    .send({from: accounts[0], gas: '4712388'})
  //  .send({from: accounts[0]})


});

describe ('Papers Contract', () => {
  it ('deploys a contract', () => {
    assert.ok(Papers.options.address);
  });


    // test uploadPaper with dummy ipfs string using web3StringtoBytes32
    // to create a bytes32 value
    // this upload uses 1 review slot and sends ether to conver
    // the review price
    it('upload a sample paper for review', async () => {
          // need to register User first using setUserDetailsIPFS
          await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
              from: accounts[0],
              gas: '4712388'
          });

          await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
              web3.utils.toWei('0.001','ether'),
              web3.utils.toWei('0.002','ether'),
              1, web3StringToBytes32('test'),
              web3StringToBytes32('test'),
            Date.now()/1000 + 24*60*60).send({
                from: accounts[0],
                gas: '4712388',
                value: web3.utils.toWei('0.002','ether')
          });

          // check review money has been received by contract
          let contractBalance = await web3.eth.getBalance(Papers.options.address);
          assert( contractBalance == web3.utils.toWei('0.002','ether'));

          // const initialBalance = await web3.eth.getBalance(accounts[0]);
    });

    it('upload a sample paper when no user details exist', async () => {
      try {
          await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
              web3.utils.toWei('0.001','ether'),
              web3.utils.toWei('0.002','ether'),
              1, web3StringToBytes32('test'),
              web3StringToBytes32('test'),
              Date.now()/1000 + 24*60*60).send({
                from: accounts[0],
                gas: '4712388',
                value: web3.utils.toWei('0.002','ether')
          });
          assert(false);
      } catch (err) {
          // since calling this with too little money should
          // cause the function to fail with an error (there is a require in the function)
          if (err instanceof AssertionError) {
            assert(false);
          } else {
            assert(err);
          }
      }
    });

    it('upload a sample paper with not enough ether for review', async () => {
      try {
          // need to register User first using setUserDetailsIPFS
          await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
            from: accounts[0],
            gas: '4712388'
          });

            await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
              web3.utils.toWei('0.001','ether'),
              web3.utils.toWei('0.002','ether'),
              1, web3StringToBytes32('test'),
              web3StringToBytes32('test'),
              Date.now()/1000 + 24*60*60).send({
                from: accounts[0],
                gas: '4712388',
                value: web3.utils.toWei('0.001','ether')
          });
          assert(false);
      } catch (err) {
          // since calling this with too little money should
          // cause the function to fail with an error (there is a require in the function)
          if (err instanceof AssertionError) {
            assert(false);
          } else {
            assert(err);
          }
      }
    });

  it('upload a sample paper with no money for review', async () => {
      try {
          // need to register User first using setUserDetailsIPFS
          await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
            from: accounts[0],
            gas: '4712388'
          });
            await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
              web3.utils.toWei('0.000','ether'),
              web3.utils.toWei('0.000','ether'),
              1, web3StringToBytes32('test'),
              web3StringToBytes32('test'),
              Date.now()/1000 + 24*60*60).send({
                from: accounts[0],
                gas: '4712388',
                value: web3.utils.toWei('0.001','ether')
          });
          assert(false);
      } catch (err) {
          // since calling this with too little money should
          // cause the function to fail with an error (there is a require in the function)
          if (err instanceof AssertionError) {
            assert(false);
          } else {
            assert(err);
          }
      }
    });

    it('upload a sample paper with negative price and negative review price', async () => {
      try {
          // need to register User first using setUserDetailsIPFS
          await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
            from: accounts[0],
            gas: '4712388'
          });

            await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
              web3.utils.toWei('-0.002','ether'),
              web3.utils.toWei('-0.001','ether'),
              1, web3StringToBytes32('test'),
              web3StringToBytes32('test'),
              Date.now()/1000 + 24*60*60).send({
                from: accounts[0],
                gas: '4712388',
                value: web3.utils.toWei('0.001','ether')
          });
          assert(false);
      } catch (err) {
          // since calling this with too little money should
          // cause the function to fail with an error (there is a require in the function)
          if (err instanceof AssertionError) {
            assert(false);
          } else {
            assert(err);
          }
      }
    });

  it('call getReviewState with a hash for which no paper has been uploaded', async () => {
    try {
      returnCode = await Papers.methods.getReviewState(web3.utils.fromAscii("20160528"),web3.utils.fromAscii("20160528")).call({
        from: accounts[0]
      });
      assert(false);
    } catch (err) {
      // since calling this for a paper which does not exist should
      // cause the function to fail with an error (there is a require in the function)
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }
    // assert.equal(0, returnCode);
  });

  it('call getPaperArrayLength when no paper has been uploaded', async () => {
    try {
      returnCode = await Papers.methods.getPaperArrayLength().call({
        from: accounts[0]
      });
      assert.equal(0, returnCode);
    } catch (err) {
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }
  });

  it('call getPaperOwner when no paper has been uploaded', async () => {
    try {
      returnCode = await Papers.methods.getPaperOwner(web3StringToBytes32('test')).call({
        from: accounts[0]
      });
      // should fail
      assert(false);
    } catch (err) {
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }
  });

  it('call getReviewState after uploading a dummy paper with a request hash which does not exist', async () => {
    try {
      // need to register User first using setUserDetailsIPFS
      await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
        from: accounts[0],
        gas: '4712388'
      });

      await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
        web3.utils.toWei('0.001','ether'),
        web3.utils.toWei('0.002','ether'),
        1, web3StringToBytes32('test'),
        web3StringToBytes32('test'),
        Date.now()/1000 + 24*60*60).send({
          from: accounts[0],
          gas: '4712388',
          value: web3.utils.toWei('0.002','ether')
        });
        returnCode = await Papers.methods.getReviewState(web3.utils.fromAscii("test"),web3.utils.fromAscii("20160528")).call({
          from: accounts[0]
        });
        assert(false);
    } catch (err) {
        // since calling this for a request hash which does not exist should
        // cause the function to fail with an error (there is a require in the function)
        if (err instanceof AssertionError) {
          assert(false);
        } else {
          assert(err);
        }
    }
    /* returnCode = await Papers.methods.getReviewState(web3StringToBytes32('test'),web3.utils.fromAscii("20160528")).call({
        from: accounts[0]
      });
    // returnCode should be 0 (review state 0 - requesting reviews)
    assert.equal(0, returnCode); */
  });

  it('call getAllPapers when no paper has been uploaded yet', async () => {
      returnCode = await Papers.methods.getAllPapers().call({
        from: accounts[0]
      });
      // console.log("Return Code from getOwnerPaperBoughtList=");
      // console.log(returnCode);
      // should return empty list e.g. 0
      assert.equal(0, returnCode);
  });

  it('call getPaperVersionsLength when no paper has been uploaded', async () => {
    try {
      returnCode = await Papers.methods.getPaperVersionsLength(web3StringToBytes32('test')).call({
        from: accounts[0]
      });
      assert.equal(0, returnCode);
    } catch (err) {
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }
  });

  it('call getAllPapers when no paper has been uploaded yet', async () => {
      returnCode = await Papers.methods.getAllPapers().call({
        from: accounts[0]
      });
      // console.log("Return Code from getOwnerPaperBoughtList=");
      // console.log(returnCode);
      // should return empty list e.g. 0
      assert.equal(0, returnCode);
  });

  it('call getPaperReviewList when no paper has been uploaded', async () => {
    try {
      returnCode = await Papers.methods.getPaperReviewList(web3StringToBytes32('test')).call({
        from: accounts[0]
      });
      // should fail
      assert(false);
    } catch (err) {
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }
  });

  it('call getPaperVersions when no paper has been uploaded', async () => {
    try {
      returnCode = await Papers.methods.getPaperVersions(web3StringToBytes32('test')).call({
        from: accounts[0]
      });
      // should fail
      assert(false);
    } catch (err) {
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }
  });

  it('call getNewestPaperIPFS when no paper has been uploaded', async () => {
    try {
      returnCode = await Papers.methods.getNewestPaperIPFS(web3StringToBytes32('test')).call({
        from: accounts[0]
      });
      // should fail
      assert(false);
    } catch (err) {
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }
  });

  it('call getPaperPrice when no paper has been uploaded', async () => {
    try {
      returnCode = await Papers.methods.getPaperPrice(web3StringToBytes32('test')).call({
        from: accounts[0]
      });
      // should fail
      assert(false);
    } catch (err) {
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }
  });

  it('call getReviewPrice when no paper has been uploaded', async () => {
    try {
      returnCode = await Papers.methods.getReviewPrice(web3StringToBytes32('test')).call({
        from: accounts[0]
      });
      // should fail
      assert(false);
    } catch (err) {
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }
  });

  it('call getAllPapers, getPaperArrayLength, getPaperOwner, getPaperPrice, getNewestPaperIPFS, getMainField, getSubField,getReviewPrice and getPaperVersionsLength after uploading two dummy papers', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });

    await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
      web3.utils.toWei('0.001','ether'),
        web3.utils.toWei('0.002','ether'),
        1, web3StringToBytes32('main1'),
        web3StringToBytes32('sub1'),
        Date.now()/1000 + 24*60*60).send({
          from: accounts[0],
          gas: '4712388',
          value: web3.utils.toWei('0.002','ether')
    });
    await  Papers.methods.uploadPaper(web3StringToBytes32('test2'),
        web3.utils.toWei('0.002','ether'),
        web3.utils.toWei('0.003','ether'),
        1, web3StringToBytes32('main2'),
        web3StringToBytes32('sub2'),
        Date.now()/1000 + 24*60*60).send({
          from: accounts[0],
          gas: '4712388',
          value: web3.utils.toWei('0.003','ether')
    });
    const allPapers = await Papers.methods.getAllPapers().call({
        from: accounts[0]
      });
    // allPapers should contain the uploaded hash
    assert.equal(web3StringToBytes32('test'), allPapers[0]);
    assert.equal(web3StringToBytes32('test2'), allPapers[1]);

    returnCode = await Papers.methods.getPaperArrayLength().call({
      from: accounts[0]
    });
    assert.equal(2, returnCode);

    returnCode = await Papers.methods.getPaperVersionsLength(web3StringToBytes32('test')).call({
      from: accounts[0]
    });
    assert.equal(1, returnCode);

    returnCode = await Papers.methods.getPaperVersionsLength(web3StringToBytes32('test2')).call({
      from: accounts[0]
    });
    assert.equal(1, returnCode);

    returnCode = await Papers.methods.getNewestPaperIPFS(web3StringToBytes32('test')).call({
      from: accounts[0]
    });
    assert.equal(web3StringToBytes32('test'), returnCode);

    returnCode = await Papers.methods.getNewestPaperIPFS(web3StringToBytes32('test2')).call({
      from: accounts[0]
    });
    assert.equal(web3StringToBytes32('test2'), returnCode);

    returnCode = await Papers.methods.getPaperOwner(web3StringToBytes32('test')).call({
      from: accounts[2]
    });
    assert.equal(accounts[0], returnCode);

    returnCode = await Papers.methods.getPaperPrice(web3StringToBytes32('test')).call({
      from: accounts[0]
    });
    assert.equal(web3.utils.toWei('0.001','ether'), returnCode);

    returnCode = await Papers.methods.getPaperPrice(web3StringToBytes32('test2')).call({
      from: accounts[0]
    });
    assert.equal(web3.utils.toWei('0.002','ether'), returnCode);

    returnCode = await Papers.methods.getReviewPrice(web3StringToBytes32('test')).call({
      from: accounts[0]
    });
    assert.equal(web3.utils.toWei('0.002','ether'), returnCode);

    returnCode = await Papers.methods.getReviewPrice(web3StringToBytes32('test2')).call({
      from: accounts[0]
    });
    assert.equal(web3.utils.toWei('0.003','ether'), returnCode);

    returnCode = await Papers.methods.getMainField(web3StringToBytes32('test')).call({
      from: accounts[0]
    });
    assert.equal(web3StringToBytes32('main1'), returnCode);

    returnCode = await Papers.methods.getMainField(web3StringToBytes32('test2')).call({
      from: accounts[0]
    });
    assert.equal(web3StringToBytes32('main2'), returnCode);

    returnCode = await Papers.methods.getSubField(web3StringToBytes32('test')).call({
      from: accounts[0]
    });
    assert.equal(web3StringToBytes32('sub1'), returnCode);

    returnCode = await Papers.methods.getSubField(web3StringToBytes32('test2')).call({
      from: accounts[0]
    });
    assert.equal(web3StringToBytes32('sub2'), returnCode);
  });

  it('call updatePrice, updateField after uploading two dummy papers', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });

    await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
        web3.utils.toWei('0.001','ether'),
        web3.utils.toWei('0.002','ether'),
        1, web3StringToBytes32('main1'),
        web3StringToBytes32('sub1'),
        Date.now()/1000 + 24*60*60).send({
          from: accounts[0],
          gas: '4712388',
          value: web3.utils.toWei('0.002','ether')
    });
    await  Papers.methods.uploadPaper(web3StringToBytes32('test2'),
        web3.utils.toWei('0.002','ether'),
        web3.utils.toWei('0.003','ether'),
        1, web3StringToBytes32('main2'),
        web3StringToBytes32('sub2'),
        Date.now()/1000 + 24*60*60).send({
          from: accounts[0],
          gas: '4712388',
          value: web3.utils.toWei('0.003','ether')
    });

    returnCode = await Papers.methods.getPaperPrice(web3StringToBytes32('test')).call({
      from: accounts[0]
    });
    assert.equal(web3.utils.toWei('0.001','ether'), returnCode);

    returnCode = await Papers.methods.getPaperPrice(web3StringToBytes32('test2')).call({
      from: accounts[0]
    });
    assert.equal(web3.utils.toWei('0.002','ether'), returnCode);

    returnCode = await Papers.methods.getMainField(web3StringToBytes32('test')).call({
      from: accounts[0]
    });
    assert.equal(web3StringToBytes32('main1'), returnCode);

    returnCode = await Papers.methods.getMainField(web3StringToBytes32('test2')).call({
      from: accounts[0]
    });
    assert.equal(web3StringToBytes32('main2'), returnCode);

    returnCode = await Papers.methods.getSubField(web3StringToBytes32('test')).call({
      from: accounts[0]
    });
    assert.equal(web3StringToBytes32('sub1'), returnCode);

    returnCode = await Papers.methods.getSubField(web3StringToBytes32('test2')).call({
      from: accounts[0]
    });
    assert.equal(web3StringToBytes32('sub2'), returnCode);

    await  Papers.methods.updatePrice(web3StringToBytes32('test'),
        web3.utils.toWei('0.0015','ether')).send({
          from: accounts[0],
          gas: '4712388'
    });

    await  Papers.methods.updatePrice(web3StringToBytes32('test2'),
        web3.utils.toWei('0.0025','ether')).send({
          from: accounts[0],
          gas: '4712388'
    });

    returnCode = await Papers.methods.getPaperPrice(web3StringToBytes32('test')).call({
      from: accounts[0]
    });
    assert.equal(web3.utils.toWei('0.0015','ether'), returnCode);

    returnCode = await Papers.methods.getPaperPrice(web3StringToBytes32('test2')).call({
      from: accounts[0]
    });
    assert.equal(web3.utils.toWei('0.0025','ether'), returnCode);

    await  Papers.methods.updateField(web3StringToBytes32('test'),
        web3StringToBytes32('main3'),web3StringToBytes32('sub3')).send({
          from: accounts[0],
          gas: '4712388'
    });

    await  Papers.methods.updateField(web3StringToBytes32('test2'),
        web3StringToBytes32('main4'),web3StringToBytes32('sub4')).send({
          from: accounts[0],
          gas: '4712388'
    });
    returnCode = await Papers.methods.getMainField(web3StringToBytes32('test')).call({
      from: accounts[0]
    });
    assert.equal(web3StringToBytes32('main3'), returnCode);

    returnCode = await Papers.methods.getMainField(web3StringToBytes32('test2')).call({
      from: accounts[0]
    });
    assert.equal(web3StringToBytes32('main4'), returnCode);

    returnCode = await Papers.methods.getSubField(web3StringToBytes32('test')).call({
      from: accounts[0]
    });
    assert.equal(web3StringToBytes32('sub3'), returnCode);

    returnCode = await Papers.methods.getSubField(web3StringToBytes32('test2')).call({
      from: accounts[0]
    });
    assert.equal(web3StringToBytes32('sub4'), returnCode);

  });

  it('call updateField when no paper has been uploaded', async () => {
    try {
      await Papers.methods.updateField(web3StringToBytes32('test'),web3StringToBytes32('test'),web3StringToBytes32('test')).send({
        from: accounts[0],
        gas: '4712388'
      });
      // should fail
      assert(false);
    } catch (err) {
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }
  });

  it('call updateField when account is not paper owner', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
      from: accounts[1],
      gas: '4712388'
    });

    await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
          web3.utils.toWei('0.001','ether'),
          web3.utils.toWei('0.002','ether'),
          1, web3StringToBytes32('main1'),
          web3StringToBytes32('sub1'),
          Date.now()/1000 + 24*60*60).send({
            from: accounts[0],
            gas: '4712388',
            value: web3.utils.toWei('0.002','ether')
      });

    try {
        await Papers.methods.updateField(web3StringToBytes32('test'),web3StringToBytes32('test'),web3StringToBytes32('test')).send({
          from: accounts[1],
          gas: '4712388'
      });
      // should fail
      assert(false);
    } catch (err) {
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }
  });

  it('call updatePrice when no paper has been uploaded', async () => {
    try {
      await Papers.methods.updatePrice(web3StringToBytes32('test'),
      web3.utils.toWei('0.001','ether')).send({
        from: accounts[0],
        gas: '4712388'
      });
      // should fail
      assert(false);
    } catch (err) {
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }
  });


  it('call updatePrice when account is not paper owner', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
      from: accounts[1],
      gas: '4712388'
    });


    await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
          web3.utils.toWei('0.001','ether'),
          web3.utils.toWei('0.002','ether'),
          1, web3StringToBytes32('main1'),
          web3StringToBytes32('sub1'),
          Date.now()/1000 + 24*60*60).send({
            from: accounts[0],
            gas: '4712388',
            value: web3.utils.toWei('0.002','ether')
      });1
    try {
      await Papers.methods.updatePrice(web3StringToBytes32('test'),
          web3.utils.toWei('0.0015','ether')).send({
          from: accounts[1],
          gas: '4712388'
      });
      // should fail
      assert(false);
    } catch (err) {
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }
  });

  it('call updatePrice with a negative price after uploading a paper', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });

    await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
          web3.utils.toWei('0.001','ether'),
          web3.utils.toWei('0.002','ether'),
          1, web3StringToBytes32('main1'),
          web3StringToBytes32('sub1'),
          Date.now()/1000 + 24*60*60).send({
            from: accounts[0],
            gas: '4712388',
            value: web3.utils.toWei('0.002','ether')
      });

    try {
      await Papers.methods.updatePrice(web3StringToBytes32('test'),
          web3.utils.toWei('-0.0015','ether')).send({
          from: accounts[0],
          gas: '4712388'
      });
      // should fail
      assert(false);
    } catch (err) {
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }
  });

  it('call updatePrice with a price higher than review price', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });

    await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
          web3.utils.toWei('0.001','ether'),
          web3.utils.toWei('0.002','ether'),
          1, web3StringToBytes32('main1'),
          web3StringToBytes32('sub1'),
          Date.now()/1000 + 24*60*60).send({
            from: accounts[0],
            gas: '4712388',
            value: web3.utils.toWei('0.002','ether')
      });

    try {
      await Papers.methods.updatePrice(web3StringToBytes32('test'),
          web3.utils.toWei('0.003','ether')).send({
          from: accounts[0],
          gas: '4712388'
      });
      // should fail
      assert(false);
    } catch (err) {
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }
  });

  it('call updateDeadline when no paper has been uploaded', async () => {
    try {
      await Papers.methods.updateDeadline(web3StringToBytes32('test'),
        Date.now()/1000 + 24*60*60).send({
        from: accounts[0],
        gas: '4712388'
      });
      // should fail
      assert(false);
    } catch (err) {
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }
  });


  it('call updateDeadline when account is not paper owner', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
      from: accounts[1],
      gas: '4712388'
    });

    await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
          web3.utils.toWei('0.001','ether'),
          web3.utils.toWei('0.002','ether'),
          1, web3StringToBytes32('main1'),
          web3StringToBytes32('sub1'),
          Date.now()/1000 + 24*60*60).send({
            from: accounts[0],
            gas: '4712388',
            value: web3.utils.toWei('0.002','ether')
      });

    try {
      await Papers.methods.updateDeadline(web3StringToBytes32('test'),
        Date.now()/1000 + 48*60*60).send({
        from: accounts[1],
        gas: '4712388'
      });
      // should fail
      assert(false);
    } catch (err) {
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }
  });

  it('call updateDeadline with a date before the initial date after uploading a paper', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });

    await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
          web3.utils.toWei('0.001','ether'),
          web3.utils.toWei('0.002','ether'),
          1, web3StringToBytes32('main1'),
          web3StringToBytes32('sub1'),
          Date.now()/1000 + 24*60*60).send({
            from: accounts[0],
            gas: '4712388',
            value: web3.utils.toWei('0.002','ether')
    });

    try {
      await Papers.methods.updateDeadline(web3StringToBytes32('test'),
        Date.now()/1000 - 48*60*60).send({
        from: accounts[0],
        gas: '4712388'
      });
      // should fail
      assert(false);
    } catch (err) {
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }
  });

  it('call updateDeadline when paper is in finalState', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });

    await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
          web3.utils.toWei('0.001','ether'),
          web3.utils.toWei('0.002','ether'),
          1, web3StringToBytes32('main1'),
          web3StringToBytes32('sub1'),
          Date.now()/1000 + 24*60*60).send({
            from: accounts[0],
            gas: '4712388',
            value: web3.utils.toWei('0.002','ether')
      });

    await Papers.methods.setFinalState(web3StringToBytes32('test')).send({
          from: accounts[0],
          gas: '4712388'
    });

    returnCode = await Papers.methods.getPaperFinalState(web3StringToBytes32('test')).call({
            from: accounts[0],
            gas: '4712388'
    });

    assert.equal(true,returnCode);

    try {
      await Papers.methods.updateDeadline(web3StringToBytes32('test'),
        Date.now()/1000 + 48*60*60).send({
        from: accounts[0],
        gas: '4712388'
      });
      // should fail
      assert(false);
    } catch (err) {
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }
  });

  it('call updateDeadline after uploading paper, check getDeadline', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });

    await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
          web3.utils.toWei('0.001','ether'),
          web3.utils.toWei('0.002','ether'),
          1, web3StringToBytes32('main1'),
          web3StringToBytes32('sub1'),
          Date.now()/1000 + 24*60*60).send({
            from: accounts[0],
            gas: '4712388',
            value: web3.utils.toWei('0.002','ether')
      });

    let newDate = Date.now()/1000 + 48*60*60;
    await Papers.methods.updateDeadline(web3StringToBytes32('test'), newDate).send({
          from: accounts[0],
          gas: '4712388'
    });

    returnCode = await Papers.methods.getDeadline(web3StringToBytes32('test')).call({
          from: accounts[0],
          gas: '4712388'
    });

    assert((returnCode > (newDate - 1)) && (returnCode < (newDate + 1 )));
  });

  it('call addReviewSlots when no paper has been uploaded', async () => {
    try {
      await Papers.methods.addReviewSlots(web3StringToBytes32('test'),
        3).send({
        from: accounts[0],
        gas: '4712388',
        value: web3.utils.toWei('0.002','ether')
      });
      // should fail
      assert(false);
    } catch (err) {
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }
  });


  it('call addReviewSlots when account is not paper owner', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
      from: accounts[1],
      gas: '4712388'
    });

    await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
          web3.utils.toWei('0.001','ether'),
          web3.utils.toWei('0.002','ether'),
          1, web3StringToBytes32('main1'),
          web3StringToBytes32('sub1'),
          Date.now()/1000 + 24*60*60).send({
            from: accounts[0],
            gas: '4712388',
            value: web3.utils.toWei('0.002','ether')
      });

    try {
      await Papers.methods.addReviewSlots(web3StringToBytes32('test'),
        1).send({
        from: accounts[1],
        gas: '4712388',
        value: web3.utils.toWei('0.002','ether')
      });
      // should fail
      assert(false);
    } catch (err) {
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }
  });

  it('call addReviewSlots with too little review money after uploading a paper', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });

    await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
          web3.utils.toWei('0.001','ether'),
          web3.utils.toWei('0.002','ether'),
          1, web3StringToBytes32('main1'),
          web3StringToBytes32('sub1'),
          Date.now()/1000 + 24*60*60).send({
            from: accounts[0],
            gas: '4712388',
            value: web3.utils.toWei('0.002','ether')
    });

    try {
      await Papers.methods.addReviewSlots(web3StringToBytes32('test'),
        1).send({
        from: accounts[0],
        gas: '4712388',
        value: web3.utils.toWei('0.001','ether')
      });
      // should fail
      assert(false);
    } catch (err) {
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }
  });

  it('call addReviewSlots with too much review money after uploading a paper', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });

    await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
          web3.utils.toWei('0.001','ether'),
          web3.utils.toWei('0.002','ether'),
          1, web3StringToBytes32('main1'),
          web3StringToBytes32('sub1'),
          Date.now()/1000 + 24*60*60).send({
            from: accounts[0],
            gas: '4712388',
            value: web3.utils.toWei('0.002','ether')
    });

    try {
      await Papers.methods.addReviewSlots(web3StringToBytes32('test'),
        1).send({
        from: accounts[0],
        gas: '4712388',
        value: web3.utils.toWei('0.003','ether')
      });
      // should fail
      assert(false);
    } catch (err) {
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }
  });

  it('call addReviewSlots when paper is in finalState', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });

    await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
          web3.utils.toWei('0.001','ether'),
          web3.utils.toWei('0.002','ether'),
          1, web3StringToBytes32('main1'),
          web3StringToBytes32('sub1'),
          Date.now()/1000 + 24*60*60).send({
            from: accounts[0],
            gas: '4712388',
            value: web3.utils.toWei('0.002','ether')
      });

    await Papers.methods.setFinalState(web3StringToBytes32('test')).send({
          from: accounts[0],
          gas: '4712388'
    });

    returnCode = await Papers.methods.getPaperFinalState(web3StringToBytes32('test')).call({
            from: accounts[0],
            gas: '4712388'
    });

    // ölkassert.equal(true,returnCode);

    try {
      await Papers.methods.addReviewSlots(web3StringToBytes32('test'),
        1).send({
        from: accounts[0],
        gas: '4712388',
        value: web3.utils.toWei('0.002','ether')
      });
      // should fail
      assert(false);
    } catch (err) {
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }
  });

  it('call addReviewSlots for an uploaded paper and check getReviewSlots, getReviewSlotsFree', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });

    await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
          web3.utils.toWei('0.001','ether'),
          web3.utils.toWei('0.002','ether'),
          1, web3StringToBytes32('main1'),
          web3StringToBytes32('sub1'),
          Date.now()/1000 + 24*60*60).send({
            from: accounts[0],
            gas: '4712388',
            value: web3.utils.toWei('0.002','ether')
      });

      // check review slots
      returnCode = await  Papers.methods.getReviewSlots(web3StringToBytes32('test')).call({
              from: accounts[0]
      });
      assert.equal (1, returnCode);

      // check review slots used
      returnCode = await  Papers.methods.getReviewSlotsUsed(web3StringToBytes32('test')).call({
              from: accounts[0]
      });
      assert.equal (0, returnCode);

      // check review slots free
      returnCode = await  Papers.methods.getReviewSlotsFree(web3StringToBytes32('test')).call({
              from: accounts[0]
      });
      assert.equal (1, returnCode);

      await Papers.methods.addReviewSlots(web3StringToBytes32('test'),
        1).send({
        from: accounts[0],
        gas: '4712388',
        value: web3.utils.toWei('0.002','ether')
      });

      // check review slots
      returnCode = await  Papers.methods.getReviewSlots(web3StringToBytes32('test')).call({
              from: accounts[0]
      });
      assert.equal (2, returnCode);

      // check review slots used
      returnCode = await  Papers.methods.getReviewSlotsUsed(web3StringToBytes32('test')).call({
              from: accounts[0]
      });
      assert.equal (0, returnCode);

      // check review slots free
      returnCode = await  Papers.methods.getReviewSlotsFree(web3StringToBytes32('test')).call({
              from: accounts[0]
      });
      assert.equal (2, returnCode);

  });

  it('call getTimestamp when no paper has been uploaded', async () => {
    try {
      returnCode = await Papers.methods.getTimestamp(web3StringToBytes32('test')).call({
        from: accounts[0]
      });
      // should fail
      assert(false);
    } catch (err) {
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }
  });

  it('call getTimestamp after uploading a dummy paper', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });

    await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
        web3.utils.toWei('0.001','ether'),
        web3.utils.toWei('0.002','ether'),
        1, web3StringToBytes32('main1'),
        web3StringToBytes32('sub1'),
        Date.now()/1000 + 24*60*60).send({
          from: accounts[0],
          gas: '4712388',
          value: web3.utils.toWei('0.002','ether')
    });

    returnCode = await Papers.methods.getTimestamp(web3StringToBytes32('test')).call({
      from: accounts[0]
    });
    assert((returnCode > (Date.now()/1000 - 100)) && (returnCode < (Date.now()/1000 + 100)));
  });

  it('call getDeadline when no paper has been uploaded', async () => {
    try {
      returnCode = await Papers.methods.getDeadline(web3StringToBytes32('test')).call({
        from: accounts[0]
      });
      assert.equal(0, returnCode);
    } catch (err) {
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }
  });

  it('call getDeadline after uploading two dummy papers', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });

    const reviewTime1 = Date.now()/1000 + 24*60*60;
    const reviewTime2 = Date.now()/1000 + 48*60*60;

    await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
        web3.utils.toWei('0.001','ether'),
        web3.utils.toWei('0.002','ether'),
        1, web3StringToBytes32('main1'),
        web3StringToBytes32('sub1'),
        reviewTime1).send({
          from: accounts[0],
          gas: '4712388',
          value: web3.utils.toWei('0.002','ether')
    });

    await  Papers.methods.uploadPaper(web3StringToBytes32('test2'),
        web3.utils.toWei('0.001','ether'),
        web3.utils.toWei('0.002','ether'),
        1, web3StringToBytes32('main1'),
        web3StringToBytes32('sub1'),
        reviewTime2).send({
          from: accounts[0],
          gas: '4712388',
          value: web3.utils.toWei('0.002','ether')
    });

    returnCode = await Papers.methods.getDeadline(web3StringToBytes32('test')).call({
      from: accounts[0]
    });
    assert((returnCode > (reviewTime1 - 1))&&(returnCode < (reviewTime1 + 1)));
    returnCode = await Papers.methods.getDeadline(web3StringToBytes32('test2')).call({
      from: accounts[0]
    });
    assert((returnCode > (reviewTime2 - 1))&&(returnCode < (reviewTime2 + 1)));

  });

  it('call getReviewSlots when no paper has been uploaded', async () => {
    try {
      returnCode = await Papers.methods.getReviewSlots(web3StringToBytes32('test')).call({
        from: accounts[0]
      });
      // should fail
      assert(false);
    } catch (err) {
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }
  });

  it('call getReviewSlotsUsed when no paper has been uploaded', async () => {
    try {
      returnCode = await Papers.methods.getReviewSlotsUsed(web3StringToBytes32('test')).call({
        from: accounts[0]
      });
      // should fail
      assert(false);
    } catch (err) {
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }
  });

  it('call getReviewSlotsFree when no paper has been uploaded', async () => {
    try {
      returnCode = await Papers.methods.getReviewSlotsFree(web3StringToBytes32('test')).call({
        from: accounts[0]
      });
      // should fail
      assert(false);
    } catch (err) {
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }
  });

  it('call getMainField when no paper has been uploaded', async () => {
    try {
      returnCode = await Papers.methods.getMainField(web3StringToBytes32('test')).call({
        from: accounts[0]
      });
      // should fail
      assert(false);
    } catch (err) {
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }
  });

  it('call getSubField when no paper has been uploaded', async () => {
    try {
      returnCode = await Papers.methods.getMainField(web3StringToBytes32('test')).call({
        from: accounts[0]
      });
      // should fail
      assert(false);
    } catch (err) {
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }
  });

  it('call getPaperBalance when no paper has been uploaded', async () => {
    try {
      returnCode = await Papers.methods.getPaperBalance(web3StringToBytes32('test')).call({
        from: accounts[0]
      });
      // should fail
      assert(false);
    } catch (err) {
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }
  });

  it('upload 3 paper, buy 2 papers from another account, call getOwnerPaperBoughtList, check money is transferred, check getPaperBalance is correct', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
      from: accounts[1],
      gas: '4712388'
    });

    await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
        web3.utils.toWei('0.001','ether'),
        web3.utils.toWei('0.002','ether'),
        1, web3StringToBytes32('test'),
        web3StringToBytes32('test'),
        Date.now()/1000 + 24*60*60).send({
          from: accounts[0],
          gas: '4712388',
          value: web3.utils.toWei('0.002','ether')
    });
    await  Papers.methods.uploadPaper(web3StringToBytes32('test2'),
        web3.utils.toWei('0.002','ether'),
        web3.utils.toWei('0.003','ether'),
        1, web3StringToBytes32('test'),
        web3StringToBytes32('test'),
        Date.now()/1000 + 24*60*60).send({
          from: accounts[0],
          gas: '4712388',
          value: web3.utils.toWei('0.003','ether')
    });

    await  Papers.methods.uploadPaper(web3StringToBytes32('test3'),
        web3.utils.toWei('0.003','ether'),
        web3.utils.toWei('0.004','ether'),
        1, web3StringToBytes32('test'),
        web3StringToBytes32('test'),
        Date.now()/1000 + 24*60*60).send({
          from: accounts[0],
          gas: '4712388',
          value: web3.utils.toWei('0.004','ether')
      });

      // check getPaperBalance shows review money
      returnCode = await  Papers.methods.getPaperBalance(web3StringToBytes32('test3')).call({
            from: accounts[0]
        });
      assert.equal (web3.utils.toWei('0.004','ether'), returnCode);

      returnCode = await  Papers.methods.getPaperBalance(web3StringToBytes32('test2')).call({
            from: accounts[0]
        });
      assert.equal (web3.utils.toWei('0.003','ether'), returnCode);

      returnCode = await  Papers.methods.getPaperBalance(web3StringToBytes32('test')).call({
            from: accounts[0]
        });
      assert.equal (web3.utils.toWei('0.002','ether'), returnCode);

      // check Contract has received review money
      let contractBalance = await web3.eth.getBalance(Papers.options.address);
      assert( contractBalance == web3.utils.toWei('0.009','ether'));

      const initialBalance = await web3.eth.getBalance(accounts[0]);

      await  Papers.methods.buyPaper(web3StringToBytes32('test')).send({
            from: accounts[1],
            gas: '4712388',
            value: web3.utils.toWei('0.001','ether')
      });

      await  Papers.methods.buyPaper(web3StringToBytes32('test2')).send({
              from: accounts[1],
              gas: '4712388',
              value: web3.utils.toWei('0.002','ether')
      });

      // accounts[0] should now have received money
      const newBalance = await web3.eth.getBalance(accounts[0]);

      assert( (newBalance - initialBalance) == web3.utils.toWei('0.003','ether'));

      const boughtPapers = await Papers.methods.getOwnerPaperBoughtList(accounts[1]).call({
        from: accounts[1]
      });
      // console.log("Return Code from getOwnerPaperBoughtList=");
      // console.log(returnCode);
      // List should be empty, e.g. return 0
      assert.equal(web3StringToBytes32('test'), boughtPapers[0]);
      assert.equal(web3StringToBytes32('test2'), boughtPapers[1]);

    });

    it('upload 3 paper, updatePapers, buyPapers, call getOwnerPaperBoughtList, check getPaperVersionsLength, check getPaperVersions, check getNewestPaperIPFS', async () => {
      // need to register User first using setUserDetailsIPFS
      await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
        from: accounts[0],
        gas: '4712388'
      });
      // need to register User first using setUserDetailsIPFS
      await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
        from: accounts[1],
        gas: '4712388'
      });
      // need to register User first using setUserDetailsIPFS
      await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user3')).send({
        from: accounts[2],
        gas: '4712388'
      });

      await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
          web3.utils.toWei('0.001','ether'),
          web3.utils.toWei('0.002','ether'),
          1, web3StringToBytes32('test'),
          web3StringToBytes32('test'),
          Date.now()/1000 + 24*60*60).send({
            from: accounts[0],
            gas: '4712388',
            value: web3.utils.toWei('0.002','ether')
      });
      await  Papers.methods.uploadPaper(web3StringToBytes32('test2'),
          web3.utils.toWei('0.002','ether'),
          web3.utils.toWei('0.003','ether'),
          1, web3StringToBytes32('test'),
          web3StringToBytes32('test'),
          Date.now()/1000 + 24*60*60).send({
            from: accounts[0],
            gas: '4712388',
            value: web3.utils.toWei('0.003','ether')
      });

      await  Papers.methods.uploadPaper(web3StringToBytes32('test3'),
          web3.utils.toWei('0.003','ether'),
          web3.utils.toWei('0.004','ether'),
          1, web3StringToBytes32('test'),
          web3StringToBytes32('test'),
          Date.now()/1000 + 24*60*60).send({
            from: accounts[0],
            gas: '4712388',
            value: web3.utils.toWei('0.004','ether')
        });

        await  Papers.methods.buyPaper(web3StringToBytes32('test')).send({
              from: accounts[1],
              gas: '4712388',
              value: web3.utils.toWei('0.001','ether')
        });

        await  Papers.methods.updatePaper(web3StringToBytes32('test'),
                web3StringToBytes32('testnew')).send({
                from: accounts[0],
                gas: '4712388'
        });

        await  Papers.methods.updatePaper(web3StringToBytes32('test'),
                web3StringToBytes32('testnewnew')).send({
                from: accounts[0],
                gas: '4712388'
        });

        await  Papers.methods.updatePaper(web3StringToBytes32('test2'),
                web3StringToBytes32('test2new')).send({
                from: accounts[0],
                gas: '4712388'
        });

        await  Papers.methods.buyPaper(web3StringToBytes32('test')).send({
                from: accounts[2],
                gas: '4712388',
                value: web3.utils.toWei('0.001','ether')
        });


        let boughtPapers = await Papers.methods.getOwnerPaperBoughtList(accounts[1]).call({
          from: accounts[1]
        });
        // console.log("Return Code from getOwnerPaperBoughtList=");
        // console.log(returnCode);
        // List should be empty, e.g. return 0
        assert.equal(web3StringToBytes32('test'), boughtPapers[0]);

        boughtPapers = await Papers.methods.getOwnerPaperBoughtList(accounts[2]).call({
          from: accounts[2]
        });
        // console.log("Return Code from getOwnerPaperBoughtList=");
        // console.log(returnCode);
        // List should be empty, e.g. return 0

        assert.equal(web3StringToBytes32('test'), boughtPapers[0]);

        returnCode = await Papers.methods.getNewestPaperIPFS(web3StringToBytes32('test')).call({
          from: accounts[2]
        });

        assert.equal(web3StringToBytes32('testnewnew'), returnCode);

        returnCode = await Papers.methods.getNewestPaperIPFS(web3StringToBytes32('test2')).call({
          from: accounts[2]
        });

        assert.equal(web3StringToBytes32('test2new'), returnCode);

        returnCode = await Papers.methods.getPaperVersionsLength(web3StringToBytes32('test')).call({
          from: accounts[2]
        });

        assert.equal(3, returnCode);

        returnCode = await Papers.methods.getPaperVersionsLength(web3StringToBytes32('test2')).call({
          from: accounts[2]
        });

        assert.equal(2, returnCode);

        returnCode = await Papers.methods.getPaperVersions(web3StringToBytes32('test')).call({
          from: accounts[2]
        });

        assert.equal(web3StringToBytes32('test'), returnCode[0]);
        assert.equal(web3StringToBytes32('testnew'), returnCode[1]);
        assert.equal(web3StringToBytes32('testnewnew'), returnCode[2]);

        returnCode = await Papers.methods.getPaperVersions(web3StringToBytes32('test2')).call({
          from: accounts[2]
        });

        assert.equal(web3StringToBytes32('test2'), returnCode[0]);
        assert.equal(web3StringToBytes32('test2new'), returnCode[1]);

    });

    it('upload 3 paper, buy 2 papers from another account, call getOwnerPaperBoughtList, check money is transferred, check getPaperBalance is correct', async () => {
      // need to register User first using setUserDetailsIPFS
      await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
        from: accounts[0],
        gas: '4712388'
      });
      // need to register User first using setUserDetailsIPFS
      await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
        from: accounts[1],
        gas: '4712388'
      });

        await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
            web3.utils.toWei('0.001','ether'),
            web3.utils.toWei('0.002','ether'),
            1, web3StringToBytes32('test'),
            web3StringToBytes32('test'),
            Date.now()/1000 + 24*60*60).send({
              from: accounts[0],
              gas: '4712388',
              value: web3.utils.toWei('0.002','ether')
        });
        await  Papers.methods.uploadPaper(web3StringToBytes32('test2'),
            web3.utils.toWei('0.002','ether'),
            web3.utils.toWei('0.003','ether'),
            1, web3StringToBytes32('test'),
            web3StringToBytes32('test'),
            Date.now()/1000 + 24*60*60).send({
              from: accounts[0],
              gas: '4712388',
              value: web3.utils.toWei('0.003','ether')
        });

        await  Papers.methods.uploadPaper(web3StringToBytes32('test3'),
            web3.utils.toWei('0.003','ether'),
            web3.utils.toWei('0.004','ether'),
            1, web3StringToBytes32('test'),
            web3StringToBytes32('test'),
            Date.now()/1000 + 24*60*60).send({
              from: accounts[0],
              gas: '4712388',
              value: web3.utils.toWei('0.004','ether')
          });

          // check getPaperBalance shows review money
          returnCode = await  Papers.methods.getPaperBalance(web3StringToBytes32('test3')).call({
                from: accounts[0]
          });
          assert.equal (web3.utils.toWei('0.004','ether'), returnCode);

          returnCode = await  Papers.methods.getPaperBalance(web3StringToBytes32('test2')).call({
                from: accounts[0]
          });
          assert.equal (web3.utils.toWei('0.003','ether'), returnCode);

          returnCode = await  Papers.methods.getPaperBalance(web3StringToBytes32('test')).call({
                from: accounts[0]
          });
          assert.equal (web3.utils.toWei('0.002','ether'), returnCode);

          // check Contract has received review money
          let contractBalance = await web3.eth.getBalance(Papers.options.address);
          assert( contractBalance == web3.utils.toWei('0.009','ether'));

          const initialBalance = await web3.eth.getBalance(accounts[0]);

          await  Papers.methods.buyPaper(web3StringToBytes32('test')).send({
                from: accounts[1],
                gas: '4712388',
                value: web3.utils.toWei('0.001','ether')
          });

          await  Papers.methods.buyPaper(web3StringToBytes32('test2')).send({
                  from: accounts[1],
                  gas: '4712388',
                  value: web3.utils.toWei('0.002','ether')
          });

          // accounts[0] should now have received money
          const newBalance = await web3.eth.getBalance(accounts[0]);

          assert( (newBalance - initialBalance) == web3.utils.toWei('0.003','ether'));

          const boughtPapers = await Papers.methods.getOwnerPaperBoughtList(accounts[1]).call({
            from: accounts[1]
          });
          // console.log("Return Code from getOwnerPaperBoughtList=");
          // console.log(returnCode);
          // List should be empty, e.g. return 0
          assert.equal(web3StringToBytes32('test'), boughtPapers[0]);
          assert.equal(web3StringToBytes32('test2'), boughtPapers[1]);

      });

      it('upload 3 papers with different prices, check getPaperPrice is correct, change paper price, check again', async () => {
        // need to register User first using setUserDetailsIPFS
        await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
          from: accounts[0],
          gas: '4712388'
        });

        await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
            web3.utils.toWei('0.001','ether'),
            web3.utils.toWei('0.002','ether'),
            1, web3StringToBytes32('test'),
            web3StringToBytes32('test'),
            Date.now()/1000 + 24*60*60).send({
              from: accounts[0],
              gas: '4712388',
              value: web3.utils.toWei('0.002','ether')
      });

      await  Papers.methods.uploadPaper(web3StringToBytes32('test2'),
          web3.utils.toWei('0.002','ether'),
          web3.utils.toWei('0.003','ether'),
          1, web3StringToBytes32('test'),
          web3StringToBytes32('test'),
          Date.now()/1000 + 24*60*60).send({
            from: accounts[0],
            gas: '4712388',
            value: web3.utils.toWei('0.003','ether')
      });

      await  Papers.methods.uploadPaper(web3StringToBytes32('test3'),
          web3.utils.toWei('0.003','ether'),
          web3.utils.toWei('0.004','ether'),
          1, web3StringToBytes32('test'),
          web3StringToBytes32('test'),
          Date.now()/1000 + 24*60*60).send({
            from: accounts[0],
            gas: '4712388',
            value: web3.utils.toWei('0.004','ether')
        });

        // check getPaperPrice shows correct price
        returnCode = await  Papers.methods.getPaperPrice(web3StringToBytes32('test3')).call({
              from: accounts[0]
          });
        assert.equal (web3.utils.toWei('0.003','ether'), returnCode);

        returnCode = await  Papers.methods.getPaperPrice(web3StringToBytes32('test2')).call({
              from: accounts[0]
          });
        assert.equal (web3.utils.toWei('0.002','ether'), returnCode);

        returnCode = await  Papers.methods.getPaperPrice(web3StringToBytes32('test')).call({
              from: accounts[0]
          });
        assert.equal (web3.utils.toWei('0.001','ether'), returnCode);
    });

    it('buyPaper which does not exist', async () => {
      // need to register User first using setUserDetailsIPFS
      await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
        from: accounts[0],
        gas: '4712388'
      });
      // need to register User first using setUserDetailsIPFS
      await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
        from: accounts[1],
        gas: '4712388'
      });

        await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
          web3.utils.toWei('0.001','ether'),
          web3.utils.toWei('0.002','ether'),
          1, web3StringToBytes32('test'),
          web3StringToBytes32('test'),
          Date.now()/1000 + 24*60*60).send({
            from: accounts[0],
            gas: '4712388',
            value: web3.utils.toWei('0.002','ether')
        });

      try{
        await  Papers.methods.buyPaper(web3StringToBytes32('test3')).send({
              from: accounts[1],
              gas: '4712388',
              value: web3.utils.toWei('0.001','ether')
        });

        // assert(false);
      } catch (err) {
        // since calling this for a request hash which does not exist should
        // cause the function to fail with an error (there is a require in the function)
        assert(err);
      }
    });

    it('buyPaper with not enough money', async () => {
      // need to register User first using setUserDetailsIPFS
      await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
        from: accounts[0],
        gas: '4712388'
      });
      // need to register User first using setUserDetailsIPFS
      await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
        from: accounts[1],
        gas: '4712388'
      });

          await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
            web3.utils.toWei('0.001','ether'),
            web3.utils.toWei('0.002','ether'),
            1, web3StringToBytes32('test'),
            web3StringToBytes32('test'),
            Date.now()/1000 + 24*60*60).send({
              from: accounts[0],
              gas: '4712388',
              value: web3.utils.toWei('0.002','ether')
          });

        try{
          await  Papers.methods.buyPaper(web3StringToBytes32('test')).send({
                from: accounts[1],
                gas: '4712388',
                value: web3.utils.toWei('0.0001','ether')
          });

          // assert(false);
        } catch (err) {
          // since calling this for a request hash which does not exist should
          // cause the function to fail with an error (there is a require in the function)
          assert(err);
        }
    });

    it('upload 3 paper, buy 2 papers from another account, call getOwnerPaperBoughtList, check money is transferred, check getPaperBalance is correct', async () => {
      // need to register User first using setUserDetailsIPFS
      await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
        from: accounts[0],
        gas: '4712388'
      });
      // need to register User first using setUserDetailsIPFS
      await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
        from: accounts[1],
        gas: '4712388'
      });

      await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
          web3.utils.toWei('0.001','ether'),
          web3.utils.toWei('0.002','ether'),
          1, web3StringToBytes32('test'),
          web3StringToBytes32('test'),
          Date.now()/1000 + 24*60*60).send({
            from: accounts[0],
            gas: '4712388',
            value: web3.utils.toWei('0.002','ether')
      });
      await  Papers.methods.uploadPaper(web3StringToBytes32('test2'),
          web3.utils.toWei('0.002','ether'),
          web3.utils.toWei('0.003','ether'),
          1, web3StringToBytes32('test'),
          web3StringToBytes32('test'),
          Date.now()/1000 + 24*60*60).send({
            from: accounts[0],
            gas: '4712388',
            value: web3.utils.toWei('0.003','ether')
      });

      await  Papers.methods.uploadPaper(web3StringToBytes32('test3'),
          web3.utils.toWei('0.003','ether'),
          web3.utils.toWei('0.004','ether'),
          1, web3StringToBytes32('test'),
          web3StringToBytes32('test'),
          Date.now()/1000 + 24*60*60).send({
            from: accounts[0],
            gas: '4712388',
            value: web3.utils.toWei('0.004','ether')
        });

        // check getPaperBalance shows review money
        returnCode = await  Papers.methods.getPaperBalance(web3StringToBytes32('test3')).call({
              from: accounts[0]
          });
        assert.equal (web3.utils.toWei('0.004','ether'), returnCode);

        returnCode = await  Papers.methods.getPaperBalance(web3StringToBytes32('test2')).call({
              from: accounts[0]
          });
        assert.equal (web3.utils.toWei('0.003','ether'), returnCode);

        returnCode = await  Papers.methods.getPaperBalance(web3StringToBytes32('test')).call({
              from: accounts[0]
          });
        assert.equal (web3.utils.toWei('0.002','ether'), returnCode);

        // check Contract has received review money
        let contractBalance = await web3.eth.getBalance(Papers.options.address);
        assert( contractBalance == web3.utils.toWei('0.009','ether'));

        const initialBalance = await web3.eth.getBalance(accounts[0]);

        await  Papers.methods.buyPaper(web3StringToBytes32('test')).send({
              from: accounts[1],
              gas: '4712388',
              value: web3.utils.toWei('0.001','ether')
        });

        await  Papers.methods.buyPaper(web3StringToBytes32('test2')).send({
                from: accounts[1],
                gas: '4712388',
                value: web3.utils.toWei('0.002','ether')
        });

        // accounts[0] should now have received money
        const newBalance = await web3.eth.getBalance(accounts[0]);

        assert( (newBalance - initialBalance) == web3.utils.toWei('0.003','ether'));

        const boughtPapers = await Papers.methods.getOwnerPaperBoughtList(accounts[1]).call({
          from: accounts[1]
        });
        // console.log("Return Code from getOwnerPaperBoughtList=");
        // console.log(returnCode);
        // List should be empty, e.g. return 0
        assert.equal(web3StringToBytes32('test'), boughtPapers[0]);
        assert.equal(web3StringToBytes32('test2'), boughtPapers[1]);

      });

      it('upload a paper for review with price 0, buy paper with price 0, check paperBoughtList', async () => {
        // need to register User first using setUserDetailsIPFS
        await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
          from: accounts[0],
          gas: '4712388'
        });
        // need to register User first using setUserDetailsIPFS
        await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
          from: accounts[1],
          gas: '4712388'
        });

        await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
            web3.utils.toWei('0','ether'),
            web3.utils.toWei('0.002','ether'),
            1, web3StringToBytes32('test'),
            web3StringToBytes32('test'),
            Date.now()/1000 + 24*60*60).send({
              from: accounts[0],
              gas: '4712388',
              value: web3.utils.toWei('0.002','ether')
        });

          // check getPaperPrice shows correct price
        returnCode = await  Papers.methods.getPaperPrice(web3StringToBytes32('test')).call({
                from: accounts[0]
        });
        assert.equal (web3.utils.toWei('0','ether'), returnCode);

        await  Papers.methods.buyPaper(web3StringToBytes32('test')).send({
                  from: accounts[1],
                  gas: '4712388',
                  value: web3.utils.toWei('0','ether')
        });

        const boughtPapers = await Papers.methods.getOwnerPaperBoughtList(accounts[1]).call({
            from: accounts[1]
        });
          // console.log("Return Code from getOwnerPaperBoughtList=");
          // console.log(returnCode);
          // List should be empty, e.g. return 0
        assert.equal(web3StringToBytes32('test'), boughtPapers[0]);

      });

      it('buyPaper which does not exist', async () => {
        // need to register User first using setUserDetailsIPFS
        await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
          from: accounts[0],
          gas: '4712388'
        });
        // need to register User first using setUserDetailsIPFS
        await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
          from: accounts[1],
          gas: '4712388'
        });

          await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
            web3.utils.toWei('0.001','ether'),
            web3.utils.toWei('0.002','ether'),
            1, web3StringToBytes32('test'),
            web3StringToBytes32('test'),
            Date.now()/1000 + 24*60*60).send({
              from: accounts[0],
              gas: '4712388',
              value: web3.utils.toWei('0.002','ether')
          });

        try{
          await  Papers.methods.buyPaper(web3StringToBytes32('test3')).send({
                from: accounts[1],
                gas: '4712388',
                value: web3.utils.toWei('0.001','ether')
          });

          // assert(false);
        } catch (err) {
          // since calling this for a request hash which does not exist should
          // cause the function to fail with an error (there is a require in the function)
          assert(err);
        }
      });

      it('buyPaper with not enough money', async () => {
        // need to register User first using setUserDetailsIPFS
        await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
          from: accounts[0],
          gas: '4712388'
        });
        // need to register User first using setUserDetailsIPFS
        await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
          from: accounts[1],
          gas: '4712388'
        });

            await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
              web3.utils.toWei('0.001','ether'),
              web3.utils.toWei('0.002','ether'),
              1, web3StringToBytes32('test'),
              web3StringToBytes32('test'),
              Date.now()/1000 + 24*60*60).send({
                from: accounts[0],
                gas: '4712388',
                value: web3.utils.toWei('0.002','ether')
            });

          try{
            await  Papers.methods.buyPaper(web3StringToBytes32('test')).send({
                  from: accounts[1],
                  gas: '4712388',
                  value: web3.utils.toWei('0.0001','ether')
            });

            // assert(false);
          } catch (err) {
            // since calling this for a request hash which does not exist should
            // cause the function to fail with an error (there is a require in the function)
            assert(err);
          }
    });

    it('buyPaper with too much money', async () => {
      // need to register User first using setUserDetailsIPFS
      await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
        from: accounts[0],
        gas: '4712388'
      });
      // need to register User first using setUserDetailsIPFS
      await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
        from: accounts[1],
        gas: '4712388'
      });

        await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
          web3.utils.toWei('0.001','ether'),
          web3.utils.toWei('0.002','ether'),
          1, web3StringToBytes32('test'),
          web3StringToBytes32('test'),
          Date.now()/1000 + 24*60*60).send({
            from: accounts[0],
            gas: '4712388',
            value: web3.utils.toWei('0.002','ether')
        });

      try{
        await  Papers.methods.buyPaper(web3StringToBytes32('test')).send({
              from: accounts[1],
              gas: '4712388',
              value: web3.utils.toWei('0.003','ether')
        });

        // assert(false);
      } catch (err) {
        // since calling this for a request hash which does not exist should
        // cause the function to fail with an error (there is a require in the function)
        assert(err);
      }
    });

    it('call getOwnerPaperBoughtList with test address for which no paper exists', async () => {
      returnCode = await Papers.methods.getOwnerPaperBoughtList(accounts[1]).call({
        from: accounts[0]
      });
      // console.log("Return Code from getOwnerPaperBoughtList=");
      // console.log(returnCode);
      // List should be empty, e.g. return 0
      assert.equal(0, returnCode);
  });


  it('request review for an uploaded paper (sendReviewRequest) from same account, paper is bought', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });

    try{
      await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
        web3.utils.toWei('0.001','ether'),
        web3.utils.toWei('0.002','ether'),
        1, web3StringToBytes32('test'),
        web3StringToBytes32('test'),
        Date.now()/1000 + 24*60*60).send({
          from: accounts[0],
          gas: '4712388',
          value: web3.utils.toWei('0.002','ether')
        });

        await  Papers.methods.buyPaper(web3StringToBytes32('test2')).send({
                from: accounts[0],
                gas: '4712388',
                value: web3.utils.toWei('0.001','ether')
        });

        await Papers.methods.sendReviewRequest(web3StringToBytes32('test'),
          web3StringToBytes32('I am reviewer')).send({
            from: accounts[0],
            gas: '4712388'
        });
        // attempting to review from same account should not be possible
        // assert(false);
    } catch (err) {
      // since attempting to review from same account should cause error, this is correct
      assert(err);
    }
  });

  it('request review for an uploaded paper (sendReviewRequest) (different accounts for uploader and reviewer, paper is not bought)', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
      from: accounts[1],
      gas: '4712388'
    });

    await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
        web3.utils.toWei('0.001','ether'),
        web3.utils.toWei('0.002','ether'),
        1, web3StringToBytes32('test'),
        web3StringToBytes32('test'),
        Date.now()/1000 + 24*60*60).send({
          from: accounts[0],
          gas: '4712388',
          value: web3.utils.toWei('0.002','ether')
    });

    try{
      await Papers.methods.sendReviewRequest(web3StringToBytes32('test'),
        web3StringToBytes32('I am reviewer')).send({
        from: accounts[1],
        gas: '4712388'
      });
      assert(false);
    } catch (err) {
      // attempting to request review without bought paper should fail
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }

  });

  it('request review for an uploaded paper (sendReviewRequest) (different accounts for uploader and reviewer, paper is bought)', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
      from: accounts[1],
      gas: '4712388'
    });

    await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
        web3.utils.toWei('0.001','ether'),
        web3.utils.toWei('0.002','ether'),
        1, web3StringToBytes32('test'),
        web3StringToBytes32('test'),
        Date.now()/1000 + 24*60*60).send({
          from: accounts[0],
          gas: '4712388',
          value: web3.utils.toWei('0.002','ether')
    });

    await  Papers.methods.buyPaper(web3StringToBytes32('test')).send({
            from: accounts[1],
            gas: '4712388',
            value: web3.utils.toWei('0.001','ether')
    });

    await Papers.methods.sendReviewRequest(web3StringToBytes32('test'),
        web3StringToBytes32('I am reviewer')).send({
        from: accounts[1],
        gas: '4712388'
      });

    returnCode = await Papers.methods.getPaperFinalState(web3StringToBytes32('test')).call({
        from: accounts[0],
        gas: '4712388'
    });

    // state should not be final
    assert.equal(false,returnCode);

    // check review state
    returnCode = await Papers.methods.getReviewState(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer')).call({
        from: accounts[0],
        gas: '4712388'
      });
    // should return 0 - not yet answered
    assert.equal(0,returnCode);

  });

  it('request review for an uploaded paper (sendReviewRequest) and accept review request (answerReviewRequest), check reviewList and reviewState', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
      from: accounts[1],
      gas: '4712388'
    });

    await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
        web3.utils.toWei('0.001','ether'),
        web3.utils.toWei('0.002','ether'),
        1, web3StringToBytes32('test'),
        web3StringToBytes32('test'),
        Date.now()/1000 + 24*60*60).send({
          from: accounts[0],
          gas: '4712388',
          value: web3.utils.toWei('0.002','ether')
    });

    await  Papers.methods.buyPaper(web3StringToBytes32('test')).send({
            from: accounts[1],
            gas: '4712388',
            value: web3.utils.toWei('0.001','ether')
    });

    returnCode = await  Papers.methods.getPaperReviewList(web3StringToBytes32('test')).call({
            from: accounts[0]
    });

    // should be empty now
    assert.equal(0, returnCode);

    await Papers.methods.sendReviewRequest(web3StringToBytes32('test'),
        web3StringToBytes32('I am reviewer')).send({
        from: accounts[1],
        gas: '4712388'
      });

    returnCode = await  Papers.methods.getPaperReviewList(web3StringToBytes32('test')).call({
                from: accounts[0]
    });

    // should now contain entry
    assert.equal(web3StringToBytes32('I am reviewer'), returnCode[0]);

    returnCode = await Papers.methods.getReviewState(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer')).call({
        from: accounts[0],
        gas: '4712388'
      });
    // review state should be 0
    assert.equal(0,returnCode);

    await Papers.methods.answerReviewRequest(web3StringToBytes32('test'),
        web3StringToBytes32('I am reviewer'), true).send({
        from: accounts[0],
        gas: '4712388'
      });

    // check review state
    returnCode = await Papers.methods.getReviewState(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer')).call({
        from: accounts[0],
        gas: '4712388'
      });
    // review state should be 1 because review has been accepted
    assert.equal(1,returnCode);

  });

  it('make 3 review requests for an uploaded paper (sendReviewRequest) with 3 review slots and accept 2 review requests (answerReviewRequest), check getPaperReviewList, getReviewRequestExists and reviewState', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
      from: accounts[1],
      gas: '4712388'
    });
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user3')).send({
      from: accounts[2],
      gas: '4712388'
    });
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user4')).send({
      from: accounts[3],
      gas: '4712388'
    });

    await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
        web3.utils.toWei('0.001','ether'),
        web3.utils.toWei('0.002','ether'),
        3, web3StringToBytes32('test'),
        web3StringToBytes32('test'),
        Date.now()/1000 + 24*60*60).send({
          from: accounts[0],
          gas: '4712388',
          value: web3.utils.toWei('0.006','ether')
    });

    await  Papers.methods.buyPaper(web3StringToBytes32('test')).send({
            from: accounts[1],
            gas: '4712388',
            value: web3.utils.toWei('0.001','ether')
    });

    await  Papers.methods.buyPaper(web3StringToBytes32('test')).send({
            from: accounts[2],
            gas: '4712388',
            value: web3.utils.toWei('0.001','ether')
    });

    await  Papers.methods.buyPaper(web3StringToBytes32('test')).send({
            from: accounts[3],
            gas: '4712388',
            value: web3.utils.toWei('0.001','ether')
    });

    returnCode = await  Papers.methods.getPaperReviewList(web3StringToBytes32('test')).call({
            from: accounts[0]
    });

    // should be empty now
    assert.equal(0, returnCode);

    await Papers.methods.sendReviewRequest(web3StringToBytes32('test'),
        web3StringToBytes32('I am reviewer 1')).send({
        from: accounts[1],
        gas: '4712388'
    });

    await Papers.methods.sendReviewRequest(web3StringToBytes32('test'),
          web3StringToBytes32('I am reviewer 2')).send({
          from: accounts[2],
          gas: '4712388'
    });

    await Papers.methods.sendReviewRequest(web3StringToBytes32('test'),
            web3StringToBytes32('I am reviewer 3')).send({
            from: accounts[3],
            gas: '4712388'
    });

    returnCode = await  Papers.methods.getPaperReviewList(web3StringToBytes32('test')).call({
                from: accounts[0]
    });

    // should now contain entry
    assert.equal(web3StringToBytes32('I am reviewer 1'), returnCode[0]);
    assert.equal(web3StringToBytes32('I am reviewer 2'), returnCode[1]);
    assert.equal(web3StringToBytes32('I am reviewer 3'), returnCode[2]);

    returnCode = await Papers.methods.getReviewState(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer 1')).call({
        from: accounts[0],
        gas: '4712388'
    });
    // review state should be 0
    assert.equal(0,returnCode);

    returnCode = await Papers.methods.getReviewState(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer 2')).call({
        from: accounts[0],
        gas: '4712388'
    });
    // review state should be 0
    assert.equal(0,returnCode);

    returnCode = await Papers.methods.getReviewState(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer 3')).call({
        from: accounts[0],
        gas: '4712388'
    });
    // review state should be 0
    assert.equal(0,returnCode);

    returnCode = await Papers.methods.getReviewRequestExists(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer 1')).call({
        from: accounts[0],
        gas: '4712388'
    });
    // review state should be true
    assert.equal(true,returnCode);

    returnCode = await Papers.methods.getReviewRequestExists(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer 2')).call({
        from: accounts[0],
        gas: '4712388'
    });
    // review state should be true
    assert.equal(true,returnCode);

    returnCode = await Papers.methods.getReviewRequestExists(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer 3')).call({
        from: accounts[0],
        gas: '4712388'
    });
    // review state should be true
    assert.equal(true,returnCode);

    await Papers.methods.answerReviewRequest(web3StringToBytes32('test'),
        web3StringToBytes32('I am reviewer 1'), true).send({
        from: accounts[0],
        gas: '4712388'
    });

    await Papers.methods.answerReviewRequest(web3StringToBytes32('test'),
          web3StringToBytes32('I am reviewer 2'), true).send({
          from: accounts[0],
          gas: '4712388'
    });

    // check review state
    returnCode = await Papers.methods.getReviewState(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer 1')).call({
        from: accounts[0],
        gas: '4712388'
    });
    // review state should be 1 because review has been accepted
    assert.equal(1,returnCode);

    // check review state
    returnCode = await Papers.methods.getReviewState(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer 2')).call({
        from: accounts[0],
        gas: '4712388'
    });
    // review state should be 1 because review has been accepted
    assert.equal(1,returnCode);

    // check review state
    returnCode = await Papers.methods.getReviewState(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer 3')).call({
        from: accounts[0],
        gas: '4712388'
    });

    // review state should be 0 because review has not been accepted
    assert.equal(0,returnCode);

  });

  it('call getReviewOwner when no paper exists', async () => {

    try{
      returnCode = await Papers.methods.getReviewOwner(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer')).call({
        from: accounts[0],
        gas: '4712388'
      });
      // should fail
      assert(false);
    } catch (err) {
      // attempting to call user details should fail
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }

  });

  it('call getReviewOwner when paper exists but no review request exists', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });

    try{
      await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
          web3.utils.toWei('0.001','ether'),
          web3.utils.toWei('0.002','ether'),
          3, web3StringToBytes32('test'),
          web3StringToBytes32('test'),
          Date.now()/1000 + 24*60*60).send({
            from: accounts[0],
            gas: '4712388',
            value: web3.utils.toWei('0.006','ether')
      });

      returnCode = await Papers.methods.getReviewOwner(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer')).call({
        from: accounts[0],
        gas: '4712388'
      });
      // should fail
      assert(false);
    } catch (err) {
      // attempting to call user details should fail
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }

  });

  it('make 2 review requests for 2 uploaded papers (sendReviewRequest) for 2 reviewers check getReviewOwner', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
      from: accounts[1],
      gas: '4712388'
    });
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user3')).send({
      from: accounts[2],
      gas: '4712388'
    });


    await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
        web3.utils.toWei('0.001','ether'),
        web3.utils.toWei('0.002','ether'),
        1, web3StringToBytes32('test'),
        web3StringToBytes32('test'),
        Date.now()/1000 + 24*60*60).send({
          from: accounts[0],
          gas: '4712388',
          value: web3.utils.toWei('0.002','ether')
    });

    await  Papers.methods.uploadPaper(web3StringToBytes32('test2'),
        web3.utils.toWei('0.001','ether'),
        web3.utils.toWei('0.002','ether'),
        1, web3StringToBytes32('test'),
        web3StringToBytes32('test'),
        Date.now()/1000 + 24*60*60).send({
          from: accounts[0],
          gas: '4712388',
          value: web3.utils.toWei('0.002','ether')
    });

    await  Papers.methods.uploadPaper(web3StringToBytes32('test3'),
        web3.utils.toWei('0.001','ether'),
        web3.utils.toWei('0.002','ether'),
        1, web3StringToBytes32('test'),
        web3StringToBytes32('test'),
        Date.now()/1000 + 24*60*60).send({
          from: accounts[0],
          gas: '4712388',
          value: web3.utils.toWei('0.002','ether')
    });

    await  Papers.methods.buyPaper(web3StringToBytes32('test')).send({
            from: accounts[1],
            gas: '4712388',
            value: web3.utils.toWei('0.001','ether')
    });

    await  Papers.methods.buyPaper(web3StringToBytes32('test2')).send({
            from: accounts[2],
            gas: '4712388',
            value: web3.utils.toWei('0.001','ether')
    });

    await Papers.methods.sendReviewRequest(web3StringToBytes32('test'),
        web3StringToBytes32('I am reviewer 1')).send({
        from: accounts[1],
        gas: '4712388'
    });

    await Papers.methods.sendReviewRequest(web3StringToBytes32('test2'),
          web3StringToBytes32('I am reviewer 2')).send({
          from: accounts[2],
          gas: '4712388'
    });

    returnCode = await  Papers.methods.getReviewOwner(web3StringToBytes32('test'),
        web3StringToBytes32('I am reviewer 1')).call({
            from: accounts[0]
    });

    // check request owner address
    assert.equal(accounts[1],returnCode);

    returnCode = await  Papers.methods.getReviewOwner(web3StringToBytes32('test2'),
        web3StringToBytes32('I am reviewer 2')).call({
            from: accounts[0]
    });

    // check request owner address
    assert.equal(accounts[2],returnCode);

  });

  it('make 3 review requests for 3 uploaded papers (sendReviewRequest) check getOwnerReviewList', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
      from: accounts[1],
      gas: '4712388'
    });

    await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
        web3.utils.toWei('0.001','ether'),
        web3.utils.toWei('0.002','ether'),
        1, web3StringToBytes32('test'),
        web3StringToBytes32('test'),
        Date.now()/1000 + 24*60*60).send({
          from: accounts[0],
          gas: '4712388',
          value: web3.utils.toWei('0.002','ether')
    });

    await  Papers.methods.uploadPaper(web3StringToBytes32('test2'),
        web3.utils.toWei('0.001','ether'),
        web3.utils.toWei('0.002','ether'),
        1, web3StringToBytes32('test'),
        web3StringToBytes32('test'),
        Date.now()/1000 + 24*60*60).send({
          from: accounts[0],
          gas: '4712388',
          value: web3.utils.toWei('0.002','ether')
    });

    await  Papers.methods.uploadPaper(web3StringToBytes32('test3'),
        web3.utils.toWei('0.001','ether'),
        web3.utils.toWei('0.002','ether'),
        1, web3StringToBytes32('test'),
        web3StringToBytes32('test'),
        Date.now()/1000 + 24*60*60).send({
          from: accounts[0],
          gas: '4712388',
          value: web3.utils.toWei('0.002','ether')
    });

    await  Papers.methods.buyPaper(web3StringToBytes32('test')).send({
            from: accounts[1],
            gas: '4712388',
            value: web3.utils.toWei('0.001','ether')
    });

    await  Papers.methods.buyPaper(web3StringToBytes32('test2')).send({
            from: accounts[1],
            gas: '4712388',
            value: web3.utils.toWei('0.001','ether')
    });

    await  Papers.methods.buyPaper(web3StringToBytes32('test3')).send({
            from: accounts[1],
            gas: '4712388',
            value: web3.utils.toWei('0.001','ether')
    });

    returnCode = await  Papers.methods.getOwnerReviewList(accounts[1]).call({
            from: accounts[1]
    });

    // should be empty now
    assert.equal(0, returnCode);

    await Papers.methods.sendReviewRequest(web3StringToBytes32('test'),
        web3StringToBytes32('I am reviewer 1')).send({
        from: accounts[1],
        gas: '4712388'
    });

    await Papers.methods.sendReviewRequest(web3StringToBytes32('test2'),
          web3StringToBytes32('I am reviewer 2')).send({
          from: accounts[1],
          gas: '4712388'
    });

    await Papers.methods.sendReviewRequest(web3StringToBytes32('test3'),
            web3StringToBytes32('I am reviewer 3')).send({
            from: accounts[1],
            gas: '4712388'
    });

    returnCode = await  Papers.methods.getOwnerReviewList(accounts[1]).call({
            from: accounts[1]
    });

    // should contain entries now
    assert.equal(web3StringToBytes32('I am reviewer 1'),returnCode[0]);
    assert.equal(web3StringToBytes32('I am reviewer 2'),returnCode[1]);
    assert.equal(web3StringToBytes32('I am reviewer 3'),returnCode[2]);

  });

  it('call getUserDetailsIPFS when no user exists', async () => {

    try{
      returnCode = await Papers.methods.getUserDetailsIPFS(accounts[0]).call({
        from: accounts[0],
        gas: '4712388'
      });
      assert(false);
    } catch (err) {
      // attempting to call user details should fail
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }

  });

  it('call getOwnerReviewList when no user exists', async () => {

    try{
      returnCode = await Papers.methods.getOwnerReviewList(accounts[0]).call({
        from: accounts[0],
        gas: '4712388'
      });
      assert.equal(0, returnCode);
    } catch (err) {
      // attempting to call user details should fail
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }

  });

  it('call getReviewRequestExists when no paper exists', async () => {

    try{
      returnCode = await Papers.methods.getReviewRequestExists(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer')).call({
        from: accounts[0],
        gas: '4712388'
      });
      // should fail
      assert(false);
    } catch (err) {
      // attempting to call user details should fail
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }

  });

  it('call getReviewRequestExists when paper exists but no review request exists', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });

    try{
      await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
          web3.utils.toWei('0.001','ether'),
          web3.utils.toWei('0.002','ether'),
          3, web3StringToBytes32('test'),
          web3StringToBytes32('test'),
          Date.now()/1000 + 24*60*60).send({
            from: accounts[0],
            gas: '4712388',
            value: web3.utils.toWei('0.006','ether')
      });

      returnCode = await Papers.methods.getReviewRequestExists(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer')).call({
        from: accounts[0],
        gas: '4712388'
      });
      // should fail
      assert(false);
    } catch (err) {
      // attempting to call user details should fail
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }

  });

  it('call getReviewExists when no paper exists', async () => {

    try{
      returnCode = await Papers.methods.getReviewExists(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer')).call({
        from: accounts[0],
        gas: '4712388'
      });
      // should fail
      assert(false);
    } catch (err) {
      // attempting to call user details should fail
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }

  });

  it('call getReviewExists when paper exists but no review request exists', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });

    try{
      await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
          web3.utils.toWei('0.001','ether'),
          web3.utils.toWei('0.002','ether'),
          3, web3StringToBytes32('test'),
          web3StringToBytes32('test'),
          Date.now()/1000 + 24*60*60).send({
            from: accounts[0],
            gas: '4712388',
            value: web3.utils.toWei('0.006','ether')
      });

      returnCode = await Papers.methods.getReviewExists(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer')).call({
        from: accounts[0],
        gas: '4712388'
      });
      // should fail
      assert(false);
    } catch (err) {
      // attempting to call user details should fail
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }

  });

  it('call getReviewExists when paper exists, request exists but no review exists', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
      from: accounts[1],
      gas: '4712388'
    });

    try{
      await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
          web3.utils.toWei('0.001','ether'),
          web3.utils.toWei('0.002','ether'),
          3, web3StringToBytes32('test'),
          web3StringToBytes32('test'),
          Date.now()/1000 + 24*60*60).send({
            from: accounts[0],
            gas: '4712388',
            value: web3.utils.toWei('0.006','ether')
      });

      await  Papers.methods.buyPaper(web3StringToBytes32('test')).send({
              from: accounts[1],
              gas: '4712388',
              value: web3.utils.toWei('0.001','ether')
      });

      await Papers.methods.sendReviewRequest(web3StringToBytes32('test'),
          web3StringToBytes32('I am reviewer')).send({
          from: accounts[1],
          gas: '4712388'
      });

      returnCode = await Papers.methods.getReviewExists(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer')).call({
        from: accounts[0],
        gas: '4712388'
      });
      // should fail
      assert(false);
    } catch (err) {
      // attempting to call user details should fail
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }

  });

  it('call getOwnerPaperUploadList when no user exists', async () => {

    try{
      returnCode = await Papers.methods.getOwnerPaperUploadList(accounts[0]).call({
        from: accounts[0],
        gas: '4712388'
      });
      assert.equal(0, returnCode);
    } catch (err) {
      // attempting to call user details should fail
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }

  });

/* commented out after discussion with team as price is a uint in the solidity contract
  it('call uploadFinalPaper with a negative price', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });

    try{
      await Papers.methods.uploadFinalPaper(web3StringToBytes32('test'),
          web3.utils.toWei('-0.001','ether'), web3StringToBytes32('main'),
          web3StringToBytes32('sub')).send({
        from: accounts[0],
        gas: '4712388'


      });
      // should fail


      assert.equal(false);
    } catch (err) {
      // attempting to call user details should fail
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }

  });
*/

  it('call uploadFinalPaper when no user details exist', async () => {
    try{
      await Papers.methods.uploadFinalPaper(web3StringToBytes32('test'),
          web3.utils.toWei('0.001','ether'), web3StringToBytes32('main'),
          web3StringToBytes32('sub')).send({
        from: accounts[0],
        gas: '4712388'
      });
      // should fail
      assert.equal(false);
    } catch (err) {
      // attempting to call user details should fail
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }

  });

  it('call uploadFinalPaper for 2 papers with a valid price, check FinalState and check price, main/sub field are set correctly', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });

      await Papers.methods.uploadFinalPaper(web3StringToBytes32('test'),
          web3.utils.toWei('0.001','ether'), web3StringToBytes32('main1'),
          web3StringToBytes32('sub1')).send({
        from: accounts[0],
        gas: '4712388'
      });

      await Papers.methods.uploadFinalPaper(web3StringToBytes32('test2'),
          web3.utils.toWei('0.002','ether'), web3StringToBytes32('main2'),
          web3StringToBytes32('sub2')).send({
        from: accounts[0],
        gas: '4712388'
      });

      const allPapers = await Papers.methods.getAllPapers().call({
          from: accounts[0]
        });
      // allPapers should contain the uploaded hash
      assert.equal(web3StringToBytes32('test'), allPapers[0]);
      assert.equal(web3StringToBytes32('test2'), allPapers[1]);

      returnCode = await Papers.methods.getPaperArrayLength().call({
        from: accounts[0]
      });
      assert.equal(2, returnCode);

      returnCode = await Papers.methods.getPaperVersionsLength(web3StringToBytes32('test')).call({
        from: accounts[0]
      });
      assert.equal(1, returnCode);

      returnCode = await Papers.methods.getPaperVersionsLength(web3StringToBytes32('test2')).call({
        from: accounts[0]
      });
      assert.equal(1, returnCode);

      returnCode = await Papers.methods.getNewestPaperIPFS(web3StringToBytes32('test')).call({
        from: accounts[0]
      });
      assert.equal(web3StringToBytes32('test'), returnCode);

      returnCode = await Papers.methods.getNewestPaperIPFS(web3StringToBytes32('test2')).call({
        from: accounts[0]
      });
      assert.equal(web3StringToBytes32('test2'), returnCode);

      returnCode = await Papers.methods.getPaperOwner(web3StringToBytes32('test')).call({
        from: accounts[2]
      });
      assert.equal(accounts[0], returnCode);

      returnCode = await Papers.methods.getPaperPrice(web3StringToBytes32('test')).call({
        from: accounts[0]
      });
      assert.equal(web3.utils.toWei('0.001','ether'), returnCode);

      returnCode = await Papers.methods.getPaperPrice(web3StringToBytes32('test2')).call({
        from: accounts[0]
      });
      assert.equal(web3.utils.toWei('0.002','ether'), returnCode);

      returnCode = await Papers.methods.getMainField(web3StringToBytes32('test')).call({
        from: accounts[0]
      });
      assert.equal(web3StringToBytes32('main1'), returnCode);

      returnCode = await Papers.methods.getMainField(web3StringToBytes32('test2')).call({
        from: accounts[0]
      });
      assert.equal(web3StringToBytes32('main2'), returnCode);

      returnCode = await Papers.methods.getSubField(web3StringToBytes32('test')).call({
        from: accounts[0]
      });
      assert.equal(web3StringToBytes32('sub1'), returnCode);

      returnCode = await Papers.methods.getSubField(web3StringToBytes32('test2')).call({
        from: accounts[0]
      });
      assert.equal(web3StringToBytes32('sub2'), returnCode);

      returnCode = await Papers.methods.getPaperFinalState(web3StringToBytes32('test')).call({
        from: accounts[0],
        gas: '4712388'
      });

      assert.equal(true,returnCode);
      returnCode = await Papers.methods.getPaperFinalState(web3StringToBytes32('test2')).call({
        from: accounts[0],
        gas: '4712388'
      });

      assert.equal(true,returnCode);

  });

  it('call setUserDetailsIPFS and getUserDetailsIPFS', async () => {

      await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('xxxx')).send({
        from: accounts[0],
        gas: '4712388'
      });

      await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('test')).send({
        from: accounts[0],
        gas: '4712388'
      });

      await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('xxxx')).send({
        from: accounts[1],
        gas: '4712388'
      });

      await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('test2')).send({
        from: accounts[1],
        gas: '4712388'
      });

      returnCode = await Papers.methods.getUserDetailsIPFS(accounts[0]).call({
        from: accounts[0],
        gas: '4712388'
      });

      assert.equal(web3StringToBytes32('test'), returnCode);

      returnCode = await Papers.methods.getUserDetailsIPFS(accounts[1]).call({
        from: accounts[0],
        gas: '4712388'
      });

      assert.equal(web3StringToBytes32('test2'), returnCode);

  });

  it('call getReviewIpfsHash when no paper or request exists', async () => {

    try{
      await Papers.methods.getReviewIpfsHash(web3StringToBytes32('test'),
        web3StringToBytes32('I am reviewer')).call({
        from: accounts[1],
        gas: '4712388'
      });
      assert(false);
    } catch (err) {
      // attempting to request review without bought paper should fail
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }

  });

  it('call getReviewIpfsHash after a paper has been uploaded and review requested but no review exists', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
      from: accounts[1],
      gas: '4712388'
    });

    await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
        web3.utils.toWei('0.001','ether'),
        web3.utils.toWei('0.002','ether'),
        1, web3StringToBytes32('test'),
        web3StringToBytes32('test'),
        Date.now()/1000 + 24*60*60).send({
          from: accounts[0],
          gas: '4712388',
          value: web3.utils.toWei('0.002','ether')
    });

    await  Papers.methods.buyPaper(web3StringToBytes32('test')).send({
            from: accounts[1],
            gas: '4712388',
            value: web3.utils.toWei('0.001','ether')
    });

    await Papers.methods.sendReviewRequest(web3StringToBytes32('test'),
        web3StringToBytes32('I am reviewer')).send({
        from: accounts[1],
        gas: '4712388'
      });

    returnCode = await Papers.methods.getReviewState(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer')).call({
        from: accounts[0],
        gas: '4712388'
      });
    // review state should be 0
    assert.equal(0,returnCode);

/*    await Papers.methods.answerReviewRequest(web3StringToBytes32('test'),
        web3StringToBytes32('I am reviewer'), true).send({
        from: accounts[0],
        gas: '4712388'
      });

    // check review state
    returnCode = await Papers.methods.getReviewState(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer')).call({
        from: accounts[0],
        gas: '4712388'
      });
    // review state should be 1 because review has been accepted
    assert.equal(1,returnCode);
*/
    try{
      returnCode = await Papers.methods.getReviewIpfsHash(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer')).call({
        from: accounts[0],
        gas: '4712388'
      });
      // the way this is implemented now it will not throw but return 0
      assert.equal(0, returnCode);
      // assert(false);
    } catch (err) {
      // attempting to request review without bought paper should fail
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }

  });

  it('call buyPaper and sendReviewRequest when no user details exist', async () => {
    // need to register User1 for upload first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });

    await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
        web3.utils.toWei('0.001','ether'),
        web3.utils.toWei('0.002','ether'),
        1, web3StringToBytes32('test'),
        web3StringToBytes32('test'),
        Date.now()/1000 + 24*60*60).send({
          from: accounts[0],
          gas: '4712388',
          value: web3.utils.toWei('0.002','ether')
    });

    try{
      await  Papers.methods.buyPaper(web3StringToBytes32('test')).send({
              from: accounts[1],
              gas: '4712388',
              value: web3.utils.toWei('0.001','ether')
      });

      await Papers.methods.sendReviewRequest(web3StringToBytes32('test'),
          web3StringToBytes32('I am reviewer')).send({
          from: accounts[1],
          gas: '4712388'
        });
      // should fail
      assert(false);
    } catch (err) {
      // attempting to request review without bought paper should fail
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);
      }
    }
  });

  it('request review for an uploaded paper (sendReviewRequest) and decline review request (answerReviewRequest), check reviewList and reviewState', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
      from: accounts[1],
      gas: '4712388'
    });

    await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
        web3.utils.toWei('0.001','ether'),
        web3.utils.toWei('0.002','ether'),
        1, web3StringToBytes32('test'),
        web3StringToBytes32('test'),
        Date.now()/1000 + 24*60*60).send({
          from: accounts[0],
          gas: '4712388',
          value: web3.utils.toWei('0.002','ether')
    });

    returnCode = await  Papers.methods.getPaperReviewList(web3StringToBytes32('test')).call({
            from: accounts[0]
    });

    // should be empty now
    assert.equal(0, returnCode);

    await  Papers.methods.buyPaper(web3StringToBytes32('test')).send({
            from: accounts[1],
            gas: '4712388',
            value: web3.utils.toWei('0.001','ether')
    });

    await Papers.methods.sendReviewRequest(web3StringToBytes32('test'),
        web3StringToBytes32('I am reviewer')).send({
        from: accounts[1],
        gas: '4712388'
      });

    returnCode = await  Papers.methods.getPaperReviewList(web3StringToBytes32('test')).call({
              from: accounts[0]
    });

    // should now contain entry
    assert.equal(web3StringToBytes32('I am reviewer'), returnCode[0]);

    returnCode = await Papers.methods.getReviewState(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer')).call({
        from: accounts[0],
        gas: '4712388'
      });
    // review state should be 0
    assert.equal(0,returnCode);

    await Papers.methods.answerReviewRequest(web3StringToBytes32('test'),
        web3StringToBytes32('I am reviewer'), false).send({
        from: accounts[0],
        gas: '4712388'
      });

      returnCode = await Papers.methods.getReviewState(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer')).call({
          from: accounts[0],
          gas: '4712388'
        });
      // review state should be 4 - declined request
      assert.equal(4,returnCode);

  });

  it('request review for an uploaded, bought paper (sendReviewRequest), accept review request (answerReviewRequest) and upload review, check getReviewIpfsHash, check getReviewExists', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
      from: accounts[1],
      gas: '4712388'
    });

    await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
        web3.utils.toWei('0.001','ether'),
        web3.utils.toWei('0.002','ether'),
        1, web3StringToBytes32('test'),
        web3StringToBytes32('test'),
        Date.now()/1000 + 24*60*60).send({
          from: accounts[0],
          gas: '4712388',
          value: web3.utils.toWei('0.002','ether')
    });

    await  Papers.methods.buyPaper(web3StringToBytes32('test')).send({
            from: accounts[1],
            gas: '4712388',
            value: web3.utils.toWei('0.001','ether')
    });

    await Papers.methods.sendReviewRequest(web3StringToBytes32('test'),
        web3StringToBytes32('I am reviewer')).send({
        from: accounts[1],
        gas: '4712388'
      });

    returnCode = await Papers.methods.getReviewState(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer')).call({
          from: accounts[0],
          gas: '4712388'
    });
    // review state should be 0
    assert.equal(0,returnCode);

    await Papers.methods.answerReviewRequest(web3StringToBytes32('test'),
        web3StringToBytes32('I am reviewer'), true).send({
        from: accounts[0],
        gas: '4712388'
      });

    // check review state
    returnCode = await Papers.methods.getReviewState(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer')).call({
        from: accounts[0],
        gas: '4712388'
      });
    // review state should be 1 because review has been accepted
    assert.equal(1,returnCode);

    await Papers.methods.uploadReview(web3StringToBytes32('test'),
        web3StringToBytes32('I am reviewer'), web3StringToBytes32('review')).send({
        from: accounts[1],
        gas: '4712388'
      });

    returnCode = await Papers.methods.getReviewState(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer')).call({
          from: accounts[0],
          gas: '4712388'
    });
    // review state should be 2 (uploaded)
    assert.equal(2,returnCode);

    // check getReviewIpfsHash
    returnCode = await Papers.methods.getReviewIpfsHash(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer')).call({
      from: accounts[0],
      gas: '4712388'
    });

    assert.equal(web3StringToBytes32('review'),returnCode);

    // check getReviewExists
    returnCode = await Papers.methods.getReviewExists(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer')).call({
      from: accounts[0],
      gas: '4712388'
    });

    assert.equal(true,returnCode);
  });

  it('request review for an uploaded, bought paper (sendReviewRequest), accept review (answerReviewRequest), upload review, accept Review, check review money is transferred', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
      from: accounts[1],
      gas: '4712388'
    });

    await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
        web3.utils.toWei('0.001','ether'),
        web3.utils.toWei('0.002','ether'),
        1, web3StringToBytes32('test'),
        web3StringToBytes32('test'),
        Date.now()/1000 + 24*60*60).send({
          from: accounts[0],
          gas: '4712388',
          value: web3.utils.toWei('0.002','ether')
    });

    await  Papers.methods.buyPaper(web3StringToBytes32('test')).send({
            from: accounts[1],
            gas: '4712388',
            value: web3.utils.toWei('0.001','ether')
    });

    await Papers.methods.sendReviewRequest(web3StringToBytes32('test'),
        web3StringToBytes32('I am reviewer')).send({
        from: accounts[1],
        gas: '4712388'
      });

    returnCode = await Papers.methods.getReviewState(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer')).call({
          from: accounts[0],
          gas: '4712388'
    });
    // review state should be 0
    assert.equal(0,returnCode);

    await Papers.methods.answerReviewRequest(web3StringToBytes32('test'),
        web3StringToBytes32('I am reviewer'), true).send({
        from: accounts[0],
        gas: '4712388'
      });

    // check review state
    returnCode = await Papers.methods.getReviewState(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer')).call({
        from: accounts[0],
        gas: '4712388'
      });
    // review state should be 1 (no slots free) because review has been accepted
    assert.equal(1,returnCode);

    await Papers.methods.uploadReview(web3StringToBytes32('test'),
        web3StringToBytes32('I am reviewer'), web3StringToBytes32('review')).send({
        from: accounts[1],
        gas: '4712388'
      });

    returnCode = await Papers.methods.getReviewState(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer')).call({
          from: accounts[0],
          gas: '4712388'
    });
    // review state should be 2 (uploaded)
    assert.equal(2,returnCode);

    const initialBalance = await web3.eth.getBalance(accounts[1]);

    await Papers.methods.answerReview(web3StringToBytes32('test'),
        web3StringToBytes32('I am reviewer'), true).send({
        from: accounts[0],
        gas: '4712388'
    });

    returnCode = await Papers.methods.getReviewState(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer')).call({
          from: accounts[0],
          gas: '4712388'
    });
    // review state should be 6 (paymentSucceeded)
    assert.equal(6,returnCode);

    // accounts[1] should now have received money
    const newBalance = await web3.eth.getBalance(accounts[1]);

    assert( (newBalance - initialBalance) == web3.utils.toWei('0.002','ether'));

  });

  it('request review for an uploaded, bought paper (sendReviewRequest), accept review request (answerReviewRequest), upload review, sendReviewPayment (without accepting review), check review money is not transferred, check getPaperFinalState', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
      from: accounts[1],
      gas: '4712388'
    });

    await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
        web3.utils.toWei('0.001','ether'),
        web3.utils.toWei('0.002','ether'),
        1, web3StringToBytes32('test'),
        web3StringToBytes32('test'),
        Date.now()/1000 + 24*60*60).send({
          from: accounts[0],
          gas: '4712388',
          value: web3.utils.toWei('0.002','ether')
    });

    await  Papers.methods.buyPaper(web3StringToBytes32('test')).send({
            from: accounts[1],
            gas: '4712388',
            value: web3.utils.toWei('0.001','ether')
    });

    await Papers.methods.sendReviewRequest(web3StringToBytes32('test'),
        web3StringToBytes32('I am reviewer')).send({
        from: accounts[1],
        gas: '4712388'
      });

    await Papers.methods.answerReviewRequest(web3StringToBytes32('test'),
        web3StringToBytes32('I am reviewer'), true).send({
        from: accounts[0],
        gas: '4712388'
      });

    // check review state
    returnCode = await Papers.methods.getReviewState(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer')).call({
        from: accounts[0],
        gas: '4712388'
      });
    // review state should be 1 (no slots free) because review has been accepted
    assert.equal(1,returnCode);

    await Papers.methods.uploadReview(web3StringToBytes32('test'),
        web3StringToBytes32('I am reviewer'), web3StringToBytes32('review')).send({
        from: accounts[1],
        gas: '4712388'
      });

    returnCode = await Papers.methods.getReviewState(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer')).call({
          from: accounts[0],
          gas: '4712388'
    });
    // review state should be 2 (uploaded)
    assert.equal(2,returnCode);

    const initialBalance = await web3.eth.getBalance(accounts[1]);

    // should fail because state is not review accepted and not enoguth time has passed
    try{
      await Papers.methods.sendReviewPayment(web3StringToBytes32('test'),
        web3StringToBytes32('I am reviewer'), true).send({
        from: accounts[0],
        gas: '4712388'
      });
      assert(false);
    } catch (err) {
      // since calling this without accepting review should
      // cause the function to fail with an error
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);

        returnCode = await Papers.methods.getReviewState(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer')).call({
          from: accounts[0],
          gas: '4712388'
        });
        // review state should be 2
        assert.equal(2,returnCode);

        // accounts[1] should not have received money
        const newBalance = await web3.eth.getBalance(accounts[1]);

        assert( (newBalance - initialBalance) == web3.utils.toWei('0','ether'));

        returnCode = await Papers.methods.getPaperFinalState(web3StringToBytes32('test')).call({
          from: accounts[0],
          gas: '4712388'
        });

        assert.equal(false,returnCode);

      }
    }
  });

  it('request review for an uploaded, bought paper (sendReviewRequest), accept review request (answerReviewRequest), upload review, call setPaperFinalState without accepting review', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
      from: accounts[1],
      gas: '4712388'
    });

    await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
        web3.utils.toWei('0.001','ether'),
        web3.utils.toWei('0.002','ether'),
        1, web3StringToBytes32('test'),
        web3StringToBytes32('test'),
        Date.now()/1000 + 24*60*60).send({
          from: accounts[0],
          gas: '4712388',
          value: web3.utils.toWei('0.002','ether')
    });

    await  Papers.methods.buyPaper(web3StringToBytes32('test')).send({
            from: accounts[1],
            gas: '4712388',
            value: web3.utils.toWei('0.001','ether')
    });

    await Papers.methods.sendReviewRequest(web3StringToBytes32('test'),
        web3StringToBytes32('I am reviewer')).send({
        from: accounts[1],
        gas: '4712388'
      });

    await Papers.methods.answerReviewRequest(web3StringToBytes32('test'),
        web3StringToBytes32('I am reviewer'), true).send({
        from: accounts[0],
        gas: '4712388'
      });

    // check review state
    returnCode = await Papers.methods.getReviewState(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer')).call({
        from: accounts[0],
        gas: '4712388'
      });
    // review state should be 1 (no slots free) because review has been accepted
    assert.equal(1,returnCode);

    await Papers.methods.uploadReview(web3StringToBytes32('test'),
        web3StringToBytes32('I am reviewer'), web3StringToBytes32('review')).send({
        from: accounts[1],
        gas: '4712388'
      });

    returnCode = await Papers.methods.getReviewState(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer')).call({
          from: accounts[0],
          gas: '4712388'
    });
    // review state should be 2 (uploaded)
    assert.equal(2,returnCode);

    const initialBalance = await web3.eth.getBalance(accounts[1]);

    // should fail because state is not review accepted and not enoguth time has passed
    try{
      await Papers.methods.setFinalState(web3StringToBytes32('test')).send({
        from: accounts[0],
        gas: '4712388'
      });
      assert(false);
    } catch (err) {
      // since calling this without accepting review should
      // cause the function to fail with an error
      if (err instanceof AssertionError) {
        assert(false);
      } else {
        assert(err);

        returnCode = await Papers.methods.getReviewState(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer')).call({
          from: accounts[0],
          gas: '4712388'
        });
        // review state should be 2
        assert.equal(2,returnCode);

        // accounts[1] should not have received money
        const newBalance = await web3.eth.getBalance(accounts[1]);

        assert( (newBalance - initialBalance) == web3.utils.toWei('0','ether'));

        returnCode = await Papers.methods.getPaperFinalState(web3StringToBytes32('test')).call({
          from: accounts[0],
          gas: '4712388'
        });

        assert.equal(false,returnCode);

      }
    }
  });

  it('request review for an uploaded, bought paper (sendReviewRequest), accept review (answerReviewRequest), upload review, decline Review, setFinalState, check review money is transferred, check getPaperFinalState, check getReviewSlots, getReviewSlotsUsed, check getReviewSlotsFree', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
      from: accounts[1],
      gas: '4712388'
    });

    await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
        web3.utils.toWei('0.001','ether'),
        web3.utils.toWei('0.002','ether'),
        1, web3StringToBytes32('test'),
        web3StringToBytes32('test'),
        Date.now()/1000 + 24*60*60).send({
          from: accounts[0],
          gas: '4712388',
          value: web3.utils.toWei('0.002','ether')
    });

    await  Papers.methods.buyPaper(web3StringToBytes32('test')).send({
            from: accounts[1],
            gas: '4712388',
            value: web3.utils.toWei('0.001','ether')
    });

    // check review slots
    returnCode = await  Papers.methods.getReviewSlots(web3StringToBytes32('test')).call({
            from: accounts[0]
    });
    assert.equal (1, returnCode);

    // check review slots used
    returnCode = await  Papers.methods.getReviewSlotsUsed(web3StringToBytes32('test')).call({
            from: accounts[0]
    });
    assert.equal (0, returnCode);

    // check review slots used
    returnCode = await  Papers.methods.getReviewSlotsFree(web3StringToBytes32('test')).call({
            from: accounts[0]
    });
    assert.equal (1, returnCode);

    await Papers.methods.sendReviewRequest(web3StringToBytes32('test'),
        web3StringToBytes32('I am reviewer')).send({
        from: accounts[1],
        gas: '4712388'
      });

    // check review slots
    returnCode = await  Papers.methods.getReviewSlots(web3StringToBytes32('test')).call({
              from: accounts[0]
      });
    assert.equal (1, returnCode);

    // check review slots used
    returnCode = await  Papers.methods.getReviewSlotsUsed(web3StringToBytes32('test')).call({
              from: accounts[0]
      });
    assert.equal (0, returnCode);

    // check review slots used
    returnCode = await  Papers.methods.getReviewSlotsFree(web3StringToBytes32('test')).call({
            from: accounts[0]
    });
    assert.equal (1, returnCode);

    await Papers.methods.answerReviewRequest(web3StringToBytes32('test'),
        web3StringToBytes32('I am reviewer'), true).send({
        from: accounts[0],
        gas: '4712388'
      });

    // check review state
    returnCode = await Papers.methods.getReviewState(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer')).call({
        from: accounts[0],
        gas: '4712388'
      });
    // review state should be 1 (no slots free) because review has been accepted
    assert.equal(1,returnCode);

    // check review slots
    returnCode = await  Papers.methods.getReviewSlots(web3StringToBytes32('test')).call({
              from: accounts[0]
      });
    assert.equal (1, returnCode);

    // check review slots used
    returnCode = await  Papers.methods.getReviewSlotsUsed(web3StringToBytes32('test')).call({
              from: accounts[0]
      });
    assert.equal (1, returnCode);

    // check review slots used
    returnCode = await  Papers.methods.getReviewSlotsFree(web3StringToBytes32('test')).call({
            from: accounts[0]
    });
    assert.equal (0, returnCode);

    await Papers.methods.uploadReview(web3StringToBytes32('test'),
        web3StringToBytes32('I am reviewer'), web3StringToBytes32('review')).send({
        from: accounts[1],
        gas: '4712388'
      });

    returnCode = await Papers.methods.getReviewState(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer')).call({
          from: accounts[0],
          gas: '4712388'
    });
    // review state should be 2 (uploaded)
    assert.equal(2,returnCode);

    let initialBalance = await web3.eth.getBalance(accounts[1]);

    await Papers.methods.answerReview(web3StringToBytes32('test'),
        web3StringToBytes32('I am reviewer'), false).send({
        from: accounts[0],
        gas: '4712388'
    });

    returnCode = await Papers.methods.getReviewState(web3StringToBytes32('test'),web3StringToBytes32('I am reviewer')).call({
          from: accounts[0],
          gas: '4712388'
    });
    // review state should be 5 (declined)
    assert.equal(5,returnCode);

    // check review slots used
    returnCode = await  Papers.methods.getReviewSlotsUsed(web3StringToBytes32('test')).call({
              from: accounts[0]
      });
    assert.equal (0, returnCode);

    // check review slots used
    returnCode = await  Papers.methods.getReviewSlotsFree(web3StringToBytes32('test')).call({
            from: accounts[0]
    });
    assert.equal (1, returnCode);

    // accounts[1] should not have received money
    let newBalance = await web3.eth.getBalance(accounts[1]);

    assert( (newBalance - initialBalance) == web3.utils.toWei('0','ether'));

    returnCode = await Papers.methods.getPaperFinalState(web3StringToBytes32('test')).call({
          from: accounts[0],
          gas: '4712388'
    });

    assert.equal(false,returnCode);

    initialBalance = await web3.eth.getBalance(accounts[0]);
    await Papers.methods.setFinalState(web3StringToBytes32('test')).send({
        from: accounts[0],
        gas: '4712388'
    });
    newBalance = await web3.eth.getBalance(accounts[0]);

    returnCode = await Papers.methods.getPaperFinalState(web3StringToBytes32('test')).call({
          from: accounts[0],
          gas: '4712388'
    });

    assert.equal(true,returnCode);

    //console.log('paperBalance ='+ paperBalance);
    //console.log('initialBalance ='+ initialBalance);
    //console.log('newBalance ='+ newBalance);
    //console.log('newBalance minus initialBalance =' + (newBalance - initialBalance))
    // since calling setFinalState.send costs some gas, cannot check for equal to 0.002
    assert( (newBalance - initialBalance) > web3.utils.toWei('0.001','ether'));

    let paperBalance = await Papers.methods.getPaperBalance(web3StringToBytes32('test')).call({
        from: accounts[0],
        gas: '4712388'
    });

    // should be empty
    assert.equal(web3.utils.toWei('0','ether'),paperBalance);
  });

  it('setFinalState twice', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
      from: accounts[1],
      gas: '4712388'
    });

    await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
        web3.utils.toWei('0.001','ether'),
        web3.utils.toWei('0.002','ether'),
        1, web3StringToBytes32('test'),
        web3StringToBytes32('test'),
        Date.now()/1000 + 24*60*60).send({
          from: accounts[0],
          gas: '4712388',
          value: web3.utils.toWei('0.002','ether')
    });

    await  Papers.methods.buyPaper(web3StringToBytes32('test')).send({
            from: accounts[1],
            gas: '4712388',
            value: web3.utils.toWei('0.001','ether')
    });

    await Papers.methods.sendReviewRequest(web3StringToBytes32('test'),
        web3StringToBytes32('I am reviewer')).send({
        from: accounts[1],
        gas: '4712388'
      });

    await Papers.methods.answerReviewRequest(web3StringToBytes32('test'),
        web3StringToBytes32('I am reviewer'), true).send({
        from: accounts[0],
        gas: '4712388'
      });

    await Papers.methods.uploadReview(web3StringToBytes32('test'),
        web3StringToBytes32('I am reviewer'), web3StringToBytes32('review')).send({
        from: accounts[1],
        gas: '4712388'
      });

    await Papers.methods.answerReview(web3StringToBytes32('test'),
        web3StringToBytes32('I am reviewer'), true).send({
        from: accounts[0],
        gas: '4712388'
    });


    returnCode = await Papers.methods.getPaperFinalState(web3StringToBytes32('test')).call({
          from: accounts[0],
          gas: '4712388'
    });

    assert.equal(false,returnCode);

    await Papers.methods.setFinalState(web3StringToBytes32('test')).send({
        from: accounts[0],
        gas: '4712388'
    });

    returnCode = await Papers.methods.getPaperFinalState(web3StringToBytes32('test')).call({
          from: accounts[0],
          gas: '4712388'
    });

    assert.equal(true,returnCode);

    try{
      await Papers.methods.setFinalState(web3StringToBytes32('test')).send({
          from: accounts[0],
          gas: '4712388'
      });
      assert(false);
    } catch (err) {
        // this is correct - request should cause error
        if (err instanceof AssertionError) {
          assert(false);
        } else {
          assert(err);
        }
    }
  });

  it('request review for a paper which does not exist (sendReviewRequest)', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
      from: accounts[1],
      gas: '4712388'
    });

    // upload a dummy paper
    await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
        web3.utils.toWei('0.001','ether'),
        web3.utils.toWei('0.002','ether'),
        1, web3StringToBytes32('test'),
        web3StringToBytes32('test'),
        Date.now()/1000 + 24*60*60).send({
          from: accounts[0],
          gas: '4712388',
          value: web3.utils.toWei('0.002','ether')
    });
    // request a review for a different paper hash
    try{
      await Papers.methods.sendReviewRequest(web3StringToBytes32('different'),
        web3StringToBytes32('I am reviewer')).send({
        from: accounts[1],
        gas: '4712388'
      });
      // function no longer has returnCode
      // assert.equal(2,returnCode);
      // this should fail
      assert(false);
      } catch (err) {
        // this is correct - request should cause error
        if (err instanceof AssertionError) {
          assert(false);
        } else {
          assert(err);
        }
      }
  });


    it('call answerReviewRequest for a paper which does not exist', async () => {
      // need to register User first using setUserDetailsIPFS
      await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
        from: accounts[0],
        gas: '4712388'
      });

      // upload a dummy paper
      await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
          web3.utils.toWei('0.001','ether'),
          web3.utils.toWei('0.002','ether'),
          1, web3StringToBytes32('test'),
          web3StringToBytes32('test'),
          Date.now()/1000 + 24*60*60).send({
            from: accounts[0],
            gas: '4712388',
            value: web3.utils.toWei('0.002','ether')
      });
      // request a review for a different paper hash
      try{
        await Papers.methods.answerReviewRequest(web3StringToBytes32('different'),
          web3StringToBytes32('I am reviewer'), true).send({
          from: accounts[0],
          gas: '4712388'
        });
        // function no longer has returnCode
        // assert.equal(2,returnCode);
        // this should fail
        assert(false);
        } catch (err) {
          // this is correct - request should cause error
          if (err instanceof AssertionError) {
            assert(false);
          } else {
            assert(err);
          }
        }
    });

    it('call answerReviewRequest for a paper which exist, when no prior review request exists', async () => {
      // need to register User first using setUserDetailsIPFS
      await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
        from: accounts[0],
        gas: '4712388'
      });

      // upload a dummy paper
      await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
          web3.utils.toWei('0.001','ether'),
          web3.utils.toWei('0.002','ether'),
          1, web3StringToBytes32('test'),
          web3StringToBytes32('test'),
          Date.now()/1000 + 24*60*60).send({
            from: accounts[0],
            gas: '4712388',
            value: web3.utils.toWei('0.002','ether')
      });
      // request a review for a different paper hash
      try{
        await Papers.methods.answerReviewRequest(web3StringToBytes32('test'),
          web3StringToBytes32('I am reviewer'), true).send({
          from: accounts[0],
          gas: '4712388'
        });
        // function no longer has returnCode
        // assert.equal(2,returnCode);
        // this should fail
        assert(false);
        } catch (err) {
          // this is correct - request should cause error
          if (err instanceof AssertionError) {
            assert(false);
          } else {
            assert(err);
          }
        }
    });

    it('call answerReviewRequest for a paper which is set to finalState', async () => {
      // need to register User first using setUserDetailsIPFS
      await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
        from: accounts[0],
        gas: '4712388'
      });
      // need to register User first using setUserDetailsIPFS
      await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
        from: accounts[1],
        gas: '4712388'
      });

      // upload a dummy paper
      await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
          web3.utils.toWei('0.001','ether'),
          web3.utils.toWei('0.002','ether'),
          1, web3StringToBytes32('test'),
          web3StringToBytes32('test'),
          Date.now()/1000 + 24*60*60).send({
            from: accounts[0],
            gas: '4712388',
            value: web3.utils.toWei('0.002','ether')
      });

      await  Papers.methods.buyPaper(web3StringToBytes32('test')).send({
              from: accounts[1],
              gas: '4712388',
              value: web3.utils.toWei('0.001','ether')
      });

      await Papers.methods.sendReviewRequest(web3StringToBytes32('test'),
          web3StringToBytes32('I am reviewer')).send({
          from: accounts[1],
          gas: '4712388'
        });

      await Papers.methods.setFinalState(web3StringToBytes32('test')).send({
              from: accounts[0],
              gas: '4712388'
      });

      // answer review - should fail
      try{
        await Papers.methods.answerReviewRequest(web3StringToBytes32('test'),
          web3StringToBytes32('I am reviewer'), true).send({
          from: accounts[0],
          gas: '4712388'
        });
        // function no longer has returnCode
        // assert.equal(2,returnCode);
        // this should fail
        assert(false);
        } catch (err) {
          // this is correct - request should cause error
          if (err instanceof AssertionError) {
            assert(false);
          } else {
            assert(err);
          }
        }
    });

    it('call answerReviewRequest twice for a paper', async () => {
      // need to register User first using setUserDetailsIPFS
      await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
        from: accounts[0],
        gas: '4712388'
      });
      // need to register User first using setUserDetailsIPFS
      await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
        from: accounts[1],
        gas: '4712388'
      });

      // upload a dummy paper
      await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
          web3.utils.toWei('0.001','ether'),
          web3.utils.toWei('0.002','ether'),
          1, web3StringToBytes32('test'),
          web3StringToBytes32('test'),
          Date.now()/1000 + 24*60*60).send({
            from: accounts[0],
            gas: '4712388',
            value: web3.utils.toWei('0.002','ether')
      });

      await  Papers.methods.buyPaper(web3StringToBytes32('test')).send({
              from: accounts[1],
              gas: '4712388',
              value: web3.utils.toWei('0.001','ether')
      });

      await Papers.methods.sendReviewRequest(web3StringToBytes32('test'),
          web3StringToBytes32('I am reviewer')).send({
          from: accounts[1],
          gas: '4712388'
        });

      await Papers.methods.answerReviewRequest(web3StringToBytes32('test'),
          web3StringToBytes32('I am reviewer'), true).send({
          from: accounts[0],
          gas: '4712388'
        });

      // answer review - should fail
      try{
        await Papers.methods.answerReviewRequest(web3StringToBytes32('test'),
          web3StringToBytes32('I am reviewer'), true).send({
          from: accounts[0],
          gas: '4712388'
        });
        // function no longer has returnCode
        // assert.equal(2,returnCode);
        // this should fail
        assert(false);
        } catch (err) {
          // this is correct - request should cause error
          if (err instanceof AssertionError) {
            assert(false);
          } else {
            assert(err);
          }
        }
    });

    it('call uploadReview for a paper which does not exist', async () => {
      // need to register User first using setUserDetailsIPFS
      await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
        from: accounts[0],
        gas: '4712388'
      });
      // need to register User first using setUserDetailsIPFS
      await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
        from: accounts[1],
        gas: '4712388'
      });

      // upload a dummy paper
      await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
          web3.utils.toWei('0.001','ether'),
          web3.utils.toWei('0.002','ether'),
          1, web3StringToBytes32('test'),
          web3StringToBytes32('test'),
          Date.now()/1000 + 24*60*60).send({
            from: accounts[0],
            gas: '4712388',
            value: web3.utils.toWei('0.002','ether')
      });
      // request a review for a different paper hash
      try{
        await Papers.methods.uploadReview(web3StringToBytes32('different'),
          web3StringToBytes32('I am reviewer'),
          web3StringToBytes32('Review')).send({
          from: accounts[1],
          gas: '4712388'
        });
        // this should fail
        assert(false);
        } catch (err) {
          // this is correct - request should cause error
          if (err instanceof AssertionError) {
            assert(false);
          } else {
            assert(err);
          }
        }
    });

    it('call uploadReview for a paper which exist, when no prior review request exists', async () => {
      // need to register User first using setUserDetailsIPFS
      await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
        from: accounts[0],
        gas: '4712388'
      });
      // need to register User first using setUserDetailsIPFS
      await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
        from: accounts[1],
        gas: '4712388'
      });

      // upload a dummy paper
      await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
          web3.utils.toWei('0.001','ether'),
          web3.utils.toWei('0.002','ether'),
          1, web3StringToBytes32('test'),
          web3StringToBytes32('test'),
          Date.now()/1000 + 24*60*60).send({
            from: accounts[0],
            gas: '4712388',
            value: web3.utils.toWei('0.002','ether')
      });
      // upload a review - should fail
      try{
        await Papers.methods.uploadReview(web3StringToBytes32('test'),
          web3StringToBytes32('I am reviewer'),
          web3StringToBytes32('Review')).send({
          from: accounts[1],
          gas: '4712388'
        });
        // function no longer has returnCode
        // assert.equal(2,returnCode);
        // this should fail
        assert(false);
        } catch (err) {
          // this is correct - request should cause error
          if (err instanceof AssertionError) {
            assert(false);
          } else {
            assert(err);
          }
        }
    });

    it('call uploadReview for a paper which is set to finalState', async () => {
      // need to register User first using setUserDetailsIPFS
      await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
        from: accounts[0],
        gas: '4712388'
      });
      // need to register User first using setUserDetailsIPFS
      await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
        from: accounts[1],
        gas: '4712388'
      });

      // upload a dummy paper
      await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
          web3.utils.toWei('0.001','ether'),
          web3.utils.toWei('0.002','ether'),
          1, web3StringToBytes32('test'),
          web3StringToBytes32('test'),
          Date.now()/1000 + 24*60*60).send({
            from: accounts[0],
            gas: '4712388',
            value: web3.utils.toWei('0.002','ether')
      });

      await  Papers.methods.buyPaper(web3StringToBytes32('test')).send({
              from: accounts[1],
              gas: '4712388',
              value: web3.utils.toWei('0.001','ether')
      });

      await Papers.methods.sendReviewRequest(web3StringToBytes32('test'),
          web3StringToBytes32('I am reviewer')).send({
          from: accounts[1],
          gas: '4712388'
        });

      await Papers.methods.setFinalState(web3StringToBytes32('test')).send({
              from: accounts[0],
              gas: '4712388'
      });

      // upload review - should fail
      try{
        await Papers.methods.uploadReview(web3StringToBytes32('test'),
          web3StringToBytes32('I am reviewer'),
            web3StringToBytes32('review')).send({
          from: accounts[1],
          gas: '4712388'
        });
        // this should fail
        assert(false);
        } catch (err) {
          // this is correct - request should cause error
          if (err instanceof AssertionError) {
            assert(false);
          } else {
            assert(err);
          }
        }
    });

    it('call uploadReview twice for a paper', async () => {
      // need to register User first using setUserDetailsIPFS
      await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
        from: accounts[0],
        gas: '4712388'
      });
      // need to register User first using setUserDetailsIPFS
      await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
        from: accounts[1],
        gas: '4712388'
      });

      // upload a dummy paper
      await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
          web3.utils.toWei('0.001','ether'),
          web3.utils.toWei('0.002','ether'),
          1, web3StringToBytes32('test'),
          web3StringToBytes32('test'),
          Date.now()/1000 + 24*60*60).send({
            from: accounts[0],
            gas: '4712388',
            value: web3.utils.toWei('0.002','ether')
      });

      await  Papers.methods.buyPaper(web3StringToBytes32('test')).send({
              from: accounts[1],
              gas: '4712388',
              value: web3.utils.toWei('0.001','ether')
      });

      await Papers.methods.sendReviewRequest(web3StringToBytes32('test'),
          web3StringToBytes32('I am reviewer')).send({
          from: accounts[1],
          gas: '4712388'
        });

      await Papers.methods.answerReviewRequest(web3StringToBytes32('test'),
          web3StringToBytes32('I am reviewer'), true).send({
          from: accounts[0],
          gas: '4712388'
        });

      await Papers.methods.uploadReview(web3StringToBytes32('test'),
          web3StringToBytes32('I am reviewer'),
            web3StringToBytes32('review')).send({
          from: accounts[1],
          gas: '4712388'
      });
      // answer review - should fail
      try{
        await Papers.methods.uploadReview(web3StringToBytes32('test'),
          web3StringToBytes32('I am reviewer'),
            web3StringToBytes32('review')).send({
          from: accounts[1],
          gas: '4712388'
        });        // function no longer has returnCode
        // assert.equal(2,returnCode);
        // this should fail
        assert(false);
        } catch (err) {
          // this is correct - request should cause error
          if (err instanceof AssertionError) {
            assert(false);
          } else {
            assert(err);
          }
        }
    });


        it('call answerReview for a paper which does not exist', async () => {
          // need to register User first using setUserDetailsIPFS
          await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
            from: accounts[0],
            gas: '4712388'
          });

          // upload a dummy paper
          await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
              web3.utils.toWei('0.001','ether'),
              web3.utils.toWei('0.002','ether'),
              1, web3StringToBytes32('test'),
              web3StringToBytes32('test'),
              Date.now()/1000 + 24*60*60).send({
                from: accounts[0],
                gas: '4712388',
                value: web3.utils.toWei('0.002','ether')
          });
          // answer a review for a different paper hash
          try{
            await Papers.methods.answerReview(web3StringToBytes32('different'),
              web3StringToBytes32('I am reviewer'),
              true).send({
              from: accounts[0],
              gas: '4712388'
            });
            // this should fail
            assert(false);
            } catch (err) {
              // this is correct - request should cause error
              if (err instanceof AssertionError) {
                assert(false);
              } else {
                assert(err);
              }
            }
        });

        it('call answerReview for a paper which exist, when no prior review request exists', async () => {
          // need to register User first using setUserDetailsIPFS
          await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
            from: accounts[0],
            gas: '4712388'
          });

          // upload a dummy paper
          await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
              web3.utils.toWei('0.001','ether'),
              web3.utils.toWei('0.002','ether'),
              1, web3StringToBytes32('test'),
              web3StringToBytes32('test'),
              Date.now()/1000 + 24*60*60).send({
                from: accounts[0],
                gas: '4712388',
                value: web3.utils.toWei('0.002','ether')
          });
          // upload a review - should fail
          try{
            await Papers.methods.answerReview(web3StringToBytes32('test'),
              web3StringToBytes32('I am reviewer'),
              true).send({
              from: accounts[0],
              gas: '4712388'
            });
            // function no longer has returnCode
            // assert.equal(2,returnCode);
            // this should fail
            assert(false);
            } catch (err) {
              // this is correct - request should cause error
              if (err instanceof AssertionError) {
                assert(false);
              } else {
                assert(err);
              }
            }
        });

        it('call answerReview for a paper which is set to finalState', async () => {
          // need to register User first using setUserDetailsIPFS
          await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
            from: accounts[0],
            gas: '4712388'
          });
          // need to register User first using setUserDetailsIPFS
          await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
            from: accounts[1],
            gas: '4712388'
          });

          // upload a dummy paper
          await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
              web3.utils.toWei('0.001','ether'),
              web3.utils.toWei('0.002','ether'),
              1, web3StringToBytes32('test'),
              web3StringToBytes32('test'),
              Date.now()/1000 + 24*60*60).send({
                from: accounts[0],
                gas: '4712388',
                value: web3.utils.toWei('0.002','ether')
          });

          await  Papers.methods.buyPaper(web3StringToBytes32('test')).send({
                  from: accounts[1],
                  gas: '4712388',
                  value: web3.utils.toWei('0.001','ether')
          });

          await Papers.methods.sendReviewRequest(web3StringToBytes32('test'),
              web3StringToBytes32('I am reviewer')).send({
              from: accounts[1],
              gas: '4712388'
            });

          await Papers.methods.setFinalState(web3StringToBytes32('test')).send({
                  from: accounts[0],
                  gas: '4712388'
          });

          // answer review - should fail
          try{
            await Papers.methods.answerReview(web3StringToBytes32('test'),
              web3StringToBytes32('I am reviewer'),
                true).send({
              from: accounts[0],
              gas: '4712388'
            });
            // this should fail
            assert(false);
            } catch (err) {
              // this is correct - request should cause error
              if (err instanceof AssertionError) {
                assert(false);
              } else {
                assert(err);
              }
            }
        });

        it('call answerReview twice for a paper', async () => {
          // need to register User first using setUserDetailsIPFS
          await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
            from: accounts[0],
            gas: '4712388'
          });
          // need to register User first using setUserDetailsIPFS
          await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
            from: accounts[1],
            gas: '4712388'
          });

          // upload a dummy paper
          await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
              web3.utils.toWei('0.001','ether'),
              web3.utils.toWei('0.002','ether'),
              1, web3StringToBytes32('test'),
              web3StringToBytes32('test'),
              Date.now()/1000 + 24*60*60).send({
                from: accounts[0],
                gas: '4712388',
                value: web3.utils.toWei('0.002','ether')
          });

          await  Papers.methods.buyPaper(web3StringToBytes32('test')).send({
                  from: accounts[1],
                  gas: '4712388',
                  value: web3.utils.toWei('0.001','ether')
          });

          await Papers.methods.sendReviewRequest(web3StringToBytes32('test'),
              web3StringToBytes32('I am reviewer')).send({
              from: accounts[1],
              gas: '4712388'
            });

          await Papers.methods.answerReviewRequest(web3StringToBytes32('test'),
              web3StringToBytes32('I am reviewer'), true).send({
              from: accounts[0],
              gas: '4712388'
            });

          await Papers.methods.uploadReview(web3StringToBytes32('test'),
              web3StringToBytes32('I am reviewer'),
                web3StringToBytes32('review')).send({
              from: accounts[1],
              gas: '4712388'
          });

          await Papers.methods.answerReview(web3StringToBytes32('test'),
              web3StringToBytes32('I am reviewer'),
                true).send({
              from: accounts[0],
              gas: '4712388'
          });
          // answer review - should fail
          try{
            await Papers.methods.answerReview(web3StringToBytes32('test'),
                web3StringToBytes32('I am reviewer'),
                  true).send({
                from: accounts[0],
                gas: '4712388'
            });
            // this should fail
            assert(false);
            } catch (err) {
              // this is correct - request should cause error
              if (err instanceof AssertionError) {
                assert(false);
              } else {
                assert(err);
              }
            }
        });


            it('call uploadReview for a paper which does not exist', async () => {
              // need to register User first using setUserDetailsIPFS
              await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
                from: accounts[0],
                gas: '4712388'
              });
              // need to register User first using setUserDetailsIPFS
              await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
                from: accounts[1],
                gas: '4712388'
              });

              // upload a dummy paper
              await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
                  web3.utils.toWei('0.001','ether'),
                  web3.utils.toWei('0.002','ether'),
                  1, web3StringToBytes32('test'),
                  web3StringToBytes32('test'),
                  Date.now()/1000 + 24*60*60).send({
                    from: accounts[0],
                    gas: '4712388',
                    value: web3.utils.toWei('0.002','ether')
              });
              // request a review for a different paper hash
              try{
                await Papers.methods.uploadReview(web3StringToBytes32('different'),
                  web3StringToBytes32('I am reviewer'),
                  web3StringToBytes32('Review')).send({
                  from: accounts[1],
                  gas: '4712388'
                });
                // this should fail
                assert(false);
                } catch (err) {
                  // this is correct - request should cause error
                  if (err instanceof AssertionError) {
                    assert(false);
                  } else {
                    assert(err);
                  }
                }
            });

            it('call uploadReview for a paper which exist, when no prior review request exists', async () => {
              // need to register User first using setUserDetailsIPFS
              await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
                from: accounts[0],
                gas: '4712388'
              });
              // need to register User first using setUserDetailsIPFS
              await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
                from: accounts[1],
                gas: '4712388'
              });

              // upload a dummy paper
              await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
                  web3.utils.toWei('0.001','ether'),
                  web3.utils.toWei('0.002','ether'),
                  1, web3StringToBytes32('test'),
                  web3StringToBytes32('test'),
                  Date.now()/1000 + 24*60*60).send({
                    from: accounts[0],
                    gas: '4712388',
                    value: web3.utils.toWei('0.002','ether')
              });
              // upload a review - should fail
              try{
                await Papers.methods.uploadReview(web3StringToBytes32('test'),
                  web3StringToBytes32('I am reviewer'),
                  web3StringToBytes32('Review')).send({
                  from: accounts[1],
                  gas: '4712388'
                });
                // function no longer has returnCode
                // assert.equal(2,returnCode);
                // this should fail
                assert(false);
                } catch (err) {
                  // this is correct - request should cause error
                  if (err instanceof AssertionError) {
                    assert(false);
                  } else {
                    assert(err);
                  }
                }
            });

            it('call uploadReview for a paper which is set to finalState', async () => {
              // need to register User first using setUserDetailsIPFS
              await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
                from: accounts[0],
                gas: '4712388'
              });
              // need to register User first using setUserDetailsIPFS
              await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
                from: accounts[1],
                gas: '4712388'
              });

              // upload a dummy paper
              await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
                  web3.utils.toWei('0.001','ether'),
                  web3.utils.toWei('0.002','ether'),
                  1, web3StringToBytes32('test'),
                  web3StringToBytes32('test'),
                  Date.now()/1000 + 24*60*60).send({
                    from: accounts[0],
                    gas: '4712388',
                    value: web3.utils.toWei('0.002','ether')
              });

              await  Papers.methods.buyPaper(web3StringToBytes32('test')).send({
                      from: accounts[1],
                      gas: '4712388',
                      value: web3.utils.toWei('0.001','ether')
              });

              await Papers.methods.sendReviewRequest(web3StringToBytes32('test'),
                  web3StringToBytes32('I am reviewer')).send({
                  from: accounts[1],
                  gas: '4712388'
                });

              await Papers.methods.setFinalState(web3StringToBytes32('test')).send({
                      from: accounts[0],
                      gas: '4712388'
              });

              // upload review - should fail
              try{
                await Papers.methods.uploadReview(web3StringToBytes32('test'),
                  web3StringToBytes32('I am reviewer'),
                    web3StringToBytes32('review')).send({
                  from: accounts[1],
                  gas: '4712388'
                });
                // this should fail
                assert(false);
                } catch (err) {
                  // this is correct - request should cause error
                  if (err instanceof AssertionError) {
                    assert(false);
                  } else {
                    assert(err);
                  }
                }
            });

            it('call uploadReview twice for a paper', async () => {
              // need to register User first using setUserDetailsIPFS
              await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
                from: accounts[0],
                gas: '4712388'
              });
              // need to register User first using setUserDetailsIPFS
              await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
                from: accounts[1],
                gas: '4712388'
              });

              // upload a dummy paper
              await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
                  web3.utils.toWei('0.001','ether'),
                  web3.utils.toWei('0.002','ether'),
                  1, web3StringToBytes32('test'),
                  web3StringToBytes32('test'),
                  Date.now()/1000 + 24*60*60).send({
                    from: accounts[0],
                    gas: '4712388',
                    value: web3.utils.toWei('0.002','ether')
              });

              await  Papers.methods.buyPaper(web3StringToBytes32('test')).send({
                      from: accounts[1],
                      gas: '4712388',
                      value: web3.utils.toWei('0.001','ether')
              });

              await Papers.methods.sendReviewRequest(web3StringToBytes32('test'),
                  web3StringToBytes32('I am reviewer')).send({
                  from: accounts[1],
                  gas: '4712388'
                });

              await Papers.methods.answerReviewRequest(web3StringToBytes32('test'),
                  web3StringToBytes32('I am reviewer'), true).send({
                  from: accounts[0],
                  gas: '4712388'
                });

              await Papers.methods.uploadReview(web3StringToBytes32('test'),
                  web3StringToBytes32('I am reviewer'),
                    web3StringToBytes32('review')).send({
                  from: accounts[1],
                  gas: '4712388'
              });
              // answer review - should fail
              try{
                await Papers.methods.uploadReview(web3StringToBytes32('test'),
                  web3StringToBytes32('I am reviewer'),
                    web3StringToBytes32('review')).send({
                  from: accounts[1],
                  gas: '4712388'
                });        // function no longer has returnCode
                // assert.equal(2,returnCode);
                // this should fail
                assert(false);
                } catch (err) {
                  // this is correct - request should cause error
                  if (err instanceof AssertionError) {
                    assert(false);
                  } else {
                    assert(err);
                  }
                }
            });

  it('call getPaperFinalState for a paper which does not exist', async () => {
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user1')).send({
      from: accounts[0],
      gas: '4712388'
    });
    // need to register User first using setUserDetailsIPFS
    await Papers.methods.setUserDetailsIPFS(web3StringToBytes32('user2')).send({
      from: accounts[1],
      gas: '4712388'
    });

    // upload a dummy paper
    await  Papers.methods.uploadPaper(web3StringToBytes32('test'),
        web3.utils.toWei('0.001','ether'),
        web3.utils.toWei('0.002','ether'),
        1, web3StringToBytes32('test'),
        web3StringToBytes32('test'),
        Date.now()/1000 + 24*60*60).send({
          from: accounts[0],
          gas: '4712388',
          value: web3.utils.toWei('0.002','ether')
    });
    // request a review for a different paper hash
    try{
      await Papers.methods.getPaperFinalState(web3StringToBytes32('different')).call({
        from: accounts[1],
        gas: '4712388'
      });
      // function no longer has returnCode
      // assert.equal(2,returnCode);
      // this should fail
      assert(false);
      } catch (err) {
        // this is correct - request should cause error
        if (err instanceof AssertionError) {
          assert(false);
        } else {
          assert(err);
        }
      }
  });


});
