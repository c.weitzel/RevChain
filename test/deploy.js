const HDWalletProvider = require('truffle-hdwallet-provider');
const Web3 = require ('web3');
const {interface, bytecode} = require('./compile');

const provider = new HDWalletProvider (
  'never park grocery capable raw evidence episode citizen cross slab few chunk',
  'https://rinkeby.infura.io/2ZbBEFAPypq3NEKVvagB'
);
const web3 = new Web3(provider);

const deploy = async ()=> {
  const accounts = await web3.eth.getAccounts();

  console.log('Attempting to deploy from account', accounts[0]);

  const result = await new web3.eth.Contract(JSON.parse(interface))
    .deploy({data: '0x' + bytecode})
  //  .send({from: accounts[0] });
  .send({gas: '4712388', from: accounts[0] });
  console.log('Contract deployed to address', result.options.address);

};
deploy();
