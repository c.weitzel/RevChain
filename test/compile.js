const path = require ('path');
const fs = require ('fs');
const solc = require ('solc');

// const lotteryPath = path.resolve(__dirname, '../contracts', 'lottery.sol');
const papersPath = path.resolve(__dirname, '../contracts', 'Papers.sol');
const source = fs.readFileSync(papersPath, 'utf8');

module.exports = solc.compile(source, 1).contracts[':Papers'];
// module.exports = solc.compile(source, 1).contracts[':Lottery'];
// console.log(solc.compile(source, 1));
